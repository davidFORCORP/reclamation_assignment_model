from models.create_surface import CreateSurface
from models.create_moisture_regime import CreateMoistureRegime
from models.create_topographic_categorizations import CreateClassifications
from models.create_ecosite import CreateEcosite
from datetime import datetime
from core.config import Config
from core.reused_functions import WorkspaceWorker
from models.original_ram.Moisture_Model_v18 import Run_Ram


def main():

    Run_Ram().run()
    #
    # start_from_scratch = eval(Config().get('start_from_scratch', 'start_from_scratch'))
    # create_dem_surface = eval(Config().get('models_to_run', 'create_dem_surface'))
    # create_moisture_regime = eval(Config().get('models_to_run', 'create_moisture_regime'))
    # create_slope_classification = eval(Config().get('models_to_run', 'create_slope_classification'))
    # create_aspect_classification = eval(Config().get('models_to_run', 'create_aspect_classification'))
    # create_topographic_position_classification = eval(Config().get('models_to_run', 'create_topographic_position_classification'))
    # create_ecosite = eval(Config().get('models_to_run', 'create_ecosite'))
    #
    # if start_from_scratch:
    #     WorkspaceWorker(Config().get('global_parameters', 'output_location')).create()
    #     WorkspaceWorker(Config().get('global_parameters', 'output_location')).delete()
    #
    # if create_dem_surface:
    #     print ('Beginning work on the DEM')
    #     CreateSurface().run()
    #
    # if create_slope_classification:
    #     print ('\nBeginning work on slope')
    #     CreateClassifications('SLOPE').run()
    #
    # if create_aspect_classification:
    #     print ('\nBeginning work on aspect')
    #     CreateClassifications('ASPECT').run()
    #
    # if create_topographic_position_classification:
    #     print ('\nBeginning work on topographic positioning')
    #     CreateClassifications('TOPOGRAPHIC').run()
    #
    # if create_moisture_regime:
    #     print ('\nBeginning work on moisture regime')
    #     CreateMoistureRegime().run()
    #
    # if create_ecosite:
    #     print ('\nBeginning work on ecosite prediction')
    #     CreateEcosite().run()


if __name__ == "__main__":

    now_time = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    print ('\n***************************************** \n'
           ' The Reclamation Assignment Model (RAM) \n'
           '***********  Version 3.2  *************** \n'
           '--------- ' + now_time + ' ---------- \n \n')
    main()
