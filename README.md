# The Reclamation Assignment Model #

The Reclamation Assignment Model, or RAM, is the FORCORP process built to assign an ecosite
phase call to predicted mine closure landscapes. The process utilizes the Field Guide to Ecosites
(Archibald and Beckingham, 1996) to arrive at site condition classificaitons to be be used by
internal LiDAR DEM analysis. Site conditions are matched to a likely ecosite phase based on the
Field Guide's plot distribution amongs the various site condition combinations.

For a detialed description on the RAM, please refer to:

\\silver\projects\Standard_Procedures\Reclamation_Assignment_Model_RAM\Documentation\RAM - The Reclamation Assignment Model. Version 3.0 (February, 2019).pdf

## Prerequistes ##

* python 2.7 or newer
* arcpy


## Configuration ##

The application reads it's configuration from 'setting.ini'.

Do not put database credentials into this file as they might accidentally get committed
into source control, use a .pgpass file instead. If you use pgAdmin it has probably
already created that file. See: https://www.postgresql.org/docs/11/libpq-pgpass.html

## Setting up a run ##

### Selecting Models to Run ###

Within the .ini file, you will see models_to_run. Set individual variables to True to tell the model to run these pieces
of code.

### Creating a DEM surface ###

If you set models_to_run.create_dem_surface = True then you will be creating a DEM to be used in the model. This DEM is
used for many purposes within this sub-model. Derivatives of the DEM created within this model of the DEM include Slope,
Aspect, and a hydrologically correct surface model.

When contemplating a run of the create_dem_surface, your attention must be brought to the surface_parameters
section of the .ini file. Here you define the datasets that can be used in the model.

The only required datasets for this model to run is a DEM (preferrably LiDAR based with 1m resolution). All other
datasets listed in this section of the .ini file are optional but will help in carving out a hydrological system
later on which conforms to mine planner perceptions. A useful excercise is to not include all of these extra datasets
and compare this product to when all the datasets are in place. This analysis will allow you to show the mine planner
where they may be assigning wet-areas in topographically incorrect areas.

When specifing additional datasets, be sure to either include them (True) or exclude them (False) where and when
appropriate. Any combination of ON and OFF for these parameters should be possible.

#### Notes on Execution ####

* Culvert file must be a polygon.
* Stream file must be a polygon.

If you are burning streams into the DEM, the stream layer must be a polygon and represent the "footprint" of the stream
(wetted width).



### Creating Moisture Regime ###

Moisture regime has three main components, or steps. The first step includes the use (or no use) of substrate, the second
step assesses the landscape for flood plains, and the third step creates the actual moisture regime.

#### Introducing (or not) Substrate ####

The creation of a mositure regime for the landscape in the model depends on if you wiil be introducing substrate factors 
or not. When setting up the '.ini' file, you can either set 'introduce_sai' to True or False as well as
'use_existing_texture' to True or False. The setting of these two parameters is very sensitive to the case that you are
currently wishing to process. 

##### use_existing_texture = False, introduce_sai = True #####

This signifies the first time you are running the substrate factor. A texture raster will be created. The streams that 
result for priming the moisture regime will be a function of the substrate surface.

##### use_existing_texture = True, introduce_sai = True #####

Setting the model up like so means that you have run the model before with use_existing_texture = False and 
introduce_sai = True. This is the setting to use when you want to use the substrate corrected stream layer
in the determination of moisture regime. 

##### use_existing_texture = True or False, introduce_sai = False #####

This setting forces the model to forget substrate and will create or re-create the streams layer with no consideration
to substrate. 

The settings of this section 
