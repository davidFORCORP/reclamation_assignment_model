from models.base_execution.surface_categorizations import TopographicClassifications

class CreateClassifications:

    def __init__(self, classification_type):
        self.classifications = TopographicClassifications()
        self.classification_type = classification_type

    def run(self):

        if self.classification_type == 'SLOPE':
            self.classifications.slope()
        if self.classification_type == 'ASPECT':
            self.classifications.aspect()
        if self.classification_type == 'TOPOGRAPHIC':
            self.classifications.topographic_position()

        print '...surface classified'
