BEGIN;
SET search_path = reclamation_assignment_model, public;

-----------------------------------------------------------------------------
DROP TABLE IF EXISTS ram_input_landscape_condition_and_probabilities;
CREATE TABLE ram_input_landscape_condition_and_probabilities AS
WITH tab AS
	(
		SELECT mrclass, tpclass, slclass, asclass, mrclass||tp_class||'medium'||slclass||asclass label, substrate, (ST_Dump(ST_MakeValid(ST_Buffer(ST_SnapToGrid(geom, 0.1, 0)))).geom geom , (random() * 10000000)::int random_value
		FROM ram_data_from_python_for_ecophase_classification
	)
SELECT a.*, cnd current_condition, ecosite current_ecosite, a1,b1,b2,b3,b4,c1,d1,d2,d3,e1,e2,e3,f1,f2,f3,g1,h1,i1,i2,j1,j2,k1,k2,k3,l1
FROM tab a LEFT JOIN field_guide_ecophase_probabilities b
	ON a.label = b.label;
COMMIT;

------------------------------------------------------------------------------
--CREATE TABLE largest_10_pct AS
--SELECT *, ntile(1) over (order by area desc) as quaritle FROM (
--SELECT *, st_area(geom) / 10000 area, (st_area(geom) / (SELECT sum(st_area(geom)) FROM current_condition_joined)) pct_rep
--FROM current_condition_joined) b
--WHERE substrate <> 'WATER' AND substrate IS NOT NULL
--AND cndclass not like 'sub hygric%'
--;

-------------------------------------------------------------------------------
DROP TABLE IF EXISTS current_condition_joined_rndm1;
CREATE TABLE current_condition_joined_rndm1
(
  geom geometry,
  current_condition character varying(255),
  CNDCLASS character varying(255),
  a1 double precision,
  b1 double precision,
  b2 double precision,
  b3 double precision,
  b4 double precision,
  c1 double precision,
  d1 double precision,
  d2 double precision,
  d3 double precision,
  e1 double precision,
  e2 double precision,
  e3 double precision,
  f1 double precision,
  f2 double precision,
  f3 double precision,
  g1 double precision,
  h1 double precision,
  i1 double precision,
  i2 double precision,
  j1 double precision,
  j2 double precision,
  k1 double precision,
  k2 double precision,
  k3 double precision,
  l1 double precision,
  id integer NOT NULL,
  random_value integer,
  CONSTRAINT current_condition_joined_rndm1_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE current_condition_joined_rndm1
  OWNER TO ANALYSIS;
CREATE INDEX idx_current_condition_joined_rndm1_geom
  ON current_condition_joined_rndm1
  USING gist
  (geom);
----------------------------------------------------------------------
DROP TABLE IF EXISTS non_conflict_phase1;
CREATE TABLE non_conflict_phase1
(
  current_ecophase_nc numeric(15,6),
  current_ecosite_nc numeric(15,6),
  geom geometry,
  area_ha_by_phase double precision,
  area_ha_by_site double precision,
  id serial NOT NULL,
  CONSTRAINT non_conflict_phase1_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_non_conflict_phase1_geom
  ON non_conflict_phase1
  USING gist
  (geom);
----------------------------------------------------------------------
DROP TABLE IF EXISTS non_conflict_ecosite_proportions1;
CREATE TABLE non_conflict_ecosite_proportions1
(
  current_ecosite_nc character varying(2),
  --current_ecosite_nc numeric(15,6),
  current_ecosite_difference double precision,
  id serial NOT NULL,
  CONSTRAINT non_conflict_ecosite_proportions1_pkey PRIMARY KEY (id)
);
----------------------------------------------------------------------
DROP TABLE IF EXISTS conflict1;
CREATE TABLE conflict1
(
  geom geometry,
  cndclass  character varying(255),
  current_ecosite character varying(255),
  substrate character varying(255),
  a1 double precision,
  b1 double precision,
  b2 double precision,
  b3 double precision,
  b4 double precision,
  c1 double precision,
  d1 double precision,
  d2 double precision,
  d3 double precision,
  e1 double precision,
  e2 double precision,
  e3 double precision,
  f1 double precision,
  f2 double precision,
  f3 double precision,
  g1 double precision,
  h1 double precision,
  i1 double precision,
  i2 double precision,
  j1 double precision,
  j2 double precision,
  k1 double precision,
  k2 double precision,
  k3 double precision,
  l1 double precision,
  id integer NOT NULL,
  random_value integer,
  CONSTRAINT conflict1_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_conflict1_geom
  ON conflict1
  USING gist
  (geom);

-------------------------------------------------------------
DROP TABLE IF EXISTS ecophase_transposed1;
CREATE TABLE ecophase_transposed1
(
  id serial NOT NULL,
  ecophase character varying(2),
  ecosite character varying(2),
  likelihood double precision,
  PwProb double precision,
  --PwProb01 double precision,
  --PwProbRoll double precision,
  CONSTRAINT ecophase_transposed1_pkey PRIMARY KEY (id)
);
-------------------------------------------------------------
DROP TABLE IF EXISTS proportions1;
CREATE TABLE proportions1
(
  id serial NOT NULL,
  ecophase character varying,
  ecosite character varying,
  ecophasenum integer,
  ecositenum integer,
  ecophaseprop double precision,
  ecositeprop double precision,
  CONSTRAINT proportions1_pkey PRIMARY KEY (id)
);
-------------------------------------------------------------

DO
$$
DECLARE
  count INTEGER;
  conflict_record RECORD;
  ecophase_select character varying(2);
  sub character varying(100);
  table_name character varying(50);
  sum_area double precision;
BEGIN

  INSERT INTO conflict1
  (geom, cndclass, id,random_value)
  VALUES
  (ST_SetSRID(ST_MakePoint(1,2), 26912),'X',0,0);

  select count(*) into count from conflict1;

  RAISE NOTICE 'Count is currently %', count;
  WHILE count > 0
  LOOP

  TRUNCATE TABLE conflict1;
  INSERT INTO conflict1
        (cndclass, current_ecosite, substrate, a1,b1,b2,b3,b4,c1,d1,d2,d3,e1,e2,e3,f1,f2,f3,g1,h1,i1,i2,j1,j2,k1,k2,k3,l1,id,random_value)
  SELECT cndclass, current_ecosite, substrate, a1,b1,b2,b3,b4,c1,d1,d2,d3,e1,e2,e3,f1,f2,f3,g1,h1,i1,i2,j1,j2,k1,k2,k3,l1,id,random_value
  FROM current_condition_joined1
  WHERE current_condition = 'CONFLICT';

  SELECT count(*)
  INTO count
  FROM conflict1;

  IF count = 0 THEN
	RAISE NOTICE 'There are no features left that have conflicts';
	EXIT;  -- exit loop
  END IF;
  RAISE NOTICE 'Count is %', count;

  SELECT *
  INTO conflict_record
  FROM (
	SELECT a.*, row_number() OVER (ORDER BY random_value DESC) AS row_number
	FROM conflict1 a
	ORDER BY row_number ASC
       ) subquery
  where row_number = 1;

  SELECT substrate
  INTO sub
  FROM (
	SELECT a.*, row_number() OVER (ORDER BY random_value DESC) AS row_number
	FROM conflict1 a
	ORDER BY row_number ASC
       ) subquery
  where row_number = 1;

  TRUNCATE TABLE proportions1;

  IF sub = 'SUITABLE SS' OR
     sub = 'SUITABLE SUBSOIL' THEN
	table_name = 'ss_proportions';
  ELSIF
     sub = 'N/A' OR
     sub = 'NONE' OR
     sub = NULL OR
     sub = '' THEN
	table_name = 'nat_proportions';
  ELSIF
     sub = 'TAILING SAND' OR
     sub = 'TAILINGS SAND' THEN
	table_name = 'ts_proportions';
  ELSIF
     sub = 'COKE' THEN
	table_name = 'co_proportions';
  ELSIF
     sub = 'CAKE' THEN
	table_name = 'ca_proportions';
  ELSIF
     sub = 'UNSUITABLE OVERBURDEN' THEN
	table_name = 'uo_proportions';
  END IF;

  EXECUTE format('INSERT INTO proportions1 (SELECT * FROM %I);', table_name);
  --IF table_name = 'ts_proportions' then
 -- exit;
  --end if;

TRUNCATE TABLE non_conflict_ecosite_proportions1;
  INSERT INTO non_conflict_ecosite_proportions1
  (current_ecosite_nc, current_ecosite_difference)
  WITH subquery AS
	(SELECT current_ecosite current_ecosite_nc--, SUM(ST_Area(geom))/ 10000 area_ha_by_ecosite
	 FROM current_condition_joined1
	 WHERE current_condition <> 'CONFLICT'
		AND (substrate = sub or substrate is null)
	 GROUP BY current_ecosite)
  SELECT DISTINCT ON (b.ecosite) subquery.current_ecosite_nc,
  (CASE WHEN b.ecositeprop > 0 THEN 1 ELSE 0 END)-- - (subquery.area_ha_by_ecosite / sum_area))
  current_ecosite_difference -- TEST using select select (sum(area_ha_by_ecosite) FROM subquery)
  FROM proportions1 b JOIN subquery ON  subquery.current_ecosite_nc = b.ecosite
  --where b.ecositeprop > 0
  ;

  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'a1', 'a', conflict_record.a1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'b1', 'b', conflict_record.b1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'b2', 'b', conflict_record.b2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'b3', 'b', conflict_record.b3);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'b4', 'b', conflict_record.b4);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'c1', 'c', conflict_record.c1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'd1', 'd', conflict_record.d1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'd2', 'd', conflict_record.d2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'd3', 'd', conflict_record.d3);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'e1', 'e', conflict_record.e1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'e2', 'e', conflict_record.e2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'e3', 'e', conflict_record.e3);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'f1', 'f', conflict_record.f1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'f2', 'f', conflict_record.f2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'f3', 'f', conflict_record.f3);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'g1', 'g', conflict_record.g1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'h1', 'h', conflict_record.h1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'i1', 'i', conflict_record.i1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'i2', 'i', conflict_record.i2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'j1', 'j', conflict_record.j1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'j2', 'j', conflict_record.j2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'k1', 'k', conflict_record.k1);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'k2', 'k', conflict_record.k2);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'k3', 'k', conflict_record.k3);
  INSERT INTO ecophase_transposed1 ( ecophase, ecosite, likelihood) VALUES ( 'l1', 'l', conflict_record.l1);

 UPDATE  ecophase_transposed1
  SET pwprob = (
	SELECT max((likelihood * 1000) * current_ecosite_difference)
	FROM non_conflict_ecosite_proportions1 ncep
	WHERE ncep.current_ecosite_nc = ecosite
	);

 SELECT ecophase
    INTO ecophase_select
    FROM (
  	SELECT ecophase
  	FROM ecophase_transposed1
  	where pwprob = (select max(pwprob) from ecophase_transposed1)
         )  subquery1;

if ((UPPER(sub) = 'TAILINGS SAND' OR upper(sub) = 'UNSUITABLE SUBSOIL') and (ecophase_select = 'g1' or ecophase_select = 'a1' or ecophase_select = 'c1' or ecophase_select = 'i1' or ecophase_select = 'i2') OR ecophase_select is null) THEN
RAISE NOTICE 'check tables';
exit;
end if;

  UPDATE current_condition_joined1 a
  SET current_condition = ecophase_select
  WHERE a.id = conflict_record.id;
  UPDATE current_condition_joined1 a
  SET current_ecosite = left(ecophase_select, 1)
  WHERE a.id = conflict_record.id;


  TRUNCATE TABLE ecophase_transposed1;
  END LOOP;

END
$$