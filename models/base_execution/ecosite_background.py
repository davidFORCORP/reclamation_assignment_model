import os
import shutil
from datetime import datetime
from time import time
import arcpy
import sys
from core.config import Config
from arcpy.sa import *
from models.base_execution.substrate_preparation import SubstratePreparation
from core.reused_functions import WorkspaceWorker, CleanAndRepair, HelperFunctions


class EcositeCreation():

    def __init__(self):

        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True

        self.output_location = Config().get('global_parameters', 'output_location')
        self.ecophase_folder = self.output_location + '/ecophase'

        self.field_guide_proportion_probabilities = Config().get('ecosite_parameters', 'field_guide_proportion_probabilities')
        self.craig_substrate_proportions = Config().get('ecosite_parameters', 'craig_substrate_proportions')
        self.pre_existing_ecophase_data = Config().get('ecosite_parameters', 'pre_existing_ecophase_data')
        self.substrate = Config().get('substrate_parameters', 'substrate')

        self.psql = 'psql -q -d projects -h pg-plan01-prd.forcorp.local'
        self.cmdline = HelperFunctions()

    def process_ecophase(self):

        _ = WorkspaceWorker(self.ecophase_folder).create()

        arcpy.env.scratchWorkspace = self.ecophase_folder
        arcpy.env.workspace = self.ecophase_folder

        datasets = ['mr.shp', 'sl.shp', 'as.shp', 'tp.shp']

        add_fields = [['subs_class', 'TEXT', 'NONE'], ['subs_type', 'TEXT', '']]
        use_substrate = eval(Config().get('substrate_parameters', 'introduce_sai'))
        if use_substrate:
            datasets.append(Config().get('substrate_parameters', 'substrate'))

        arcpy.Union_analysis(datasets, self.ecophase_folder + '/collate', "", "", "NO_GAPS")
        if not use_substrate:
            arcpy.AddField_management(self.ecophase_folder + '/collate.shp', add_fields[0][0], add_fields[0][1])
            arcpy.AddField_management(self.ecophase_folder + '/collate.shp', add_fields[1][0], add_fields[1][1])
            arcpy.CalculateField_management(self.ecophase_folder + '/collate.shp', add_fields[0][0], str(add_fields[0][2]))

        insrs = arcpy.Describe(self.ecophase_folder + '/collate.shp').spatialReference.factoryCode
        command = 'shp2pgsql -I -S -d -s %s %s reclamation_assignment_model.ram_data_from_python_for_ecophase_classification| %s' % (insrs, self.ecophase_folder + '/collate.shp', self.psql)
        self.cmdline.execute_command_line(command)

        command = '%s -f %s' % (self.psql, '//silver/projects/standard_procedures/Reclamation_Assignment_Model_RAM/ecophase_processing_20220912.sql')
        self.cmdline.execute_command_line(command)

        WorkspaceWorker(self.ecophase_folder).delete()

    # def append_field_guide_data(self):
    #
    #     array = []
    #     field_list = ["MRCLASS", "TPCLASS", "SOILTXT", "SLCLASS", "ASCLASS", "CNDCLASS", "SUBS_TYPE"]
    #     arcpy.Dissolve_management("CND1.SHP", "CND5", field_list, "", "SINGLE_PART")
    #     arcpy.MakeFeatureLayer_management("CND5.SHP", Layer1)
    #     arcpy.JoinField_management(Layer1, "CNDCLASS", Label, "LABEL")
    #     arcpy.JoinField_management(Layer1, "subs_type", substrate_proportions, "subs_type")
    #     arcpy.Delete_management(Layer1)
    #
    #     arcpy.Merge_management("CND5.shp; " + output_location + "\\CND\\default.shp", "CND4")
    #
    #     shape = "CND4.shp"
    #     if arcpy.Exists(output_location + "\BNDRYMTCH\ecophase.shp"):
    #         if int(arcpy.GetCount_management(output_location + "\BNDRYMTCH\ecophase.shp").getOutput(0)) == 0:
    #             print 'No pre-existing ecophases to account for.'
    #         else:
    #             print 'Boundary matching taking place'
    #             arcpy.Erase_analysis("CND4.SHP", output_location + "\BNDRYMTCH\ecophase.shp", "CND2")
    #             arcpy.Merge_management(output_location + "\BNDRYMTCH\ecophase.shp; CND2.shp", "CND3")
    #             array.append("CND2.shp")
    #             array.append("CND3.shp")
    #             shape = "CND3.shp"
    #
    #     print 'Calculating ecophase'
    #     arcpy.AddField_management(shape, 'a1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'b1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'b2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'b3_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'b4_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'c1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'd1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'd2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'd3_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'e1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'e2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'e3_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'f1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'f2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'f3_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'g1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'h1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'i1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'i2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'j1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'j2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'k1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'k2_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'k3_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'l1_1', 'DOUBLE')
    #     arcpy.AddField_management(shape, 'ecophase', 'STRING')
    #
    #     arcpy.CalculateField_management(shape, 'a1_1', '[sub_a] * [a1]')
    #     arcpy.CalculateField_management(shape, 'b1_1', '[sub_b] * [b1]')
    #     arcpy.CalculateField_management(shape, 'b2_1', '[sub_b] * [b2]')
    #     arcpy.CalculateField_management(shape, 'b3_1', '[sub_b] * [b3]')
    #     arcpy.CalculateField_management(shape, 'b4_1', '[sub_b] * [b4]')
    #     arcpy.CalculateField_management(shape, 'c1_1', '[sub_c] * [c1]')
    #     arcpy.CalculateField_management(shape, 'd1_1', '[sub_d] * [d1]')
    #     arcpy.CalculateField_management(shape, 'd2_1', '[sub_d] * [d2]')
    #     arcpy.CalculateField_management(shape, 'd3_1', '[sub_d] * [d3]')
    #     arcpy.CalculateField_management(shape, 'e1_1', '[sub_e] * [e1]')
    #     arcpy.CalculateField_management(shape, 'e2_1', '[sub_e] * [e2]')
    #     arcpy.CalculateField_management(shape, 'e3_1', '[sub_e] * [e3]')
    #     arcpy.CalculateField_management(shape, 'f1_1', '[sub_f] * [f1]')
    #     arcpy.CalculateField_management(shape, 'f2_1', '[sub_f] * [f2]')
    #     arcpy.CalculateField_management(shape, 'f3_1', '[sub_f] * [f3]')
    #     arcpy.CalculateField_management(shape, 'g1_1', '[sub_g] * [g1]')
    #     arcpy.CalculateField_management(shape, 'h1_1', '[sub_h] * [h1]')
    #     arcpy.CalculateField_management(shape, 'i1_1', '[sub_i] * [i1]')
    #     arcpy.CalculateField_management(shape, 'i2_1', '[sub_i] * [i2]')
    #     arcpy.CalculateField_management(shape, 'j1_1', '[sub_j] * [j1]')
    #     arcpy.CalculateField_management(shape, 'j2_1', '[sub_j] * [j2]')
    #     arcpy.CalculateField_management(shape, 'k1_1', '[sub_k] * [k1]')
    #     arcpy.CalculateField_management(shape, 'k2_1', '[sub_k] * [k2]')
    #     arcpy.CalculateField_management(shape, 'k3_1', '[sub_k] * [k3]')
    #     arcpy.CalculateField_management(shape, 'l1_1', '[sub_l] * [l1]')
    #
    #     expression = 'MyCalc(!a1_1!,!b1_1!,!b2_1!,!b3_1!,!b4_1!,!c1_1!,!d1_1!,!d2_1!,!d3_1!,!e1_1!,!e2_1!,!e3_1!,!f1_1!,!f2_1!,!f3_1!,!g1_1!,!h1_1!,!i1_1!,!i2_1!,!j1_1!,!j2_1!,!k1_1!,!k2_1!,!k3_1!,!l1_1!,!CND!)'
    #     codeblock = """def MyCalc(a1_1,b1_1,b2_1,b3_1,b4_1,c1_1,d1_1,d2_1,d3_1,e1_1,e2_1,e3_1,f1_1,f2_1,f3_1,g1_1,h1_1,i1_1,i2_1,j1_1,j2_1,k1_1,k2_1,k3_1,l1_1, CND):
    #     array = [a1_1,b1_1,b2_1,b3_1,b4_1,c1_1,d1_1,d2_1,d3_1,e1_1,e2_1,e3_1,f1_1,f2_1,f3_1,g1_1,h1_1,i1_1,i2_1,j1_1,j2_1,k1_1,k2_1,k3_1,l1_1]
    #     array1 = ['a1','b1','b2','b3','b4','c1','d1','d2','d3','e1','e2','e3','f1','f2','f3','g1','h1','i1','i2','j1','j2','k1','k2','k3','l1']
    #     index_me = 0
    #     for r in array:
    #         if CND == 'CONFLICT':
    #             if r == max(array):
    #                 ecophase = array1[index_me]
    #                 break
    #             index_me += 1
    #         else:
    #             ecophase = CND
    #     return ecophase"""
    #     arcpy.CalculateField_management(shape, 'ecophase', expression, "PYTHON_9.3", codeblock)
    #     arcpy.CalculateField_management(shape, 'ecosite', 'left([ecophase],1)')
    #     print 'Creating final Layer'
    #     arcpy.Copy_management(shape, "CND_pre")
    #     arcpy.SimplifyPolygon_cartography(shape, "CND", "BEND_SIMPLIFY", "10 Meters", "100 SquareMeters", "RESOLVE_ERRORS", "KEEP_COLLAPSED_POINTS")
    #     arcpy.AddField_management("CND.shp", "AREA_ha", "DOUBLE")
    #     arcpy.CalculateField_management("CND.shp", "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")