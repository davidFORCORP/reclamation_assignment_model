import arcpy
import os
import datetime
from datetime import time
from core.config import Config

class eliminate_features(object):
    def __init__(self, in_features):
        self.in_features = in_features
        if not arcpy.Exists(self.in_features):
            raise Exception('input features do not exist - {}'.format(self.in_features))
        pass

    def eliminate(self):
        print 'Eliminating @ ' + datetime.datetime.fromtimestamp(time()).strftime('%H:%M')

        output_location = Config().get('global_parameters', 'output_location')
        temp_space = output_location + '/temp'
        layer1 = output_location + '/temp/layer1.lyr'
        layer2 = output_location + '/temp/layer2.lyr'

        if arcpy.Exists(temp_space):
            arcpy.Delete_management(temp_space)
        os.makedirs(temp_space)
        arcpy.env.workspace = temp_space
        arcpy.env.outputMFlag = "Disabled"
        shape = self.in_features

        array = []
        y = int(arcpy.GetCount_management(shape).getOutput(0))
        x = 1
        count = 1
        print 'Eliminating slivers < 0.01ha'
        while y != x:
            y = x
            arcpy.MakeFeatureLayer_management(shape, layer1)
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"AREA" < 100')
            zz = int(arcpy.GetCount_management(layer1).getOutput(0))
            arcpy.Eliminate_management(layer1, "Xa{}".format(count), "AREA")
            shape = "Xa{}.shp".format(count)
            try:
                arcpy.AddField_management(shape, "AREA", "DOUBLE")
            except:
                time.sleep(60)
                arcpy.AddField_management(shape, "AREA", "DOUBLE")
            arcpy.CalculateField_management(shape, "AREA", "!Shape.Area!", "PYTHON_9.3")

            array.append(shape)
            arcpy.Delete_management(layer1)
            x = int(arcpy.GetCount_management(shape).getOutput(0))
            count += 1

        arcpy.Dissolve_management(shape, "El_p01", "GRIDCODE", "", "SINGLE_PART")
        for d in array:
            arcpy.Delete_management(d)

        array = []
        print "Eliminating slivers < 0.1ha @ " + datetime.datetime.fromtimestamp(time()).strftime('%H:%M')
        shape = "El_p01.shp"
        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
        arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
        code = 0
        feature_count = 1
        while code < 9:
            count = 1
            print 'working on gridcode {}'.format(str(code))

            if code == 0:
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = {} '.format(str(code)))
                if int(arcpy.GetCount_management(layer1).getOutput(0)) == 0:
                    print 'We have solved all code = 0\'s'
                else:
                    arcpy.Eliminate_management(layer1, temp_space +'/Move{}S{}'.format(str(code), str(count)))
                    arcpy.Dissolve_management(temp_space +'/Move{}S{}.shp'.format(str(code), str(count)),
                                              temp_space + '/Move{}Sx{}'.format(str(code), str(count)),
                                              "GRIDCODE", "", "SINGLE_PART")
                    arcpy.RepairGeometry_management(temp_space +'/Move{}Sx{}.shp'.format(str(code), str(count)))
                    shape = temp_space +'/Move{}Sx{}.shp'.format(str(code), str(count))
                    arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                    arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                    arcpy.Delete_management(layer1)
                    array.append(shape)
            else:
                while feature_count > 0:

                    print 'Working on slivers between 0 and 0.1 - iteration {}. There are {} features left.'.format(str(count),feature_count)

                    arcpy.Copy_management(shape, "Code{}S{}".format(str(code), str(count)))
                    shape2 = temp_space +'/CODE{}S{}.shp'.format(str(code), str(count))
                    arcpy.MakeFeatureLayer_management(shape, layer1)
                    arcpy.MakeFeatureLayer_management(shape2, layer2)
                    # This ensures that there is only movement of 1 class by moisture regimes with areas < 0.3ha
                    arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                            '"GRIDCODE" = {} AND "AREA_ha" < 0.1'.format(str(code)))
                    feature_count = int(arcpy.GetCount_management(layer1).getOutput(0))

                    arcpy.SelectLayerByAttribute_management(layer2, "NEW_SELECTION", '"GRIDCODE" > {} '.format(
                        str(code + 1)) + 'OR "GRIDCODE" < {}'.format(str(code - 1)))

                    arcpy.Eliminate_management(layer1, temp_space +'/Move{}S{}'.format(str(code), str(count)), "",
                                               "", layer2)
                    arcpy.Dissolve_management(temp_space +'/Move{}S{}.shp'.format(str(code), str(count)),
                                              temp_space + '/Move{}Sy{}'.format(str(code), str(count)),
                                              "GRIDCODE", "", "SINGLE_PART")
                    arcpy.RepairGeometry_management(temp_space +'/Move{}S{}.shp'.format(str(code), str(count)))
                    shape = temp_space +'/Move{}Sy{}.shp'.format(str(code), str(count))

                    try:
                        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                    except:
                        time.sleep(60)
                        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")

                    arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                    arcpy.Delete_management(layer1)
                    arcpy.Delete_management(layer2)
                    array.append(shape)
                    array.append(shape2)
                    count += 1
                    if count == 5:
                        feature_count = 0
            feature_count = 1
            code += 1
        arcpy.Copy_management(shape, "final_elim")