import os
import shutil
from datetime import datetime
from time import time
import arcpy
import sys
from core.config import Config
from arcpy.sa import *

from core.reused_functions import WorkspaceWorker


class DemCreation():

    def __init__(self):

        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True
        self.output_location = Config().get('global_parameters', 'output_location')
        self.processing_boundary = Config().get('global_parameters', 'processing_boundary')
        self.start_from_scratch = eval(Config().get('start_from_scratch', 'start_from_scratch'))
        self.randomize_smooth_surfaces = eval(Config().get('surface_parameters', 'randomize_smooth_areas_in_dem'))
        self.original_dem = Config().get('surface_parameters', 'dem')

        self.surface_folder = os.path.join(self.output_location, 'surface')
        self.hydrology_folder = os.path.join(self.output_location, 'hydrology')
        self.extent = os.path.join(self.output_location, 'extent.shp')

    def extent_run(self):

        if arcpy.Exists(self.output_location + '/extent.shp'):
            return

        self.output_location = Config().get('global_parameters', 'output_location')
        temp_space = WorkspaceWorker(self.output_location).create()
        WorkspaceWorker(temp_space).delete()

        temp_space = WorkspaceWorker(self.surface_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        print '...creating processing extent'

        # arcpy.Buffer_analysis(self.processing_boundary, 'extent_buf', '2000 Meters', dissolve_option='ALL')
        # arcpy.Dissolve_management('extent_buf.shp', self.extent, multi_part='SINGLE_PART')
        arcpy.Copy_management(self.processing_boundary, self.extent)
        arcpy.RepairGeometry_management(self.extent)

        WorkspaceWorker(temp_space).delete()

    def raw_dem_preparation(self):

        temp_space = WorkspaceWorker(self.surface_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        if arcpy.Exists(self.surface_folder + '/raw_dem'):
            return

        print '...clipping DEM to processing boundary'
        arcpy.gp.ExtractByMask_sa(self.original_dem, self.output_location + '/extent.shp', self.surface_folder + '/raw_dem')

    def culvert_burn(self):

        culvert_dem = os.path.join(self.surface_folder, 'culvert_dem')
        culvert_burn = eval(Config().get('surface_parameters', 'burn_in_culverts'))
        dem = self.surface_folder + '/raw_dem'
        if not culvert_burn:
            return dem

        if arcpy.Exists(culvert_dem):
            return culvert_dem

        print '...burning culverts into DEM'
        temp_space = WorkspaceWorker(self.surface_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        arcpy.env.extent = self.extent
        arcpy.env.cellsize = dem

        culverts_2_burn = Config().get('surface_parameters', 'culverts')

        if not arcpy.Exists(culverts_2_burn):
            print ('If you are going to turn on culvert breaching, define a culvert breaching layer!')
            sys.exit()

        arcpy.gp.ExtractByMask_sa(dem, culverts_2_burn, "cul_dem")

        divide = Int(Raster("cul_dem") / Raster("cul_dem"))
        arcpy.gp.RegionGroup_sa(divide, "Cul_Reg", "EIGHT", "WITHIN")
        del divide

        arcpy.gp.ZonalStatistics_sa("cul_Reg", "VALUE", "cul_dem", "Cul_Zone", "MINIMUM", "DATA")
        times = Int(Raster("Cul_Zone") * 10000)
        arcpy.RasterToPolygon_conversion(times, "CUL_ZONE", "", "VALUE")
        del times

        arcpy.Union_analysis(["CUL_ZONE.SHP", self.output_location + '/extent.shp'], "UNION")
        arcpy.PolygonToRaster_conversion("UNION.shp", "VALUE", "BURN", "", "", dem)
        divide = Float(Raster("Burn")) / 10000
        ras = Con(divide > 0, divide, Raster(dem))
        ras.save(culvert_dem)
        dem = culvert_dem

        WorkspaceWorker(temp_space).delete()

        return dem

    def burn_in_hydrological_features(self, dem):

        hydro_burn = eval(Config().get('surface_parameters', 'burn_in_false_hydrology'))
        wet_areas = Config().get('surface_parameters', 'wet_areas')
        open_water = Config().get('surface_parameters', 'open_water')

        if not hydro_burn:
            return dem

        hydroburn_dem = os.path.join(self.surface_folder, 'hydroburn_dem')
        if arcpy.Exists(hydroburn_dem):
            return hydroburn_dem

        print ('...burning hydrology into DEM')
        temp_space = WorkspaceWorker(self.surface_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        arcpy.env.extent = self.extent
        arcpy.env.cellsize = dem

        hydrology = os.path.join(self.hydrology_folder, 'merged_hydrology.shp')
        wet_area_layer = arcpy.Exists(wet_areas)
        open_water_layer = arcpy.Exists(open_water)
        if wet_area_layer and open_water_layer:
            if not arcpy.Exists(hydrology):
                arcpy.Merge_management([wet_areas, open_water], hydrology)
        elif wet_area_layer:
            hydrology = wet_areas
        else:
            hydrology = open_water

        hydrologic_polygons = self.false_hydro_polygons(hydrology, dem)
        dem = self.false_streams(dem, temp_space)
        dem = self.hydro_feature_burn(dem, hydrologic_polygons, temp_space)

        WorkspaceWorker(temp_space).delete()

        return dem

    def false_streams(self, dem, temp_space):

        out_dem = self.surface_folder + '/st_dem'
        if arcpy.Exists(out_dem):
            return out_dem

        streams = Config().get('surface_parameters', 'stream_channels')
        if not arcpy.Exists(streams):
            return dem

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        arcpy.env.extent = self.extent
        arcpy.env.cellsize = dem

        # create stream rings to ensure their consideration within the TMI process
        arcpy.Buffer_analysis(streams, 'streams_buffered', '3 Meters', '', '', 'ALL')
        arcpy.PolygonToLine_management('streams_buffered.shp', self.surface_folder + '/streams_as_rings')

        arcpy.gp.ExtractByMask_sa(dem, 'streams_buffered.shp', "st_dem")
        divide = Int(Raster("st_dem") / Raster("st_dem"))
        arcpy.gp.RegionGroup_sa(divide, "st_Reg", "EIGHT", "WITHIN", "NO_LINK")
        del divide

        arcpy.gp.ZonalStatistics_sa("st_Reg", "VALUE", "st_dem", "st_Zone", "MINIMUM", "DATA")
        times = Int(Raster("st_Zone") * 10000)
        arcpy.RasterToPolygon_conversion(times, "st_ZONE_shp", "", "VALUE")
        del times

        arcpy.Union_analysis(["st_ZONE_shp", self.output_location + '/extent.shp'], "UNION")
        arcpy.PolygonToRaster_conversion("UNION", "GRIDCODE", "BURN", "", "", dem)
        divide = Float(Raster("Burn")) / 10000
        ras = Con(divide > 0, divide, dem)
        ras.save(out_dem)

        return out_dem

    def false_hydro_polygons(self, hydrology, cellsize):
        """
        If the provided hydrology layer is false (not created considering topology; usually
        provided by mine operator), and it must be forced into the landscape (its boundaries
        are to be maintained and not be corrected to topologically) then process this step. Un-corrected
        features (setting false_hydro = False) will result in Moisture Regime predictions
        following the contours of the land, resulting in less wet area predictions under these
        zones
        """

        temp_space = WorkspaceWorker(self.hydrology_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        arcpy.env.extent = self.extent
        arcpy.env.cellsize = cellsize

        hydrology_features_as_lines = os.path.join(self.hydrology_folder, 'hydrology_features_as_lines.shp')
        arcpy.PolygonToLine_management(hydrology, hydrology_features_as_lines)
        arcpy.gp.EucDistance_sa(hydrology_features_as_lines, "hyd_feat_euc", "", cellsize)
        arcpy.gp.FocalStatistics_sa("hyd_feat_euc", "std", "Rectangle 15 15 CELL", "STD", "DATA")
        arcpy.gp.Reclassify_sa("std", "Value", "0 3.7 1;3.7 5000 NoData", "hydro_std", "DATA")
        arcpy.Buffer_analysis(hydrology, "hyd_buf", "-10 Meters", "", "", "ALL")
        arcpy.gp.ExtractByMask_sa("hydro_std", "hyd_buf.shp", "hyd_stream")
        arcpy.gp.EucDistance_sa("hyd_stream", "hyd_euc", "", cellsize)
        arcpy.gp.Slope_sa(self.surface_folder + '/raw_dem', "SLOPE", "DEGREE")
        euc_max = (arcpy.GetRasterProperties_management("hyd_EUC", "MAXIMUM")).getOutput(0)
        slp_max = (arcpy.GetRasterProperties_management("slope", "MAXIMUM")).getOutput(0)
        ras = (Raster("hyd_EUC") / float(euc_max)) * (Raster("SLOPE") / float(slp_max)) * 1000
        ras.save('Hydro')

        arcpy.gp.ExtractByMask_sa('Hydro', "hyd_buf.shp", "hydro_feat")
        arcpy.gp.Reclassify_sa("hydro_feat", "Value", "0 0.025 1;0.025 5000 NoData", "hydro_class", "DATA")
        arcpy.RasterToPolygon_conversion("hydro_class", "hyd_ZONE", "", "VALUE")
        arcpy.Buffer_analysis("hyd_ZONE.shp", "hyd_buf2", "10 Meters", "", "", "ALL")
        arcpy.Buffer_analysis("hyd_buf2.shp", "hyd_3", "-10 Meters", "", "", "ALL")
        arcpy.Dissolve_management("hyd_3.shp", "hyd_4", "", "", "SINGLE_PART")
        arcpy.AddField_management("hyd_4.shp", "AREA_ha", "DOUBLE")
        arcpy.CalculateField_management("hyd_4.shp", "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
        arcpy.FeatureClassToFeatureClass_conversion("hyd_4.shp", self.hydrology_folder, 'hydrology_to_burn', '"AREA_ha" >= 0.5')

        if int(arcpy.GetCount_management(self.hydrology_folder + "/hydrology_to_burn.shp").getOutput(0)) == 0:
            raise Exception('Something went wrong with the determination of hydrologic polygons to burn in')

        WorkspaceWorker(temp_space).delete()

        return os.path.join(self.hydrology_folder, "hydrology_to_burn.shp")

    def hydro_feature_burn(self, dem, hydrologic_polygons, temp_space):
        """
        This step either burns in the corrected hydrology features or the un-corrected features. If un-corrected
        expect that when Moisture Regime is predicted that areas under the hydrological features will be dry.
        """

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        arcpy.env.extent = self.extent
        arcpy.env.cellsize = dem

        hydroburn_dem = os.path.join(self.surface_folder, 'hydroburn_dem')
        if arcpy.Exists(hydroburn_dem):
            return dem

        print "...burning in hydrological polygons @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        arcpy.PolygonToRaster_conversion(hydrologic_polygons, out_rasterdataset=self.hydrology_folder + '/Hydro_ras', cellsize=dem)
        arcpy.gp.Shrink_sa(self.hydrology_folder + '/Hydro_ras', "Feat_BURN2", "5", [1])
        #TODO check to see if the 5 here is affecting results too much (i.e., 5m versus 25m with varying cell sizes
        ras = Con(Raster("Feat_BURN2") == 1, 0, Raster(self.hydrology_folder + '/Hydro_ras'))

        water_feature_polygon_linework = self.hydrology_folder + '/water_feature_polygon_linework.shp'
        arcpy.RasterToPolyline_conversion(ras, water_feature_polygon_linework, "ZERO", "0", "SIMPLIFY", "VALUE")
        del ras

        arcpy.gp.RegionGroup_sa(divide, "hyd_Reg", "EIGHT", "WITHIN", "NO_LINK")
        arcpy.gp.ZonalStatistics_sa("hyd_Reg", "VALUE", "hyd_DEM", "hyd_Zone2", "MINIMUM", "DATA")
        times = Int(Raster("hyd_Zone2") * 10000)
        arcpy.RasterToPolygon_conversion(times, "hyd_ZONE_shp2", "", "VALUE")
        arcpy.Union_analysis(["hyd_ZONE_shp2.shp", self.output_location + '/extent.shp'], "UNION2")
        arcpy.PolygonToRaster_conversion("UNION2.shp", "GRIDCODE", "Feat_BURN", "", "", dem)
        divide = Float(Raster("Feat_Burn")) / 10000
        ras = Con(divide > 0, divide, dem)
        ras.save(hydroburn_dem)

        WorkspaceWorker(self.hydrology_folder).delete()

        return hydroburn_dem

    def create_base_rasters(self, dem):

        temp_space = WorkspaceWorker(self.surface_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        arcpy.env.extent = self.extent

        if arcpy.Exists(self.surface_folder + '/smooth_dem'):
            return

        print "...creating general surface metrics"

        arcpy.gp.FocalStatistics_sa(self.surface_folder + '/raw_dem', self.surface_folder + '/smooth_dem', "Rectangle 5 5 CELL", "MEAN", "DATA")
        arcpy.gp.Slope_sa(self.surface_folder + '/smooth_dem', self.surface_folder + '/slope_pct', "PERCENT_RISE")
        arcpy.gp.Slope_sa(self.surface_folder + '/smooth_dem', self.surface_folder + '/slope_dgr', "DEGREE")
        arcpy.gp.FocalStatistics_sa(self.surface_folder + '/slope_dgr', self.surface_folder + '/slope_std', "Rectangle 5 5 CELL", "STD", "DATA")

        if self.randomize_smooth_surfaces:
            cellsize = int((arcpy.GetRasterProperties_management(self.surface_folder + '/raw_dem', "CELLSIZEX")).getOutput(0))
            arcpy.CreateRandomRaster_management(temp_space, "random_ras", "UNIFORM -0.1 0.1", raster_extent=dem, cellsize=cellsize)
            add = Raster("random_ras") + Raster(dem)
            random_dem = Con(Raster(self.surface_folder + '/slope_std') < 0.1, add, dem)
            arcpy.gp.FocalStatistics_sa(random_dem, self.surface_folder + '/hydro_dem', "Rectangle 3 3 CELL", "MEAN", "DATA")
            arcpy.gp.Slope_sa(self.surface_folder + '/hydro_dem', self.surface_folder + '/slope_tmi', "DEGREE")
        else:
            arcpy.CopyRaster_management(dem, self.surface_folder + '/hydro_dem')
            arcpy.CopyRaster_management(self.surface_folder + '/slope_dgr', self.surface_folder + '/slope_tmi')

        arcpy.gp.HillShade_sa(self.surface_folder + '/smooth_dem', self.surface_folder + '/hillshade', "315", "45", "NO_SHADOWS")

        arcpy.gp.Aspect_sa(self.surface_folder + '/smooth_dem', self.surface_folder + '/aspect')

        WorkspaceWorker(temp_space).delete()
