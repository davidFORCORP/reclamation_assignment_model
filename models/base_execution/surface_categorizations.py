import os
from datetime import datetime
from time import time
import arcpy
import sys
from core.config import Config
from arcpy.sa import *

from core.reused_functions import WorkspaceWorker, CleanAndRepair, CleanRaster, HelperFunctions

class TopographicClassifications():

    def __init__(self):

        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True

        self.start_from_scratch = eval(Config().get('start_from_scratch', 'start_from_scratch'))
        self.output_location = Config().get('global_parameters', 'output_location')

        self.surface_folder = os.path.join(self.output_location, 'surface')
        self.hydrology_folder = os.path.join(self.output_location, 'hydrology')
        self.surface = self.surface_folder + '/smooth_dem'
        self.tpi_gridcode_conversion = dict({1: 'depression', 2: 'level', 3: 'toe', 4: 'lower slope', 5: 'mid slope', 6: 'upper slope', 7: 'crest'})

        if not arcpy.Exists(self.surface):
            raise Exception('There is no surface to use. Are you doing things in order?')

    def slope(self):
                    
        if arcpy.Exists(self.output_location + '/sl.shp'):
            return
        
        print "...creating slope classifications @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.surface_folder).create()

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        
        arcpy.gp.Reclassify_sa(self.surface_folder + '/slope_dgr', "Value", "0 2 1;2 6 2;6 10 3;10 16 4; 16 31 5;31 46 6;46 71 7; 71 100 8", 'slpcls', "DATA")

        result = CleanRaster(temp_space).run('slpcls', 3, specific_values=[1, 2])
        result = CleanRaster(temp_space).run(result, 2, specific_values=[3, 4])
        result = CleanRaster(temp_space).run(result, 1, specific_values=[5, 6, 7, 8])
        arcpy.CopyRaster_management(result, self.surface_folder + '/slpcls')

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        arcpy.RasterToPolygon_conversion(self.surface_folder + '/slpcls', "temp_cls")
        eliminated_shape = CleanAndRepair(temp_space).run('temp_cls.shp')

        arcpy.AddField_management(eliminated_shape, "SLClass", "TEXT", "", "", "25", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(eliminated_shape, "SL_GRID", "SHORT", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        gridcode_conversion = dict({1: 'level', 2: '2 - 5', 3: '6 - 9', 4: '10 - 15', 5: '16 - 30', 6: '31 - 45', 7: '46 - 70', 8: '70+'})
        with arcpy.da.UpdateCursor(eliminated_shape, ['gridcode', 'slclass', 'sl_grid']) as cursor:
            for row in cursor:
                row[1] = gridcode_conversion[row[0]]
                row[2] = row[0]
                cursor.updateRow(row)
        del cursor

        arcpy.Dissolve_management(eliminated_shape, os.path.join(self.output_location, 'sl'), ["SLCLASS", "SL_GRID"], "", "SINGLE_PART")

        WorkspaceWorker(temp_space).delete()

    def aspect(self):

        if arcpy.Exists(self.output_location + '/as.shp'):
            return
        if not arcpy.Exists(self.surface_folder + '/slpcls'):
            sys.exit('RUN SLOPE')
            
        print "...creating aspect classification @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.surface_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        arcpy.gp.Reclassify_sa(self.surface_folder + '/aspect', "Value", "-1 0 1;0 45 2;45 135 3;135 225 4;225 315 5;315 360 2", 'aspect_clsa', "DATA")
        arcpy.gp.Con_sa(self.surface_folder + '/slpcls', "0",  'aspect_clsb', 'aspect_clsa', "\"VALUE\" = 1")
        
        result = CleanRaster(temp_space).run('aspect_clsa', 3, specific_values=[1])
        result = CleanRaster(temp_space).run(result, 2, specific_values=[2, 3, 4, 5, 6])
        result = CleanRaster(temp_space).run(result, 3, specific_values=[1])
        result = CleanRaster(temp_space).run(result, 2, specific_values=[2, 3, 4, 5, 6])

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        arcpy.RasterToPolygon_conversion(result, "temp_cls")
        eliminated_shape = CleanAndRepair(temp_space).run('temp_cls.shp')

        gridcode_conversion = dict({0: 'level', 1: 'level', 2: 'northerly', 3: 'easterly', 4: 'southerly', 5: 'westerly'})
        arcpy.AddField_management(eliminated_shape, "asClass", "TEXT")
        arcpy.AddField_management(eliminated_shape, "AS_GRID", "SHORT")
        with arcpy.da.UpdateCursor(eliminated_shape, ['gridcode', 'asClass', 'AS_GRID']) as cursor:
            for row in cursor:
                row[1] = gridcode_conversion[row[0]]
                row[2] = row[0]
                cursor.updateRow(row)
        del cursor
        arcpy.Dissolve_management(eliminated_shape, os.path.join(self.output_location, 'as'), ["ASCLASS", "AS_GRID"], "", "SINGLE_PART")
        
    def topographic_position(self):

        if arcpy.Exists(self.output_location + '/tp.shp'):
            return
        if not arcpy.Exists(self.surface_folder + '/slpcls'):
            sys.exit('RUN SLOPE')
        print "...creating topographic classification @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.surface_folder).create()

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        tpi_ok = eval(Config().get('tpi_ok', 'tpi_ok'))
        get_key = HelperFunctions()
        gridcode = self.tpi_gridcode_conversion

        if not tpi_ok or not arcpy.Exists('tpi_smash.tif'):

            # gather locations where we will be appending TPI data from the large focal
            layer = 'layer.lyr'
            arcpy.MakeFeatureLayer_management(self.output_location + '/sl.shp', layer)
            arcpy.SelectLayerByAttribute_management(layer, 'NEW_SELECTION', '"SL_GRID" >= 2')
            arcpy.Dissolve_management(layer, 'steeper_slopes')
            arcpy.Buffer_analysis('steeper_slopes.shp', 'window_finding', '-50 Meters')
            arcpy.RepairGeometry_management('window_finding.shp')
            arcpy.Buffer_analysis('window_finding.shp', 'window', '50 Meters')
            arcpy.Intersect_analysis(['steeper_slopes.shp', 'window.shp'], 'application_window')
            arcpy.FeatureToRaster_conversion('application_window.shp', 'FID', 'lrg_apply', '5')

            # create TPI
            arcpy.gp.FocalStatistics_sa(self.surface, 'small_radi', "Circle 15 CELL", "MEAN", "DATA")
            arcpy.Resample_management(self.surface, 'dem_rasamp', '9')
            arcpy.gp.FocalStatistics_sa('dem_rasamp', 'large_radi', "Circle 81 CELL", "MEAN", "DATA")
            small_tpi = Raster(self.surface) - Raster('small_radi')
            small_tpi.save('tpi_small')
            arcpy.env.cellsize = 1
            large_tpi = Raster(self.surface) - Raster('large_radi')
            large_tpi.save('tpi_large')

            # classify TPI
            small_radi_tpi_reclass = "-9999 -0.2 4;-0.2 -0.015 5;-0.015 0.09 5;0.09 0.25 6;0.25 99999 6"
            large_radi_tpi_reclass = "-5000 -2 4; -2 8 5; 8 5000 6"
            arcpy.gp.Reclassify_sa(small_tpi, "VALUE", small_radi_tpi_reclass, "tpi_scls", "DATA")
            arcpy.gp.Reclassify_sa(large_tpi, "VALUE", large_radi_tpi_reclass, "tpi_lcls", "DATA")

            slope = Raster(os.path.join(self.surface_folder, 'slope_dgr'))

            tpi_smash = Con(slope <= 0.5, get_key.get_key(gridcode, 'level'),
                        Con(IsNull(Raster('lrg_apply')) != 1, Raster("tpi_lcls"),
                            Con(slope <= 1,
                                    Con(Raster("tpi_scls") > 2, Raster("tpi_scls") - 1, Raster("tpi_scls")),
                                Raster("tpi_scls"))))

            tpi_smash.save('tpi_smash.tif')

            if not tpi_ok:
                sys.exit('REVIEW "' + temp_space + '/tpi_smash.tif" AND EDIT "tpi_ok" TO SUIT')

        elif not tpi_ok:
            sys.exit('REVIEW "' + temp_space + '/tpi_smash.tif" AND EDIT "tpi_ok" TO SUIT')

        # clean TPI
        tpi = CleanRaster(temp_space).run(os.path.join(temp_space, 'tpi_smash.tif'))
        # need to reset workspaces
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        arcpy.gp.RegionGroup_sa(tpi, 'tpi_region.tif', "EIGHT", "WITHIN")
        arcpy.CalculateStatistics_management('tpi_region.tif')

        arcpy.gp.ZonalGeometry_sa('tpi_region.tif', "VALUE", 'tpi_zonal', "AREA", "1")

        arcpy.gp.SetNull('tpi_zonal', tpi, "tpi_null", 'VALUE < 10')
        del tpi

        arcpy.gp.FocalStatistics_sa("tpi_null", "tpi_simplify1", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("tpi_simplify1", "tpi_simplify2", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("tpi_simplify2", "tpi_simplify3", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("tpi_simplify3", "tpi_simplify4", "Rectangle 5 5 CELL", "MAJORITY", "DATA")

        ras = Con(IsNull(Raster("tpi_null")) == 1, Raster("tpi_simplify4"), Raster("tpi_null"))
        ras.save("tpi_focal")
        del ras
        arcpy.gp.FocalStatistics_sa("tpi_focal", "tpi_simplify5", "Rectangle 5 5 CELL", "MEDIAN", "DATA")
        ras = Con(IsNull(Raster("tpi_focal")) == 1, Raster("tpi_simplify5"), Raster("tpi_focal"))
        ras.save("tpi_simplify")
        del ras

        arcpy.RasterToPolygon_conversion("tpi_simplify", "tpi_simplify")
        arcpy.RepairGeometry_management('tpi_simplify.shp')
        eliminated_shape = CleanAndRepair(temp_space).run('tpi_simplify.shp', 100)
        eliminated_shape = CleanAndRepair(temp_space).run(eliminated_shape, 1000)

        arcpy.AddField_management(eliminated_shape, "TPClass", "TEXT")
        arcpy.AddField_management(eliminated_shape, "TP_GRID", "SHORT")
        with arcpy.da.UpdateCursor(eliminated_shape, ['gridcode', 'TPClass', 'TP_GRID']) as cursor:
            for row in cursor:
                row[1] = gridcode[row[0]]
                row[2] = row[0]
                cursor.updateRow(row)
        del cursor
        arcpy.Dissolve_management(eliminated_shape, self.output_location + '/tp.shp', ['TPClass', 'TP_GRID'])

        WorkspaceWorker(temp_space).delete()

        return

    def moisture_regime(self):

        temp_space = WorkspaceWorker(self.hydrology_folder).create()

        result = CleanRaster(temp_space).run(os.path.join(self.hydrology_folder, 'tmi_cls.tif'), 3, specific_values=[5])
        result = CleanRaster(temp_space).run(result, 2, specific_values=[6, 7, 8, 4, 3, 2], specific_order=True)
        result = CleanRaster(temp_space).run(result, 1, specific_values=[1])

        arcpy.gp.RegionGroup_sa(result, 'mr_rg', "EIGHT", "WITHIN")
        arcpy.gp.ZonalGeometry_sa("mr_rg", "VALUE", "mr_rg_zg", "AREA", "1")
        arcpy.gp.SetNull("mr_rg_zg", result, "mr_setnull", 'VALUE < 10')
        arcpy.gp.FocalStatistics_sa(result, "MAJ1", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("MAJ1", "MAJ2", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("MAJ2", "MAJ3", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("MAJ3", "MAJ4", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.Int_sa("mr_setnull", "mr_setnulli")
        arcpy.gp.Int_sa("MAJ4", "MAJint")
        ras = Con(IsNull(Raster("mr_setnulli")) == 1, Raster("MAJ4"), Raster("mr_setnulli"))
        ras.save("con_result")
        arcpy.gp.FocalStatistics_sa("con_result", "Maj5", "Rectangle 5 5 CELL", "MEDIAN", "DATA")
        ras2 = Con(IsNull(Raster("con_result")) == 1, Raster("Maj5"), Raster("con_result"))
        ras2.save("MR99")

        arcpy.RasterToPolygon_conversion('MR99', "temp_cls")
        eliminated_shape = CleanAndRepair(temp_space).run('temp_cls.shp', area_in_meters_to_remove=100)
        eliminated_shape = CleanAndRepair(temp_space).run(eliminated_shape)

        gridcode_conversion = dict({1: 'hydric', 2: 'sub hydric', 3: 'hygric', 4: 'sub hygric', 5: 'mesic', 6: 'sub mesic', 7: 'sub xeric', 8: 'xeric'})
        arcpy.AddField_management(eliminated_shape, "MRClass", "TEXT")
        arcpy.AddField_management(eliminated_shape, "MR_GRID", "SHORT")
        with arcpy.da.UpdateCursor(eliminated_shape, ['gridcode', 'MRClass', 'MR_GRID']) as cursor:
            for row in cursor:
                row[1] = gridcode_conversion[row[0]]
                row[2] = row[0]
                cursor.updateRow(row)
        del cursor
        arcpy.Dissolve_management(eliminated_shape, os.path.join(self.output_location, 'mr'), ["MRClass", "MR_GRID"], "", "SINGLE_PART")
