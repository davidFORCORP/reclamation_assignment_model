import os
from datetime import datetime
from time import time
import arcpy
import sys
from core.config import Config
from arcpy.sa import *
from models.base_execution.substrate_preparation import SubstratePreparation
from core.reused_functions import WorkspaceWorker, CleanAndRepair


class HydrologyCreation():

    def __init__(self):

        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True

        self.output_location = Config().get('global_parameters', 'output_location')
        self.surface_folder = os.path.join(self.output_location, 'surface')
        self.hydrology_folder = os.path.join(self.output_location, 'hydrology')
        self.substrate_folder = os.path.join(self.output_location, 'substrate')

        self.defined_excelerated_seepage = Config().get('moisture_regime_parameters', 'defined_accelerated_seepage_areas')
        self.wet_areas = Config().get('surface_parameters', 'wet_areas')

        self.flow_initiation_cutoff = float(Config().get('moisture_regime_parameters', 'flow_initiation_cutoff'))
        self.start_from_scratch = eval(Config().get('start_from_scratch', 'start_from_scratch'))
        self.use_existing_sai = eval(Config().get('substrate_parameters', 'use_existing_sai'))
        self.introduce_sai = eval(Config().get('substrate_parameters', 'introduce_sai'))
        self.moisture_regime_classification_ok = eval(Config().get('moisture_regime_parameters', 'moisture_class_ok'))
        self.substrate = Config().get('substrate_parameters', 'substrate')

        self.cellsize = arcpy.GetRasterProperties_management(self.surface_folder + '/hydro_dem', "CELLSIZEX").getOutput(0)
        arcpy.env.extent = os.path.join(self.output_location, 'extent.shp')
        arcpy.env.cellsize = self.cellsize
        arcpy.env.overwriteOutput = True

    def base_data(self):

        flow_accumulation = self.hydrology_folder + '/flow_acc'
        flow_direction = self.hydrology_folder + '/flow_dir'
        hydrological_dem = self.surface_folder + '/hydro_dem'

        if arcpy.Exists(flow_accumulation):
            return flow_accumulation, flow_direction

        print "...creating base data for hydrological modeling @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.hydrology_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        arcpy.gp.Fill(hydrological_dem, self.hydrology_folder + '/fill')
        arcpy.gp.FlowDirection_sa(self.hydrology_folder + '/fill', flow_direction, "NORMAL")
        arcpy.gp.FlowAccumulation_sa(self.hydrology_folder + '/flow_dir', flow_accumulation, "", "FLOAT")
        arcpy.CalculateStatistics_management(flow_accumulation)

        WorkspaceWorker(temp_space).delete()

        return flow_accumulation, flow_direction

    def create_topographic_moisture_index(self):

        print "...creating the topographic moisture index @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        if arcpy.Exists(self.hydrology_folder + '/tmi'):
            return

        temp_space = WorkspaceWorker(self.hydrology_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        hydrology = None

        slope = self.surface_folder + '/slope_tmi'
        euclidean_streams = self.hydrology_folder + "/euc_streams"
        hydrology_raster = self.hydrology_folder + '/hydro_ras'
        sai = os.path.join(self.substrate_folder, 'sai')

        slp_max = float(arcpy.GetRasterProperties_management(slope, "MAXIMUM").getOutput(0))
        ed_max = float(arcpy.GetRasterProperties_management(euclidean_streams, "MAXIMUM").getOutput(0))
        euclidean_streams_process = Float(Raster(euclidean_streams)) / ed_max
        slope = Raster(slope) / slp_max + 0.1

        # if self.introduce_sai:
        #     sai_modified = SubstratePreparation().modify_sai(temp_space, sai_raster)
        #     sai = Raster(sai_modified)
        #     slope = (sai * slope) + slope
        #     euclidean_streams_process = (sai * euclidean_streams_process) + euclidean_streams_process
        tmi = slope * euclidean_streams_process
        del slope, euclidean_streams

        if arcpy.Exists(hydrology_raster):
            hydrology = Raster(hydrology_raster)

        if not self.introduce_sai and not hydrology:
            print '......producing TMI\n         SAI=False\n         Hydrology=False'
            moisture = tmi * float(1000.0)
        elif not hydrology:
            print '......producing TMI \n         SAI=True\n         Hydrology=False'
            moisture = tmi * sai * float(1000.0)
        elif not self.introduce_sai:
            print '......producing TMI \n         SAI=False\n        Hydrology=True'
            moisture = Con(hydrology == 1, 0, tmi * float(1000.0))
        else:
            print '......producing TMI \n         SAI=True\n         Hydrology=True'
            moisture = (tmi + Power(tmi * sai, 2)) * float(1000.0)
        moisture = Con(moisture > 1000, 1000, Con(moisture < 0, 0, moisture))
        moisture.save(self.hydrology_folder + '/tmi')

    def tmi_to_tmr(self):

        print('...converting TMI to TMR')

        mr_reclass = "-99999 0.002 1;0.002 0.01 2; 0.01 0.025 3; 0.025 0.1 4; 0.1 2 5; 2 5 6;5 15 7;15 99999 8"
        if not self.moisture_regime_classification_ok:
            arcpy.gp.Reclassify_sa(self.hydrology_folder + '/tmi', "Value", mr_reclass, self.hydrology_folder + '/tmi_cls.tif', "DATA")
            sys.exit('REVIEW "' + self.hydrology_folder + '/tmi_cls.tif AND EDIT "moisture_class_ok" TO SUIT')

        tmicls = Raster(self.hydrology_folder + '/tmi_cls.tif')
        slope = Raster(self.surface_folder + '/slpcls')
        repair_slope = Con(tmicls <= 2, 1, slope)
        del slope
        repair_slope.save()



    def adjust_flow_accumulation(self, flow_accumulation, sai):

        flow_initiation_cutoff = self.flow_initiation_cutoff
        if not self.introduce_sai:
            print '...no adjustments to flow accumulation enabled'
            return flow_accumulation, flow_initiation_cutoff

        if arcpy.Exists(self.hydrology_folder + '/fa_adjusted'):
            return flow_accumulation, flow_initiation_cutoff

        direct_awhc_modifications_to_flow_accumulation = eval(Config().get('substrate_parameters', 'direct_awhc_modifications_to_flow_accumulation'))
        if not direct_awhc_modifications_to_flow_accumulation:
            print "...adjusting flow accumulation with provided substrate data @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
            adjusted_fa, flow_initiation_cutoff = SubstratePreparation().modify_flow_accumulation(sai, flow_accumulation, flow_initiation_cutoff)
        else:
            print "...adjusting flow accumulation with provided substrate data and AWHC limitation information @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
            adjusted_fa, flow_initiation_cutoff = SubstratePreparation().modify_flow_accumulation(sai, flow_accumulation, flow_initiation_cutoff, modifier=True)

        return adjusted_fa, flow_initiation_cutoff

    def create_and_assess_streams(self, flow_accumulation, flow_direction, flow_initiation_cutoff):

        return_streams = os.path.join(self.hydrology_folder, 'stream_order.shp')
        if arcpy.Exists(return_streams):
            return

        print "...creating streams @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.hydrology_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        streams_raster_fa = os.path.join(self.hydrology_folder, 'stream_fa.tif')
        streams_raster_boolean = os.path.join(self.hydrology_folder, 'stream_bool.tif')
        predicted_streams = os.path.join(self.hydrology_folder, 'streams.shp')
        flow_accumulation = Raster(flow_accumulation)

        stream_from_fa = Con(flow_accumulation - (flow_accumulation * Raster(self.substrate_folder + '/sai')) > float(flow_initiation_cutoff), 1, 0)
        # stream_fa = Con(flow_accumulation > flow_initiation_cutoff, flow_accumulation)
        # stream_fa.save(streams_raster_fa)
        # stream_from_fa = Con(flow_accumulation > flow_initiation_cutoff, 1)
        stream_from_fa.save(streams_raster_boolean)
        arcpy.RasterToPolyline_conversion(streams_raster_boolean, predicted_streams, "ZERO", "0", "SIMPLIFY", "VALUE")

        stream_order = 'stream_order'
        arcpy.gp.StreamOrder_sa(streams_raster_boolean, flow_direction, stream_order, "STRAHLER")

        stream_list = [predicted_streams]
        if arcpy.Exists(self.surface_folder + '/water_feature_polygon_linework.shp'):
            stream_list.append(self.surface_folder + '/water_feature_polygon_linework.shp')
        if arcpy.Exists(self.surface_folder + '/streams_as_rings.shp'):
            stream_list.append(self.surface_folder + '/streams_as_rings.shp')
        if len(stream_list) > 1:
            arcpy.Merge_management(stream_list, "processing_streams")
            streams = "processing_streams.shp"
        else:
            streams = predicted_streams

        arcpy.gp.EucDistance_sa(streams, "euclid_1", "", flow_accumulation)
        arcpy.gp.ExtractByMask_sa("euclid_1", self.output_location + '/extent.shp', self.hydrology_folder + "/euc_streams")

        if arcpy.Exists(self.defined_excelerated_seepage):
            print '...... accelerated seepage has been defined'
            # This makes the stream order be subtracted by X (in this case 2). This limits the streams which get the flood plain calculation.
            arcpy.PolygonToRaster_conversion(self.defined_excelerated_seepage, "FID", "FLD_Minus", "", "", flow_accumulation)
            ras = Con((Raster(stream_order) - (Con(Raster("FLD_Minus") == 1, 2, 0))) < 0, 0, (Raster("stream_order") - (Con(Raster("FLD_Minus") == 1, 2, 0))))
            ras.save("order_adjust")
            stream_order = "order_adjust"

        arcpy.RasterToPolyline_conversion(stream_order, "pre_stream_order", "ZERO", "0", "SIMPLIFY", "VALUE")
        arcpy.TrimLine_edit("pre_stream_order.shp", "50 Meters")
        arcpy.Dissolve_management("pre_stream_order.shp", return_streams, "GRID_CODE", "", "MULTI_PART")

        WorkspaceWorker(temp_space).delete()

        return

    def predict_increased_moisture(self):

        stream_order_shp = os.path.join(self.hydrology_folder, 'stream_order.shp')
        if arcpy.Exists(self.hydrology_folder + "/increase_mst"):
            return

        hydrology_features_as_lines = os.path.join(self.hydrology_folder, 'hydrology_features_as_lines.shp')
        dem = os.path.join(self.surface_folder, 'smooth_dem')
        slope_std = os.path.join(self.surface_folder, 'slope_std')

        print "...predicting areas with increased moisture @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.hydrology_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        settings_dict = eval(Config().get('moisture_regime_parameters', 'stream_order_flooding_factors'))
        flooding_factors = dict(settings_dict)

        order_as_raster = list()
        number_of_orders_to_process = int(arcpy.GetCount_management(stream_order_shp).getOutput(0))

        dem = Int(Raster(dem) * 100)
        with arcpy.da.SearchCursor(stream_order_shp, ['grid_code', 'SHAPE@'], sql_clause=(None, 'ORDER BY grid_code DESC')) as cursor:
            for i, row in enumerate(cursor):
                print '......working on stream order {} of {}'.format(str(i+1), str(number_of_orders_to_process))
                stream_order_layer = row[1]
                hydrology_features_as_lines_layer = arcpy.Exists(hydrology_features_as_lines)
                if i == number_of_orders_to_process-1 and hydrology_features_as_lines_layer:  # include water water features in the flood plain calculation
                    arcpy.Merge_management([hydrology_features_as_lines, arcpy.Polygon(row[1])], "stream_order_being_processed")
                    stream_order_layer = 'stream_order_being_processed.shp'

                arcpy.gp.ExtractByMask_sa(dem, stream_order_layer, "odem")
                euc = dem - (EucAllocation(Raster("odem"), "", "", self.cellsize, "Value", "", ""))

                factor = flooding_factors[row[0]]
                calculation = Con(Raster(slope_std) < 1, Con(euc > factor * -100, Con(euc < factor * 100, 1)))
                arcpy.RasterToPolygon_conversion(calculation, 'flood_bust' + str(row[0]))

                arcpy.MakeFeatureLayer_management("flood_bust{}.shp".format(str(row[0])), 'layer.lyr')
                arcpy.SelectLayerByLocation_management('layer.lyr', "INTERSECT", stream_order_layer)
                if arcpy.GetCount_management('layer.lyr').getOutput(0) == 0:
                    continue
                new_shape = CleanAndRepair(temp_space).buffer_quick('layer.lyr', 10, 'IN_FIRST')
                if not new_shape:
                    continue

                arcpy.FeatureToRaster_conversion(new_shape, "GRIDCODE", "captured_fld", self.cellsize)
                ras = Con(IsNull(Raster("captured_fld")) == 1, 0, Raster("captured_fld"))
                append_raster = 'captured_fld' + str(row[0])
                ras.save(append_raster)

                order_as_raster.append(append_raster)
                del calculation, euc, ras

        arcpy.Delete_management('layer.lyr')
        del stream_order_shp

        ras = CellStatistics(order_as_raster, "SUM", "DATA")
        ras.save('combined_fld')
        arcpy.gp.Reclassify_sa('combined_fld', "Value", "0 NoData;1 10000000 1", "fld", "DATA")
        arcpy.RasterToPolygon_conversion('fld', "fld_as_polygon", "", "VALUE")
        arcpy.RepairGeometry_management('fld_as_polygon.shp')
        arcpy.AddField_management('fld_as_polygon.shp', "AREA", "DOUBLE")
        arcpy.CalculateField_management('fld_as_polygon.shp', "AREA", "!Shape.Area@Meters!", 'PYTHON_9.3')
        arcpy.FeatureClassToFeatureClass_conversion('fld_as_polygon.shp', temp_space, "fld_grab", '"GRIDCODE" = 1 AND "AREA" > 1000')

        arcpy.Clip_analysis("fld_grab.shp", self.output_location + '/extent.shp', "fld_grab_clip.shp")
        arcpy.Buffer_analysis("fld_grab_clip.shp", "fld_grab_clip_out.shp", "25 Meters", "", "", "ALL")
        arcpy.Buffer_analysis("fld_grab_clip_out.shp", "fld_grab_clip_in.shp", "-25 Meters", "", "", "ALL")
        arcpy.Merge_management("fld_grab_clip_in.shp; " + self.wet_areas, "fld_and_wet_areas")
        arcpy.Dissolve_management("fld_and_wet_areas.shp", self.hydrology_folder + "/predicted_increased_moisture")
        arcpy.PolygonToRaster_conversion(self.hydrology_folder + "/predicted_increased_moisture.shp", "fid", self.hydrology_folder + "/increase_mst", "", "", self.cellsize)

        return
