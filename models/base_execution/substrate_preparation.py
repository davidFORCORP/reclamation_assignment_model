import os, shutil
import arcpy
from datetime import datetime
from time import time
import sys
from core.config import Config
from arcpy.sa import *
from models.base_execution.eliminate import eliminate_features
from core.reused_functions import WorkspaceWorker, CleanAndRepair


class SubstratePreparation():

    def __init__(self):

        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True
        self.output_location = Config().get('global_parameters', 'output_location')

        self.surface_folder = self.output_location + '/surface'
        self.hydrology_folder = self.output_location + '/hydrology'
        self.surface = self.surface_folder + '/hydro_dem'
        self.extent = self.output_location + '/extent.shp'

        self.substrate_folder = os.path.join(self.output_location, 'substrate')
        self.hydrology_folder = os.path.join(self.output_location, 'hydrology')
        self.substrate = Config().get('substrate_parameters', 'substrate')
        self.substrate_awhc_conversion = eval(Config().get('substrate_parameters', 'substrate_awhc_conversion'))
        self.introduce_sai = eval(Config().get('substrate_parameters', 'introduce_sai'))
        self.introduce_existing_sai = eval(Config().get('substrate_parameters', 'use_existing_sai'))
        self.modify_sai_for_tailings = eval(Config().get('substrate_parameters', 'modify_sai_for_tailings'))
        self.existing_sai = Config().get('substrate_parameters', 'existing_sai')
        self.start_from_scratch = eval(Config().get('start_from_scratch', 'start_from_scratch'))

    def test_if_to_run(self):

        if arcpy.Exists(self.hydrology_folder + '/mr.shp'):
            return True
        else:
            return False

    def define_sai(self):

        if not self.introduce_sai:
            return None

        sai = os.path.join(self.substrate_folder, 'sai')
        if self.introduce_existing_sai:
            if arcpy.Exists(sai) or arcpy.Exists(self.existing_sai):
                try:
                    arcpy.CopyRaster_management(self.existing_sai, sai)
                except:
                    pass
                return sai

        if arcpy.Exists(sai):
            return sai

        print "...creating soil adjustment index @ " + datetime.fromtimestamp(time()).strftime('%H:%M')
        temp_space = WorkspaceWorker(self.substrate_folder).create()
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        arcpy.Clip_analysis(self.substrate, self.extent, 'substrate')
        substrate = CleanAndRepair(temp_space).run('substrate.shp', 10)
        print '......working...'

        arcpy.PolygonToRaster_conversion(substrate, 'fid', 'zone_ras', "", "", self.surface)
        arcpy.PolygonToRaster_conversion(substrate, 'adjustment', 'awhc_ras', "", "", self.surface)
        arcpy.gp.ZonalStatistics_sa('zone_ras', "VALUE", self.surface, "soil_min", "MINIMUM", "DATA")
        arcpy.gp.ZonalStatistics_sa('zone_ras', "VALUE", self.surface, 'soil_max', "MAXIMUM", "DATA")

        dem_max_value = Raster('soil_max.tif')
        dem_min_value = Raster('soil_min.tif')
        # dem_max_value = Con(dem_max_value - (dem_max_value * 0.05) < dem_min_value, dem_max_value, dem_max_value - (dem_max_value * 0.05))

        awhc_ras = Raster('awhc_ras')
        # convert = Con(awhc_ras == 170, 0.1, Con(awhc_ras == 135, 0.55, Con(awhc_ras == 125, 0.68, Con(awhc_ras == 112, 0.85, 0.68))))
        # convert.save('convert')
        dem_value = Con(dem_max_value - dem_min_value <= 0, 0.001, dem_max_value - dem_min_value)
        texture = ((awhc_ras / dem_value) * Raster(self.surface)) + (0 - ((awhc_ras / dem_value) * dem_min_value))
        texture.save(sai)

        WorkspaceWorker(temp_space).delete()

        return sai

    def modify_sai(self, temp_space, sai):

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        print '...... modifying SAI '
        tex = Con(sai == 0, 9999, sai)
        minimum = float(arcpy.GetRasterProperties_management(tex, "MINIMUM").getOutput(0))
        tex2 = Con(tex == 9999, minimum, tex)
        tex2.save('pre_mod')
        tex2_min = float(arcpy.GetRasterProperties_management(tex2, "MINIMUM").getOutput(0))
        mod = 0.1 / Con((tex2 - tex2_min) >= 1, 1, (tex2 - tex2_min))
        mod_min = float(arcpy.GetRasterProperties_management(mod, "MINIMUM").getOutput(0))
        sai_modified = Con(IsNull(mod) == 1, 1.01, 1 + mod_min - Con(mod > 1, 1, mod))
        sai_modified.save(self.substrate_folder + '/sai_modified')

        if self.modify_sai_for_tailings:
            tailings_sai = self.modify_sai_in_tailings(temp_space, 'pre_mod')

            return tailings_sai

        return self.substrate_folder + '/sai_modified'

    def modify_sai_in_tailings(self, temp_space, pre_tailings_sai):

        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space

        awhc_to_modify = eval(Config().get('substrate_parameters', 'tailings_awhc'))
        arcpy.Clip_analysis(self.substrate, self.extent, 'substrate')
        substrate = CleanAndRepair(temp_space).run('substrate.shp', 10)
        tailings_array = list()

        with arcpy.da.SearchCursor(substrate, ['awhc', 'SHAPE@']) as cursor:
            for i, feature in enumerate(cursor):
                if feature[0] not in awhc_to_modify:
                    continue
                arcpy.gp.ExtractByMask_sa(pre_tailings_sai, feature[1], "extract")

                log = -1 / Log10(Raster("extract"))
                mean = float(arcpy.GetRasterProperties_management(log, "MEAN").getOutput(0))

                tailings_adjust = Con(IsNull(Raster("extract")) == 0,
                                      Con((log / mean) < 1, Power(((log / mean) * 10), 2) / 10, (log / mean)))
                tailings_adjust.save(temp_space + '/tail_add' + str(i))
                tailings_array.append(temp_space + '/tail_add' + str(i))

        ras = CellStatistics(tailings_array, "SUM", "DATA")
        ras = Con(IsNull(ras) == 1, pre_tailings_sai, ras)
        ras.save(self.substrate_folder + '/tailings_add')

        return self.substrate_folder + '/tailings_add'

    def modify_flow_accumulation(self, sai, flow_accumulation, flow_initiation_cutoff, modifier=False):

        temp_space = self.substrate_folder + '/temp'
        arcpy.env.scratchWorkspace = temp_space
        arcpy.env.workspace = temp_space
        inititation = flow_initiation_cutoff

        if not modifier:
            adjust_fa = Con(IsNull(Raster(sai)) == 1, flow_accumulation,
                            flow_accumulation - (flow_accumulation * Raster(sai)))
        else:
            no_mod = Config().get('substrate_parameters', 'no_awhc_modification_substrates')
            substrate_field_name = Config().get('substrate_parameters', 'substrate_name_field')
            tailings_awhc = eval(Config().get('substrate_parameters', 'tailings_awhc'))
            tail_mod_values = ','.join(str(x) for x in tailings_awhc)

            inititation = self.hydrology_folder + '/inititation'
            arcpy.FeatureClassToFeatureClass_conversion(self.substrate, temp_space, "modifying_substrates",
                                                        '"{}" NOT IN ({})'.format(substrate_field_name, str(no_mod)))
            arcpy.FeatureClassToFeatureClass_conversion(self.substrate, temp_space, "modifying_substrates_tail",
                                                        '"AWHC" IN ({})'.format(tail_mod_values))
            arcpy.PolygonToRaster_conversion('modifying_substrates.shp', "FID", 'mod', "", "", flow_accumulation)
            mod = Raster('mod')
            arcpy.PolygonToRaster_conversion('modifying_substrates_tail.shp', "FID", 'mod_t', "", "", flow_accumulation)
            mod_t = Raster('mod_t')

            flow_initiation_cutoff_adjusted = Con(IsNull(mod) != 1,
                                                  (1 + flow_initiation_cutoff) +
                                                  (1 + flow_initiation_cutoff *
                                                   Con(IsNull(mod_t) != 1, 1 + Raster(sai), Raster(sai))),
                                                  flow_initiation_cutoff)
            flow_initiation_cutoff_adjusted.save(inititation)

            adjust_fa = Con(IsNull(mod) != 1, Con(IsNull(Raster(sai)) == 1, Raster(flow_accumulation),
                                                  Raster(flow_accumulation) - (Raster(flow_accumulation) * Raster(sai))), Raster(flow_accumulation))

        adjust_fa.save(self.hydrology_folder + '/fa_adjusted')

        return self.hydrology_folder + '/fa_adjusted', inititation
