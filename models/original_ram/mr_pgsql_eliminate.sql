----------------------------------------- CREATE SHELLS ------------------------------------------
DROP TABLE IF EXISTS process_arc;
CREATE TABLE process_arc AS
SELECT a.*, st_length(geom) length
FROM
	(
	SELECT  st_union(geom) geom, leftpolyg LPOLY_id, rightpoly RPOLY_id
	FROM arc
	GROUP BY leftpolyg, rightpoly
	) a;
ALTER TABLE process_arc ADD COLUMN id SERIAL;
ALTER TABLE process_arc ADD PRIMARY KEY (id);
CREATE INDEX idx_process_arc_geom ON process_arc USING gist(geom);
CREATE INDEX idx_process_arc_lpoly_id ON process_arc(lpoly_id);
CREATE INDEX idx_process_arc_rpoly_id ON process_arc(rpoly_id);
ANALYZE process_arc;

DROP TABLE IF EXISTS process_poly;
CREATE TABLE process_poly AS
SELECT mr_cov_id
id, area, gridcode code,  geom
FROM POLYGON
WHERE mkey in (select unnest(array[lpoly_id, rpoly_id]) from process_arc )
;
ALTER TABLE process_poly ADD PRIMARY KEY (id);
CREATE INDEX idx_process_poly_geom ON process_poly USING gist(geom);

DROP TABLE IF EXISTS eliminated;
create table eliminated (
     id serial not null,
     geom geometry(polygon, 26912) not null,
     joinfield integer,
     area double precision,
     gridcode integer,
     constraint pk_eliminated primary key (id));
CREATE INDEX idx_eliminated_geom ON eliminated USING gist(geom);

DROP TABLE IF EXISTS poly_del;
CREATE TABLE poly_del (
	id integer,
	p_id integer);
CREATE INDEX idx_poly_del_id ON poly_del(id);
CREATE INDEX idx_poly_del_p_id ON poly_del(p_id);

DROP TABLE IF EXISTS delete;
CREATE TABLE delete (
	p_id integer);
CREATE INDEX idx_delete_p_id ON delete(p_id);

------------------------------------------ START LOOP OF GRIDCODE'S ----------------------------
DO
$$
DECLARE
rec RECORD;
current_gridcode INTEGER;
BEGIN

  FOR rec in (SELECT code FROM process_poly GROUP BY code order by code)
  LOOP

  SELECT rec.code INTO current_gridcode;

 IF current_gridcode = 0 THEN
	INSERT INTO poly_del (id, p_id)
	SELECT DISTINCT ON (p.id) a.id, p.id p_id
	FROM
	    process_poly p
	    JOIN process_arc a ON p.id IN (a.lpoly_id, a.rpoly_id)
	WHERE p.code = 0
	ORDER BY p.id, st_length(a.geom) DESC;
 ELSE
	INSERT INTO poly_del (id, p_id)
	SELECT DISTINCT ON (p.id)
	a.id, p.id p_id
	FROM
	    process_poly p
	    JOIN process_arc a ON p.id IN (a.lpoly_id, a.rpoly_id)
	    LEFT JOIN process_poly l ON l.id = a.lpoly_id
	    LEFT JOIN process_poly r ON r.id = a.rpoly_id
	WHERE (st_area(p.geom) <= 1000 AND p.code = current_gridcode AND (l.code = r.code + 1 OR  l.code = r.code - 1 OR l.code = r.code) )
		AND p.id NOT IN (SELECT p_id FROM delete) --order by id ASC
	ORDER BY p.id, st_length(a.geom) DESC;
 END IF;

  INSERT INTO delete (p_id)
  SELECT p_id
  FROM poly_del;

  truncate eliminated restart identity;
  insert into eliminated (geom)
  SELECT (st_dump(st_polygonize(geom))).geom
  FROM process_arc
  WHERE id NOT IN (Select id from poly_del);

  UPDATE eliminated f SET joinfield = p.id, gridcode = p.code
  FROM process_poly p
  WHERE st_pointonsurface(p.geom)  && f.geom  AND st_within(st_pointonsurface(p.geom), f.geom)
  AND p.id NOT IN (SELECT p_id FROM poly_del);
  update eliminated  set area = st_area(geom);

-------------------------------- RE-CREATE PROCESS ARC FROM NEWLY ELIMINATED FEATURE -------------------
  DROP TABLE IF EXISTS eliminated_arcm;
  CREATE TABLE eliminated_arcm AS
  SELECT  ST_MakeLine(sp,ep) geom, gridcode
  FROM
   (SELECT
      ST_PointN(geom, generate_series(1, ST_NPoints(geom)-1)) AS sp,
      ST_PointN(geom, generate_series(2, ST_NPoints(geom)  )) AS ep, gridcode
    FROM
      (SELECT (ST_Dump(ST_Boundary(geom))).geom, gridcode
       FROM eliminated
       ) AS linestrings
    ) AS segments;
  ALTER TABLE eliminated_arcm ADD COLUMN feature_id SERIAL;
  ALTER TABLE eliminated_arcm ADD PRIMARY KEY (feature_id);

  DROP TABLE IF EXISTS eliminated_arc;
  CREATE TABLE eliminated_arc AS
    SELECT feature_id, geom, gridcode
    FROM
    (SELECT row_number() OVER (PARTITION BY geom) as id, feature_id, geom, gridcode
       FROM test) a
    WHERE id=1;

  DROP TABLE IF EXISTS process_arc_del;
  CREATE TABLE process_arc_del AS
  SELECT a.feature_id, b.mr_cov_id, st_length(st_intersection(b.geom, a.geom)) lt
  FROM eliminated_arc a,
       eliminated b
  WHERE st_dwithin(a.geom, b.geom, 1);

  DROP TABLE IF EXISTS process_arc_del2;
  CREATE TABLE process_arc_del2 AS
  SELECT feature_id, ids[1] LPOLY_id, ids[2] RPOLY_id
  FROM
      (SELECT feature_id, array_agg(mr_cov_id ORDER BY lt DESC) ids
       FROM process_arc_del
       where lt <> 0
       GROUP BY feature_id) a;

  DROP TABLE IF EXISTS process_arc;
  CREATE TABLE process_arc AS
  SELECT a.*, b.LPOLY_id, b.RPOLY_id, st_length(geom) length
  FROM eliminated_arc a, process_arc_del2 b
  WHERE a.feature_id = b.feature_id;
  ALTER TABLE process_arc ADD COLUMN id SERIAL;
  ALTER TABLE process_arc ADD PRIMARY KEY (id);
  CREATE INDEX idx_process_arc_geom ON process_arc USING gist(geom);
  CREATE INDEX idx_process_arc_lpoly_id ON process_arc(lpoly_id);
  CREATE INDEX idx_process_arc_rpoly_id ON process_arc(rpoly_id);
  ANALYZE process_arc;

  DROP TABLE IF EXISTS process_poly;
  CREATE TABLE process_poly AS
  SELECT id, area, gridcode code,  geom
  FROM (SELECT joinfield id, st_area(geom) area, gridcode, geom
        FROM eliminated)a;
  ALTER TABLE process_poly ADD PRIMARY KEY (id);
  CREATE INDEX idx_process_poly_geom ON process_poly USING gist(geom);

  TRUNCATE poly_del;
  TRUNCATE delete;
 END LOOP;
END
$$
;

ALTER TABLE eliminated ADD COLUMN mrclass character varying(12);
UPDATE eliminated SET mrclass = 'hydric' WHERE gridcode = 1;
UPDATE eliminated SET mrclass = 'sub hydric' WHERE gridcode = 2;
UPDATE eliminated SET mrclass = 'hygric' WHERE gridcode = 3;
UPDATE eliminated SET mrclass = 'sub hygric' WHERE gridcode = 4;
UPDATE eliminated SET mrclass = 'mesic' WHERE gridcode = 5;
UPDATE eliminated SET mrclass = 'sub mesic' WHERE gridcode = 6;
UPDATE eliminated SET mrclass = 'sub xeric' WHERE gridcode = 7;
UPDATE eliminated SET mrclass = 'xeric' WHERE gridcode = 8;




