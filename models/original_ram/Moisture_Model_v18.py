import arcpy, os, datetime, sys, time
from arcpy.sa import *
from core.config_ram import Config
from core.reused_functions import HelperFunctions


class Run_Ram():
    def __init__(self):

        print "Starting the Reclamation Assignment Model @ " + datetime.datetime.fromtimestamp(time.time()).strftime(
            '%H:%M:%S')
        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True

        self.cmdline = HelperFunctions()
        self.psql = 'psql -q -d projects -h pg-plan01-prd.forcorp.local'
        self.begin_execution_from_specific_point = eval(
            Config().get('global_parameters', 'begin_execution_from_specific_point'))
        self.CreateFiles = eval(Config().get('global_parameters', 'CreateFiles'))
        self.hydro_line_burn = eval(Config().get('global_parameters', 'hydro_line_burn'))
        self.should_we_burn_in_hydro_features = eval(
            Config().get('global_parameters', 'should_we_burn_in_hydro_features'))
        self.burn_in_culverts = eval(Config().get('global_parameters', 'burn_in_culverts'))
        self.CreateSurface = eval(Config().get('global_parameters', 'CreateSurface'))
        self.check_streams = eval(Config().get('global_parameters', 'check_streams'))
        self.cellsize = Config().get('global_parameters', 'cellsize')
        self.process_dem = eval(Config().get('global_parameters', 'process_dem'))
        self.BoundaryMatch = eval(Config().get('global_parameters', 'BoundaryMatch'))
        self.ProcessSLPChar = int(Config().get('global_parameters', 'ProcessSLPChar'))
        self.is_hydrology_false = eval(Config().get('global_parameters', 'is_hydrology_false'))
        self.CreateMoist = eval(Config().get('global_parameters', 'CreateMoist'))
        self.introduce_and_create_substrate_factor = eval(
            Config().get('global_parameters', 'introduce_and_create_substrate_factor'))
        self.use_existing_texture = eval(Config().get('global_parameters', 'use_existing_texture'))
        self.moisture_class_ok = eval(Config().get('global_parameters', 'moisture_class_ok'))
        self.eliminateMR = eval(Config().get('global_parameters', 'eliminateMR'))
        self.ProcessASPChar = int(Config().get('global_parameters', 'ProcessASPChar'))
        self.ProcessTopo = int(Config().get('global_parameters', 'ProcessTopo'))
        self.CreateSoilNut = int(Config().get('global_parameters', 'CreateSoilNut'))
        self.CreateConditionSet = eval(Config().get('global_parameters', 'CreateConditionSet'))
        self.output_location = Config().get('global_parameters', 'output_location')
        self.parent_folder = Config().get('global_parameters', 'parent_folder')
        self.scratch_workspace = self.output_location + '\\temp'
        self.processing_boundary = Config().get('global_parameters', 'processing_boundary')
        self.footprint = Config().get('global_parameters', 'footprint')
        self.Culvert_Burn = Config().get('global_parameters', 'Culvert_Burn')
        self.hydro_line = Config().get('global_parameters', 'hydro_line')
        self.hydro_features = Config().get('global_parameters', 'hydro_features')
        self.dem = Config().get('global_parameters', 'dem')
        self.open_water = Config().get('global_parameters', 'open_water')
        self.TPI = Config().get('global_parameters', 'TPI')
        self.substrate = Config().get('global_parameters', 'substrate')
        self.substrate_proportions = Config().get('global_parameters', 'substrate_proportions')
        self.substrate_version = Config().get('global_parameters', 'substrate_version')
        self.substrate_name_field = Config().get('global_parameters', 'substrate_name_field')
        self.ExistingEcosites = Config().get('global_parameters', 'ExistingEcosites')
        self.label = Config().get('global_parameters', 'label')
        self.flow_initiation_cutoff = float(Config().get('global_parameters', 'flow_initiation_cutoff'))
        self.MR_Reclass = Config().get('global_parameters', 'MR_Reclass')
        self.TPReclass = Config().get('global_parameters', 'TPReclass')
        self.SLPReclass = Config().get('global_parameters', 'SLPReclass')
        self.Factor1 = float(Config().get('global_parameters', 'Factor1'))
        self.Factor2 = float(Config().get('global_parameters', 'Factor2'))
        self.Factor3 = float(Config().get('global_parameters', 'Factor3'))
        self.Factor4 = float(Config().get('global_parameters', 'Factor4'))
        self.Factor5 = float(Config().get('global_parameters', 'Factor5'))
        self.Factor6 = float(Config().get('global_parameters', 'Factor6'))
        self.Factor7 = float(Config().get('global_parameters', 'Factor7'))
        self.Factor8 = float(Config().get('global_parameters', 'Factor8'))
        self.Factor9 = float(Config().get('global_parameters', 'Factor9'))
        self.stream_order = int(Config().get('global_parameters', 'stream_order'))
        self.awhc_112 = float(Config().get('global_parameters', 'awhc_112'))
        self.awhc_125 = float(Config().get('global_parameters', 'awhc_125'))
        self.awhc_135 = float(Config().get('global_parameters', 'awhc_135'))
        self.awhc_150 = float(Config().get('global_parameters', 'awhc_150'))
        self.awhc_170 = float(Config().get('global_parameters', 'awhc_170'))
        self.awhc_field = str(Config().get('global_parameters', 'awhc_field'))
        # self.coke = Config().get('coke', 'global_parameters')
        # self.tailings = Config().get('tailings', 'global_parameters')
        # self.suitable_ss_an = Config().get('suitable_ss_an', 'global_parameters')
        # self.none_an = Config().get('none_an', 'global_parameters')
        # self.unsuitable_ob_los = Config().get('unsuitable_ob_los', 'global_parameters')
        # self.suitable_ss_ml = Config().get('suitable_ss_ml', 'global_parameters')
        # self.none_ml = Config().get('none_ml', 'global_parameters')
        # self.unsuitable_ob = Config().get('unsuitable_ob', 'global_parameters')
        # self.cake = Config().get('cake', 'global_parameters')


    def create(self):

        if self.CreateFiles:

            if arcpy.Exists(self.output_location):
                arcpy.Delete_management(self.output_location)
            os.makedirs(self.output_location)
            os.makedirs(self.scratch_workspace)
            os.makedirs(self.output_location + "\NewSurface")
            os.makedirs(self.output_location + "\moisture")

    def delete(self, delete_features):
        for d in delete_features:
            try:
                arcpy.Delete_management(d)
            except:
                continue

    def start(self):
        if self.CreateSurface:

            print 'Creating New Surface'
            if arcpy.Exists(self.output_location + "\NewSurface"):
                arcpy.Delete_management(self.output_location + "\NewSurface")
            os.makedirs(self.output_location + "\NewSurface")
            arcpy.env.workspace = self.output_location + "\NewSurface"
            arcpy.env.scratchWorkspace = self.scratch_workspace

            # Boun = Int(Raster(dem) / Raster(dem))
            # arcpy.RasterToPolygon_conversion(Boun, "EXTENT", "", "VALUE")
            arcpy.CopyFeatures_management(self.footprint, 'extent')
            arcpy.gp.ExtractByMask_sa(self.dem, 'extent.shp', "dem_clip")
            dem = 'dem_clip'
            dsc = arcpy.Describe(dem)
            arcpy.env.Extent = dsc.Extent

            if not self.burn_in_culverts:
                dem1 = dem
            else:
                if not arcpy.Exists(self.Culvert_Burn):
                    print 'If you are going to turn on culvert breaching, define a culvert breaching layer!'
                    sys.exit()
                print "Burning in culverts @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
                arcpy.gp.ExtractByMask_sa(dem, self.Culvert_Burn, "cul_dem")
                divide = Int(Raster("cul_dem") / Raster("cul_dem"))
                arcpy.gp.RegionGroup_sa(divide, "Cul_Reg", "EIGHT", "WITHIN")
                del divide
                arcpy.gp.ZonalStatistics_sa("cul_Reg", "VALUE", "cul_dem", "Cul_Zone", "MINIMUM", "DATA")
                times = Int(Raster("Cul_Zone") * 10000)
                arcpy.RasterToPolygon_conversion(times, "CUL_ZONE", "", "VALUE")
                del times
                arcpy.Union_analysis(["CUL_ZONE.SHP", "EXTENT.ShP"], "UNION")
                arcpy.PolygonToRaster_conversion("UNION.shp", "VALUE", "BURN", "", "", self.cellsize)
                divide = Float(Raster("Burn")) / 10000
                ras = Con(divide > 0, divide, dem)
                ras.save("dem1")
                dem1 = "dem1"
                del ras
                del divide
                delete_features = ["Culvert", "Cul_Reg", "Cul_Zone", "CUL_ZONE.shp", "UNION.shp", "BURN"]
                self.delete(delete_features)

            if not self.hydro_line_burn:
                # Note: features are burnt in to the lowest elevation of the whole line. This method was used as if the mine
                # wants to enforce a stream channel, they would have to bulldoze it in. Other, natural stream channels
                # would have no steep cutting in around the channel.
                dem2 = dem1
            else:
                if not arcpy.Exists(self.hydro_line):
                    print 'If you are going to turn on hydro line breaching, define the layer'
                    sys.exit()
                print "Burning in hydro lines @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
                arcpy.PolygonToLine_management(self.hydro_line, "RING1")
                #  Buffer ring by 3m or a little more
                arcpy.gp.ExtractByMask_sa(dem1, self.hydro_line, "st_dem")
                divide = Int(Raster("st_dem") / Raster("st_dem"))
                arcpy.gp.RegionGroup_sa(divide, "st_Reg", "EIGHT", "WITHIN", "NO_LINK")
                del divide
                arcpy.gp.ZonalStatistics_sa("st_Reg", "VALUE", "st_dem", "st_Zone", "MINIMUM", "DATA")
                times = Int(Raster("st_Zone") * 10000)
                arcpy.RasterToPolygon_conversion(times, "st_ZONE", "", "VALUE")
                del times
                arcpy.Union_analysis(["st_ZONE.SHP", "EXTENT.ShP"], "UNION")
                arcpy.PolygonToRaster_conversion("UNION.shp", "GRIDCODE", "BURN", "", "", self.cellsize)
                divide = Float(Raster("Burn")) / 10000
                ras = Con(divide > 0, divide, dem1)
                ras.save("dem2")
                dem2 = "dem2"
                del ras
                del divide
                delete_features = ["st_dem", "st_Reg", "st_Zone", "st_ZONE.shp", "UNION.shp", "BURN"]
                self.delete(delete_features)
            #
            # # if self.is_hydrology_false:
            # #     print "Creating hydrology layer from provided hydrology data @" + datetime.datetime.fromtimestamp(
            # #         time.time()).strftime('%H:%M:%S')
            # #
            # #     arcpy.Dissolve_management(self.hydro_features, "hyd_clean", "", "", "SINGLE_PART")
            # #     arcpy.PolygonToLine_management('hyd_clean.shp', "hyd_line")
            # #     arcpy.gp.EucDistance_sa("hyd_line.shp", "hyd_feat_euc", "", self.cellsize)
            # #     arcpy.gp.FocalStatistics_sa("hyd_feat_euc", "std", "Rectangle {} {} CELL".format(str(int(15 / int(self.cellsize))), str(int(15 / int(self.cellsize)))), "STD", "DATA")
            # #     arcpy.gp.Reclassify_sa("std", "Value", "0 3.7 1;3.7 5000 NoData", "hydro_std", "DATA")
            # #     arcpy.Buffer_analysis(self.hydro_features, "hyd_buf.shp", "-1 Meters", "", "", "ALL")
            # #     arcpy.gp.ExtractByMask_sa("hydro_std", "hyd_buf.shp", "hyd_stream")
            # #     arcpy.gp.EucDistance_sa("hyd_stream", "hyd_euc", "", self.cellsize)
            # #
            # #     arcpy.gp.Slope_sa(dem, "SLOPE", "DEGREE", "1")
            # #     euc_max = (arcpy.GetRasterProperties_management("hyd_EUC", "MAXIMUM")).getOutput(0)
            # #     slp_max = (arcpy.GetRasterProperties_management("slope", "MAXIMUM")).getOutput(0)
            # #     ras = (Raster("hyd_EUC") / float(euc_max)) * (Raster("SLOPE") / float(slp_max)) * 1000
            # #     ras.save('Hydro')
            # #     arcpy.gp.ExtractByMask_sa('Hydro', "hyd_buf.shp", "hydro_feat")
            # #     arcpy.gp.Reclassify_sa("hydro_feat", "Value", "0 0.025 1;0.025 5000 NoData", "hydro_class", "DATA")
            # #     arcpy.RasterToPolygon_conversion("hydro_class", "hyd_ZONE", "", "VALUE")
            # #     arcpy.Buffer_analysis("hyd_ZONE.shp", "hyd_buf2.shp", "1 Meters", "", "", "ALL")
            # #     arcpy.Buffer_analysis("hyd_buf2.shp", "hyd_3.shp", "-1 Meters", "", "", "ALL")
            # #     arcpy.AddField_management("hyd_3.shp", "AREA", "DOUBLE")
            # #     arcpy.CalculateField_management("hyd_3.shp", "AREA", "!Shape.Area@Hectares!", "PYTHON_9.3")
            # #     arcpy.Dissolve_management("hyd_3.shp", "hyd_4.shp", "", "", "SINGLE_PART")
            # #     arcpy.AddField_management("hyd_4.shp", "AREA", "DOUBLE")
            # #     arcpy.CalculateField_management("hyd_4.shp", "AREA", "!Shape.Area@Hectares!", "PYTHON_9.3")
            # #     arcpy.FeatureClassToFeatureClass_conversion("hyd_4.shp", self.output_location + '\\newsurface',
            # #                                                 'hydrology',
            # #                                                 '"AREA" >= 0.01')
            # #     if int(arcpy.GetCount_management("hydrology.shp").getOutput(0)) == 0:
            # #         print 'Hydrology empty'
            # #         sys.exit()

            if not self.should_we_burn_in_hydro_features:
                dem3 = dem2
            else:
                print "Considering hydrological features @" + datetime.datetime.fromtimestamp(time.time()).strftime(
                    '%H:%M:%S')
                hydro_features = "hydrology.shp"
                try:
                    if arcpy.Exists(hydro_features):
                        hydro = hydro_features
                    else:
                        hydro = self.hydro_features
                except:
                    hydro = self.hydro_features

                if arcpy.Exists(self.open_water):
                    array = [hydro, self.open_water]
                    arcpy.Merge_management(array, "water_features")
                    arcpy.gp.ExtractByMask_sa(dem2, "water_features.shp", "hyd_dem")
                else:
                    arcpy.gp.ExtractByMask_sa(dem2, hydro_features, "hyd_dem")

                divide = Int(Raster("hyd_dem") / Raster("hyd_dem"))
                arcpy.RasterToPolygon_conversion(divide, "hyd_ZONE", "", "VALUE")

                arcpy.gp.RegionGroup_sa(divide, "hyd_Reg", "EIGHT", "WITHIN", "NO_LINK")
                arcpy.gp.ZonalStatistics_sa("hyd_Reg", "VALUE", "hyd_dem", "hyd_Zone", "MINIMUM", "DATA")
                times = Int(Raster("hyd_Zone") * 10000)

                arcpy.Union_analysis(["hyd_ZONE.SHP", "EXTENT.ShP"], "UNION")
                arcpy.PolygonToRaster_conversion("UNION.shp", "GRIDCODE", "Hydro_ras", "", "", self.cellsize)
                arcpy.gp.Shrink_sa("Hydro_ras", "Feat_BURN2", "{}".format(int(5/int(self.cellsize))), [1])
                ras = Con(Raster("Feat_BURN2") == 1, 0, Raster("Hydro_ras"))
                ras.save("Feat_Burn")
                arcpy.RasterToPolyline_conversion(Raster("Feat_Burn"), 'Streams1', "ZERO", "0", "SIMPLIFY", "VALUE")
                del ras
                delete_features = ["hyd_dem", "hyd_Zone.shp", "UNION.shp", "Feat_BURN2", "Feat_BURN"]  # "merge.shp"]
                self.delete(delete_features)

                arcpy.RasterToPolygon_conversion(times, "hyd_ZONE", "", "VALUE")
                arcpy.Union_analysis(["hyd_ZONE.SHP", "EXTENT.ShP"], "UNION")
                arcpy.PolygonToRaster_conversion("UNION.shp", "GRIDCODE", "Feat_BURN", "", "", self.cellsize)
                divide = Float(Raster("Feat_Burn")) / 10000
                ras = Con(divide > 0, divide, dem2)
                ras.save("dem3")
                dem3 = "dem3"
                del ras, divide
                delete_features = ["hyd_Reg", "hyd_Zone", "hyd_ZONE.shp", "UNION.shp"]
                self.delete(delete_features)

            if self.process_dem:
                # Smooth and remove artificially flat areas
                print "Processing dem @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
                # arcpy.gp.FocalStatistics_sa(dem3, "New_Surface", "Rectangle 3 3 CELL", "MEAN", "DATA")
                # arcpy.gp.FocalStatistics_sa(dem, "SMTH2", "Rectangle 9 9 CELL", "MEAN", "DATA")
                # Used to randomize entire landscape but now turning off completely
                arcpy.gp.FocalStatistics_sa(dem3, "SMTH", "Rectangle 3 3 CELL", "MEAN", "DATA")
                arcpy.CreateRandomRaster_management(self.output_location + "\NewSurface", "RNDM", "UNIFORM -0.1 0.1",
                                                    dem, self.cellsize)
                add = Raster("RNDM") + Raster("SMTH")

                # Turned off for AN as the whole landscape was fake and required randomization.
                # arcpy.gp.FocalStatistics_sa(add, "Focal1", "Rectangle 9 9 CELL", "MEAN", "DATA")
                # arcpy.gp.Con_sa("slpstd", "focal1", "Move1", "smth", "\"VALUE\" < 0.1")

                arcpy.gp.FocalStatistics_sa(add, self.output_location + "\NEWSURFACE\\New_Surface",
                                            "Rectangle 9 9 MAP", "MEAN",
                                            "DATA")
                add2 = Raster("RNDM") + Raster(dem)
                arcpy.gp.FocalStatistics_sa(add2, "SMTH2", "Rectangle 9 9 MAP", "MEAN", "DATA")

                arcpy.gp.Slope_sa("SMTH2", "SLP", "DEGREE", "1")
                arcpy.gp.FocalStatistics_sa("SLP", "SLPSTD", "Rectangle 9 9 MAP", "STD", "DATA")
                # Smooth out artificially or naturally (beaver and other) steep elevation changes
                # arcpy.gp.Slope_sa("Move1", "SLP2", "DEGREE", "1")
                # arcpy.gp.FocalStatistics_sa("SLP2", "SLPSTD2", "Rectangle 3 3 CELL", "STD", "DATA")
                # arcpy.gp.Reclassify_sa("SLPSTD2", "Value", "-1 1 0;1 5000 1", "STDCLSA", "DATA")
                # arcpy.gp.FocalStatistics_sa("Move1", "MOVE3", "Rectangle 9 9 CELL", "MEAN", "DATA")
                # ras = Con(Raster("SLPSTD") < 0.1, 0, Raster("STDCLSA"))
                # arcpy.gp.Con_sa(ras, "MOVE3", output_location + "\NEWSURFACE\\New_Surface", "MOVE1", "\"VALUE\" = 1")
                # del ras, dem1, dem2

                print "Creating General Surface Metrics @" + datetime.datetime.fromtimestamp(time.time()).strftime(
                    '%H:%M:%S')

                arcpy.gp.HillShade_sa("SMTH2", "Hillshade", "315", "45", "NO_SHADOWS", "1")
                arcpy.gp.Fill("New_Surface", "FILL")
                arcpy.gp.FlowDirection_sa("FILL", "FLOW_DIR", "NORMAL")
                arcpy.gp.FlowAccumulation_sa("FLOW_DIR", "FLOW_ACC", "", "FLOAT")
                arcpy.CalculateStatistics_management("FLOW_ACC")

                arcpy.gp.Slope_sa("SMTH2", "SLOPE", "DEGREE", "1")
                arcpy.gp.Reclassify_sa("SLOPE", "Value",
                                       "0 2 1;2 6 2;6 10 3;10 16 4; 16 31 5;31 46 6;46 71 7; 71 100 8",
                                       "SLPCLS", "DATA")
                arcpy.gp.Aspect_sa("SMTH2", "ASPECT")
                arcpy.gp.Reclassify_sa("ASPECT", "Value", "-1 0 1;0 45 2;45 135 3;135 225 4;225 315 5;315 360 2",
                                       "ASPTCLSA", "DATA")
                arcpy.gp.Con_sa("SLPCLS", "0", "ASPTCLS", "ASPTCLSA",
                                "\"VALUE\" = 1")  # turning slope class 1 to flat Aspect
                arcpy.gp.Reclassify_sa("New_Surface", "Value", "0 10000 0;NODATA 0", "EXTENT", "NODATA")

                arcpy.FeatureClassToFeatureClass_conversion(self.substrate, self.output_location + "\\newsurface",
                                                            "subsoil",
                                                            '"{}" = '.format(self.substrate_name_field) + "'Tailings Sand'")
                arcpy.AddField_management("subsoil.SHP", "GRIDCODE", "SHORT")
                arcpy.CalculateField_management("subsoil.SHP", "GRIDCODE", "0", "PYTHON_9.3")
                arcpy.Erase_analysis("EXTENT.ShP", "subsoil.SHP", "mergeme")
                arcpy.Merge_management("subsoil.SHP; mergeme.shp", "UNION")
                arcpy.PolygonToRaster_conversion("UNION.shp", "GRIDCODE", "FLD_Minus", "", "", self.cellsize)
                array5 = ["subsoil.shp", "mergeme.shp", "UNION.shp", "FLD_Minus"]

                arcpy.FeatureClassToFeatureClass_conversion(self.substrate, self.output_location + "\\newsurface",
                                                            "subsoil2",
                                                            '"substrate_" = \'Suitable Subsoil\' OR "substrate_" = \'N/A\' OR '
                                                            '"substrate_" = \'Other\' OR "substrate_" = \'Suitable Subsoil (Littoral Zone)\'')
                arcpy.AddField_management("subsoil2.SHP", "GRIDCODE", "SHORT")
                arcpy.CalculateField_management("subsoil2.SHP", "GRIDCODE", "0", "PYTHON_9.3")
                arcpy.Erase_analysis("EXTENT.ShP", "subsoil2.SHP", "mergeme2")
                arcpy.Merge_management("subsoil2.SHP; mergeme2.shp", "UNION2")
                arcpy.PolygonToRaster_conversion("UNION2.shp", "GRIDCODE", "FLD_Minus_NA", "", "", self.cellsize)
                array6 = ["subsoil2.shp", "mergeme2.shp", "UNION2.shp", "FLD_Minus_NA"]

            arcpy.Clip_analysis(self.substrate, 'extent.shp', 'sub_clip')
            substrate = 'sub_clip.shp'

            if self.introduce_and_create_substrate_factor:

                print "Processing substrate factor @" + datetime.datetime.fromtimestamp(time.time()).strftime(
                    '%H:%M:%S')
                arcpy.MultipartToSinglepart_management(substrate, "UN2.shp")
                arcpy.AddField_management("UN2.shp", "AREA", "DOUBLE")
                arcpy.CalculateField_management("UN2.shp", "AREA", "!Shape.Area@Hectares!", "PYTHON_9.3")
                shape = "UN2.shp"

                array = []
                y = int(arcpy.GetCount_management(shape).getOutput(0))
                x = 1
                count = 1
                tvv = 'layer.lyr'
                while y != x:
                    y = x
                    arcpy.MakeFeatureLayer_management(shape, tvv)
                    arcpy.SelectLayerByAttribute_management(tvv, "NEW_SELECTION", '"AREA" < 10')
                    arcpy.Eliminate_management(tvv, "Xa{}".format(count), "AREA")
                    shape = "Xa{}.shp".format(count)
                    try:
                        arcpy.AddField_management(shape, "AREA", "DOUBLE")
                    except:
                        time.sleep(10)
                        arcpy.AddField_management(shape, "AREA", "DOUBLE")
                    arcpy.CalculateField_management(shape, "AREA", "!Shape.Area@Hectares!", "PYTHON_9.3")

                    array.append(shape)
                    arcpy.Delete_management(tvv)
                    x = int(arcpy.GetCount_management(shape).getOutput(0))
                    count += 1

                tv = 'layer1.lyr'
                arcpy.MakeFeatureLayer_management(shape, tv)
                n = 0
                array2 = []
                array2.append("EXTENT")
                for feature in arcpy.SearchCursor(shape):

                    objectid = str(feature.getValue('FID'))
                    arcpy.SelectLayerByAttribute_management(tv, "NEW_SELECTION", '"FID" = {}'.format(str(objectid)))
                    arcpy.gp.ExtractByMask_sa(dem, tv, "extract")

                    dem_max = arcpy.GetRasterProperties_management("extract", "MAXIMUM")
                    dem_min = arcpy.GetRasterProperties_management("extract", "MINIMUM")
                    dem_min_value = float(dem_min.getOutput(0))
                    if float(dem_max.getOutput(0)) - (float(dem_max.getOutput(0)) * 0.05) < dem_min:
                        dem_max_value = float(float(dem_max.getOutput(0)))
                    else:
                        dem_max_value = float(float(dem_max.getOutput(0)) - (float(dem_max.getOutput(0)) * 0.05))

                    awhc = float(feature.getValue(self.awhc_field))
                    if awhc == 170:
                        awhc_factor = float(self.awhc_170)
                    elif awhc == 135:
                        awhc_factor = float(self.awhc_135)
                    elif awhc == 125:
                        awhc_factor = float(self.awhc_125)
                    elif awhc == 112:
                        awhc_factor = float(self.awhc_112)
                    else:
                        awhc_factor = float(0)

                    texture = ((awhc_factor / ((dem_max_value + 0.001) - dem_min_value)) * Raster("extract")) + (
                            0 - (awhc_factor / ((dem_max_value + 0.001) - dem_min_value) * dem_min_value))
                    texture.save("TEX")
                    texture_factor = Con(Raster("TEX") < 0, 0, Raster("TEX"))
                    texture_factor.save("texture" + str(n))
                    array2.append("texture" + str(n))
                    arcpy.Delete_management("TEX")

                    n += 1
                dsc = arcpy.Describe(dem)
                arcpy.env.extent = dsc.Extent
                ras = CellStatistics(array2, "SUM", "DATA")
                ras.save(self.parent_folder + '\\texture_{}'.format(str(self.substrate_version)))
                array.append('layer1.lyr')
                for feature in array:
                    arcpy.Delete_management(feature)
                for feature in array2:
                    arcpy.Delete_management(feature)
                # arcpy.RasterToPolygon_conversion(Boun, "EXTENT", "", "VALUE")

                texture = self.parent_folder + '\\texture_{}'.format(str(self.substrate_version))
                flow_initiation_cutoff_adjusted = Con(Raster("fld_minus_NA") == 0, 40000, (
                        ((Raster(dem) / Raster(dem)) + self.flow_initiation_cutoff) + (
                        (Raster(dem) / Raster(dem)) + self.flow_initiation_cutoff * (
                    Con(Raster("FLD_MINUS") == 1, 1 + Raster(texture), Raster(texture))))))
                flow_initiation_cutoff_adjusted.save("inititation")
                adjust_fa = Con(Raster("FLD_MINUS_NA") == 0, Raster("flow_acc"),
                                Con(IsNull(Raster(texture)) == 1, Raster("flow_acc"),
                                    Raster("flow_acc") - (Raster("Flow_acc") * Raster(texture))))
                adjust_fa.save("fa_adjusted")
                stream_from_fa = Con(Raster("fa_adjusted") > Raster("inititation"), 1, 0)
                stream_from_fa.save("streams")
                del adjust_fa
            else:
                if self.use_existing_texture:
                    print "Processing substrate factor @" + datetime.datetime.fromtimestamp(time.time()).strftime(
                        '%H:%M:%S')
                    texture = self.parent_folder + '\\texture_{}'.format(str(self.substrate_version))
                    flow_initiation_cutoff_adjusted = Con(Raster("fld_minus_NA") == 0, 40000, (
                            ((Raster(dem) / Raster(dem)) + self.flow_initiation_cutoff) + (
                            (Raster(dem) / Raster(dem)) + self.flow_initiation_cutoff * (
                        Con(Raster("FLD_MINUS") == 1, 1 + Raster(texture), Raster(texture))))))
                    flow_initiation_cutoff_adjusted.save("inititation")
                    adjust_fa = Con(Raster("fld_minus_NA") == 0, Raster("flow_acc"),
                                    Con(IsNull(Raster(texture)) == 1, Raster("flow_acc"),
                                        Raster("flow_acc") - (Raster("Flow_acc") * Raster(texture))))
                    adjust_fa.save("fa_adjusted")
                    stream_from_fa = Con(Raster("fa_adjusted") > Raster("inititation"), 1, 0)
                    stream_from_fa.save("streams")
                    # stream_fa = Con(Raster("fa_adjusted") > flow_initiation_cutoff_adjusted, Raster("fa_adjusted"), SetNull(Raster("fa_adjusted"),Raster("fa_adjusted"), '"Value" < {}'.format(str(flow_inititation_cutoff_adjusted))))
                    # stream_fa.save("stream_fa")
                    del adjust_fa
                else:
                    # stream_fa = Con(Raster("flow_acc") > flow_initiation_cutoff_adjusted, Raster("flow_acc"), SetNull(Raster("flow_acc"),Raster("flow_acc"), '"Value" < {}'.format(str(flow_inititation_cutoff_adjusted))))
                    # stream_fa.save("stream_fa")
                    stream_from_fa = Con(Raster("flow_acc") > self.flow_initiation_cutoff, 1, 0)
                    stream_from_fa.save("streams")
            return

    def streams(self):
        dem = 'dem_clip'
        arcpy.env.workspace = self.output_location + "\\NewSurface"
        arcpy.env.scratchWorkspace = self.scratch_workspace
        dsc = arcpy.Describe(dem)
        arcpy.env.extent = dsc.Extent

        print "Creating streams @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        arcpy.RasterToPolyline_conversion("STREAMS", "STREAMS", "ZERO", "0", "SIMPLIFY", "VALUE")

        if self.check_streams:
            print 'Check Streams'
            sys.exit()

        array = []
        if arcpy.Exists("ring1.shp"):
            array.append("ring1.shp")
        if self.should_we_burn_in_hydro_features:
            array.append("Streams1.shp")
        array.append("STREAMs.shp")
        arcpy.Merge_management(array, "euc_streams")
        arcpy.gp.EucDistance_sa("euc_streams.shp", "STREAM_EUCa", "", self.cellsize)

        arcpy.gp.ExtractByMask_sa("STREAM_EUCa", self.processing_boundary, "STREAM_EUC")

        arcpy.gp.StreamOrder_sa("STREAMS", "FLOW_DIR", "Stream_Order5", "STRAHLER")
        arcpy.FeatureClassToFeatureClass_conversion(self.substrate, self.output_location + "\\newsurface", "subsoil",
                                                    '"substrate_" = \'Suitable Subsoil\' OR "substrate_" = \'N/A\' OR '
                                                    '"substrate_" = \'Other\' OR "substrate_" = \'Suitable Subsoil (Littoral Zone)\'')
        arcpy.AddField_management("subsoil.SHP", "GRIDCODE", "SHORT")
        arcpy.CalculateField_management("subsoil.SHP", "GRIDCODE", "0", "PYTHON_9.3")
        arcpy.Erase_analysis("EXTENT.ShP", "subsoil.SHP", "mergeme")
        arcpy.Merge_management("subsoil.SHP; mergeme.shp", "UNION")
        arcpy.PolygonToRaster_conversion("UNION.shp", "GRIDCODE", "FLD_Minus", "", "", self.cellsize)
        # This makes the stream order be subtracted by X (in this case 2). This limits the streams which get the flood plain calculation.
        ras = Con((Raster("stream_order5") - (Con(Raster("FLD_Minus") == 1, 2, 0))) < 0, 0,
                  (Raster("stream_order5") - (Con(Raster("FLD_Minus") == 1, 2, 0))))
        ras.save("Stream_Order")
        arcpy.RasterToPolyline_conversion("Stream_Order", "StreamOrder1", "ZERO", "0", "SIMPLIFY", "VALUE")
        arcpy.TrimLine_edit("StreamOrder1.shp", "50 Meters")
        arcpy.Dissolve_management("streamOrder1.shp", "StreamOrder", "GRID_CODE", "", "SINGLE_PART")

        # delete_features = ["StreamOrder1.shp", "Streams", "RNDM", "Extent.shp", "Streams1.shp",
        #                    "ASPTCLSA", "SLP", "STREAM_EUCa", "ASPTCLSA", "Stream_order5",
        #                    "UNION.shp", "Subsoil.shp", "mergeme.shp", "Ring1.shp", "Ring2.shp"]
        # for feature in delete_features:
        #     try:
        #         arcpy.Delete_management(feature)
        #     except:
        #         continue

        print 'Creating flood plains @ ' + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        arcpy.env.workspace = self.output_location + "\\NewSurface"
        arcpy.env.scratchWorkspace = self.scratch_workspace
        dem = 'dem_clip'
        dsc = arcpy.Describe(dem)
        arcpy.env.extent = dsc.Extent
        arcpy.env.overwriteOutput = True

        # Alter this value to play with the factors that enter into the flood plain calculation
        arcpy.gp.Reclassify_sa("New_Surface", "Value", "0 10000 0;NODATA 0", "EXTENT", "NODATA")
        count = self.stream_order
        array2 = []
        dsc = arcpy.Describe(dem)
        arcpy.env.extent = dsc.Extent
        hydro_features = "hydrology.shp"

        hydro = None
        if self.should_we_burn_in_hydro_features:
            try:
                if arcpy.Exists(hydro_features):
                    hydro = hydro_features
                else:
                    hydro = self.hydro_features
            except:
                hydro = self.hydro_features
            no_hydro = 0
        else:
            no_hydro = 1

        try:
            arcpy.AddField_management(hydro, 'area_ha', 'DOUBLE')
            arcpy.CalculateField_management(hydro, 'area_ha', '!Shape.Area@Hectares!')
        except:
            pass

        if arcpy.Exists(self.output_location + "\\temp"):
            arcpy.Delete_management(self.output_location + "\\temp")
        os.makedirs(self.output_location + "\\temp")
        tv = 'layer.lyr'
        Layer3 = 'layer3.lyr'
        sum_rasters = [self.output_location + "\\newsurface\\Extent"]
        while count <= 9:
            array = [row[0] for row in arcpy.da.SearchCursor("Streamorder.shp", "GRID_CODE")]
            if count not in array:
                count += 1
                continue

            arcpy.gp.SetNull_sa("Stream_Order", "Stream_Order", "SetNull", 'VALUE <> ' + str(count))
            if count == int(max(array)):
                if no_hydro == 0:
                    # include water water features in the flood plain calculation
                    arcpy.MakeFeatureLayer_management(hydro, tv)
                    arcpy.SelectLayerByAttribute_management(tv, "NEW_SELECTION", '"AREA_ha" > 5')
                    arcpy.PolygonToLine_management(tv, "Hydro")
                    arcpy.RasterToPolyline_conversion("SetNull", "ST_ORD1", "ZERO", "0", "SIMPLIFY", "VALUE")
                    arcpy.Merge_management(["hydro.shp", "st_ord1.shp"], "ST_ORD")
                    arcpy.Delete_management("hydro.shp")
                    arcpy.Delete_management("ST_ORD1.shp")
                    arcpy.Delete_management(tv)
                else:
                    arcpy.RasterToPolyline_conversion("SetNull", "ST_ORD", "ZERO", "0", "SIMPLIFY", "VALUE")
            else:
                arcpy.RasterToPolyline_conversion("SetNull", "ST_ORD", "ZERO", "0", "SIMPLIFY", "VALUE")

            arcpy.gp.ExtractByMask_sa("SMTH2", "st_ord.shp", "EM")
            TIMES = Int(Raster("EM") * 10000)
            arcpy.gp.EucAllocation_sa(TIMES, "ALLO", "", "", self.cellsize, "VALUE")
            DIVIDE = arcpy.sa.FloatDivide("ALLO", 10000)
            arcpy.gp.Minus_sa("SMTH2", DIVIDE, "SLPDIF")
            arcpy.CalculateStatistics_management("SLPDIF")
            # Either add 1 to streamorder gridcode or re-evaluate.
            factor = None
            if count == 1:
                Factor = self.Factor1
            elif count == 2:
                Factor = self.Factor2
            elif count == 3:
                Factor = self.Factor3
            elif count == 4:
                Factor = self.Factor4
            elif count == 5:
                Factor = self.Factor5
            elif count == 6:
                Factor = self.Factor6
            elif count == 7:
                Factor = self.Factor7
            elif count == 8:
                Factor = self.Factor8
            elif count == 9:
                Factor = self.Factor9
            else:
                sys.exit('Wrong Factor??')

            arcpy.gp.Reclassify_sa("SLPDIF", "Value", "-1000000 " + str(Factor * -1) + " NODATA;" + str(Factor * -1)
                                   + " " + str(Factor) + " 1;" + str(Factor) + " 1000000 NODATA", "ZONE", "NODATA")
            arcpy.gp.Shrink_sa("ZONE", "SHRINK", "7", [1])
            arcpy.gp.SetNull_sa("slpstd", "SHRINK", "OUTPUT", 'VALUE > 1')
            arcpy.RasterToPolygon_conversion("OUTPUT", 'RASPOLY')
            arcpy.MakeFeatureLayer_management("RASPOLY.shp", Layer3)
            arcpy.SelectLayerByLocation_management(Layer3, "INTERSECT", "ST_ORD.shp")
            arcpy.FeatureToRaster_conversion(Layer3, "GRIDCODE", "ZONEs2", self.cellsize)
            arcpy.gp.Expand_sa("ZONEs2", "ZONEs3", "7", [1])
            ras = Con(IsNull(Raster("ZONEs3")) == 1, 0, Raster("ZONEs3"))
            ras.save(self.output_location + "\\temp\\ZONE" + str(count))
            sum_rasters.append(self.output_location + "\\temp\\ZONE" + str(count))
            delete = ["Zones2", "Zones3", "SHRINK", "RASPOLY.shp", "ZONE", "SETNULL", "EM", "ALLO", "EMINT",
                      "ST_ORD.shp", "SLPDIF", "OUTPUT"]
            # for d in delete:
            #     arcpy.Delete_management(d)

            count += 1

        dsc = arcpy.Describe(dem)
        arcpy.env.extent = dsc.Extent
        ras = CellStatistics(sum_rasters, "SUM", "DATA")
        ras.save('fld')

        arcpy.gp.Reclassify_sa('fld', "Value", "0 NoData;1 1000 1", "MAJFLDPLN", "DATA")
        arcpy.RasterToPolygon_conversion("MAJFLDPLN", "MAJORFLD2", "", "VALUE")
        arcpy.FeatureclassToCoverage_conversion("MAJORFLD2.shp POLYGON", "MAJORFLD2x")
        arcpy.FeatureClassToFeatureClass_conversion("MAJORFLD2x\\polygon", self.output_location + "\\newsurface",
                                                    "MAJORFLD4",
                                                    '"GRIDCODE" = 1 OR ( "GRIDCODE" = 0 AND "AREA" < 10000 )')
        arcpy.Clip_analysis("MAJORFLD4.shp", self.processing_boundary, "MAJORFLD5.shp")
        if int(arcpy.GetCount_management("MAJORFLD5.shp").getOutput(0)) == 0:
            print 'Clip problems'
            sys.exit()
        arcpy.Buffer_analysis("MAJORFLD5.shp", "MAJORFLD6.shp", "25 Meters", "", "", "ALL")
        if int(arcpy.GetCount_management("MAJORFLD6.shp").getOutput(0)) == 0:
            print 'Buffer didn\'t work. Trying agiain'
            arcpy.Dissolve_management("MAJORFLD5.shp", "explode", "", "", "SINGLE_PART")
            arcpy.Buffer_analysis("explode.shp", "MAJORFLD6.shp", "25 Meters", "", "", "ALL")
            if int(arcpy.GetCount_management("MAJORFLD6.shp").getOutput(0)) == 0:
                print 'BUFFERS!!!!!'
                sys.exit()
        arcpy.Buffer_analysis("MAJORFLD6.shp", "MAJORFLD7.shp", "-25 Meters", "", "", "ALL")
        if no_hydro == 1:
            arcpy.Copy_management("MAJORFLD7.shp", "CND4")
        else:
            arcpy.Merge_management("MAJORFLD7.shp; " + self.hydro_features, "CND4")
        arcpy.Dissolve_management("CND4.shp", "Major_Flood_Plains")
        arcpy.Union_analysis("Major_Flood_Plains.shp;extent.shp", 'fld_extent', "ONLY_FID", gaps="NO_GAPS")
        arcpy.PolygonToRaster_conversion("fld_extent.shp", "fid_major_", "FLDPLAIN", "", "", self.cellsize)

        delete_features = ["MAJORFLD2.shp", "MAJORFLD2x", "MAJORFLD4.shp", "CND4.shp",
                           "MAJORFLD5.shp", "MAJORFLD6.shp", "MAJORFLD7.shp"]
        for feature in delete_features:
            arcpy.Delete_management(feature)

        print 'end flood plains'

    def moist(self):

        arcpy.env.workspace = self.output_location + "\\MOISTURE"
        arcpy.env.scratchworkspace = self.scratch_workspace
        dem = self.output_location + "\\newsurface\\dem_clip"
        dsc = arcpy.Describe(dem)

        if not self.CreateMoist:
            if not self.eliminateMR:
                if not arcpy.Exists(self.output_location + "\Moisture\\mr.shp"):
                    print "There is no final moisture condition created. Moving forward without it. Be aware."
            return

        if self.moisture_class_ok and arcpy.Exists('MOISTURE'):
            pass
        else:
            print "Creating Moisture Conditions @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            if arcpy.Exists(self.output_location + "\\Moisture"):
                arcpy.Delete_management(self.output_location + "\\Moisture")
            os.makedirs(self.output_location + "\\MOISTURE")
            arcpy.env.extent = dsc.Extent

            texture = self.parent_folder + '\\texture_{}'.format(str(self.substrate_version))

            hyd = 0
            list_of_required_layers1 = [self.output_location + "\\NewSurface\\Slope",
                                        self.output_location + "\\NewSurface\\STREAM_EUC",
                                        self.output_location + "\\NewSurface\\Hydro_ras"]
            list_of_required_layers2 = [self.output_location + "\\NewSurface\\Slope",
                                        self.output_location + "\\NewSurface\\STREAM_EUC"]
            for list_item in list_of_required_layers1:
                if arcpy.Exists(list_item):
                    hyd += 1
            if hyd != 3:
                for list_item in list_of_required_layers2:
                    if not arcpy.Exists(list_item):
                        print 'The Layer ' + list_item + ' does not exist. Please (re)run CreateSurface @ 0'
                        sys.exit()
                hyd = 2
                #
            # # might need some sort of histogram recognition for euclidean distance:
            # # |
            # # |
            # # |
            # # ||
            # # ||
            # # |||
            # # ||||
            # # ||||
            # # |||||
            # # ||||||||
            # # ||||||||||||||||||||||||||||||||||||||
            #
            # # Versus
            # # ||
            # # ||
            # # ||||
            # # |||||
            # # ||||||
            # # |||||||||
            # # |||||||||||
            # # ||||||||||||||
            # # ||||||||||||||||||||
            # # ||||||||||||||||||||||||||||||
            # # ||||||||||||||||||||||||||||||||||||||

            Boun = Int(Raster(dem) / Raster(dem))
            arcpy.RasterToPolygon_conversion(Boun, "EXTENT", "", "VALUE")
            arcpy.FeatureClassToFeatureClass_conversion(self.substrate, self.output_location + "\\moisture", "subsoil",
                                                        '"{}" = '.format(self.substrate_name_field) + "'Tailings Sand'")
            arcpy.AddField_management("subsoil.SHP", "GRIDCODE", "SHORT")
            arcpy.CalculateField_management("subsoil.SHP", "GRIDCODE", "0", "PYTHON_9.3")
            arcpy.Erase_analysis("EXTENT.ShP", "subsoil.SHP", "mergeme")
            arcpy.Merge_management("subsoil.SHP; mergeme.shp", "UNION")
            arcpy.PolygonToRaster_conversion("UNION.shp", "GRIDCODE", "FLD_Minus", "", "", self.cellsize)
            delete_array = ["subsoil.SHP", "mergeme.shp", "UNION.shp", "FLD_Minus", "extent.shp"]
            print 'moist2'
            slp_max = arcpy.GetRasterProperties_management(self.output_location + "\\NewSurface\\Slope", "MAXIMUM")
            ed_max = arcpy.GetRasterProperties_management(self.output_location + "\\NewSurface\\STREAM_EUC", "MAXIMUM")
            SLPMAX = float(slp_max.getOutput(0))
            EDMAX = float(ed_max.getOutput(0))
            if self.introduce_and_create_substrate_factor or self.use_existing_texture:
                arcpy.env.extent = dsc.Extent
                print 'Accounting for Substrate factor'
                if arcpy.Exists(self.output_location + "\\NewSurface\\Hydro_ras"):
                    hydrology = Raster(self.output_location + "\\NewSurface\\Hydro_ras")
                slope = Raster(self.output_location + "\\NewSurface\\slope")
                euclidean = Raster(self.output_location + "\\NewSurface\\STREAM_EUC")
                texture_ras = Raster(self.parent_folder + '\\texture_{}'.format(str(self.substrate_version)))
                slp_dif = (slope / SLPMAX)
                slp_dif.save("slp_dif")
                euc_dif = (euclidean / EDMAX)
                euc_dif.save("euc_dif")
                orig_moist = euc_dif * slp_dif
                orig_moist.save("orig_moist")
                tex = Con(Int(texture_ras * 100) == 0, float(9999), texture_ras)
                minimum = float(arcpy.GetRasterProperties_management(tex, "MINIMUM").getOutput(0))
                tex2 = Con(tex == 9999, minimum, tex)
                tex2_min = float(arcpy.GetRasterProperties_management(tex2, "MINIMUM").getOutput(0))
                tex_mex = Con((tex2 - tex2_min) >= 1, 1, Con(tex2 - tex2_min <= 0, 0, (tex2 - tex2_min)))
                mod = 0.1 / tex_mex
                mod1 = Con(IsNull(mod) == 1, 1.01, (
                        1 + (float(arcpy.GetRasterProperties_management(mod, "MINIMUM").getOutput(0))) - Con(mod > 1, 1,
                                                                                                             mod)))
                log = Log10(tex2)
                log_1neg = -1 / log
                log_1 = SetNull(Raster("fld_minus"), log_1neg, "Value = 1")
                mean = float(arcpy.GetRasterProperties_management(log_1, "MEAN").getOutput(0))
                tailings_adjust = Con(Raster("fld_minus") == 0,
                                      Con((log_1neg / mean) < 1, Power(((log_1neg / mean) * 10), 2) / 10,
                                          (log_1neg / mean)), 0)
                tailings_adjust.save("tailings_ad")
                slp_dif_tex = (tailings_adjust * slp_dif) + slp_dif
                euc_dif_tex = (tailings_adjust * euc_dif) + euc_dif

                if hyd == 3:
                    moisture = Con(hydrology == 1, 0, (
                            (slp_dif_tex * euc_dif_tex) + ((slp_dif_tex * euc_dif_tex) * texture_ras) + (
                            (slp_dif_tex * euc_dif_tex) * mod1)) * 1000)
                    # moisture = Con(hydrology == 1, 0, ((slp_dif * euc_dif) + ((slp_dif * euc_dif) * texture_ras) + ((slp_dif * euc_dif) * mod1)) * 1000)
                else:
                    moisture = ((slp_dif_tex * euc_dif_tex) + ((slp_dif_tex * euc_dif_tex) * texture_ras) + (
                            (slp_dif_tex * euc_dif_tex) * mod1)) * 1000
                    # moisture = ((slp_dif * euc_dif) + ((slp_dif * euc_dif) * texture_ras) + ((slp_dif * euc_dif) * mod1)) * 1000
            else:
                if hyd == 3:
                    moisture = Con(Raster(self.output_location + "\\NewSurface\\Hydro_ras") == 1, 0, (
                            (Raster(self.output_location + "\\NewSurface\\slope") / float(SLPMAX)) * (
                            Raster(self.output_location + "\\NewSurface\\STREAM_EUC") / float(EDMAX))) * 1000)
                else:
                    moisture = ((Raster(self.output_location + "\\NewSurface\\slope") / float(SLPMAX)) * (
                            Raster(self.output_location + "\\NewSurface\\STREAM_EUC") / float(EDMAX)) * (
                                            Raster(texture) * 10) * 100)
            moisture.save("MOISTURE")

        print '...calculate moisture class'
        arcpy.gp.Reclassify_sa("MOISTURE", "Value", self.MR_Reclass, "MOIST_CLS", "DATA")

        if not self.moisture_class_ok:
            print 'Check to ensure that the classification adequately represents expected regime'
            sys.exit()

        if self.introduce_and_create_substrate_factor or self.use_existing_texture:
            if arcpy.Exists(self.output_location + "\\NewSurface\\Hydro_ras"):
                arcpy.PolygonToRaster_conversion(self.hydro_features, "fid", "wetareas", "", "", self.cellsize)
                burned_hydro = 1
            substrate_consider = Raster(self.output_location + "\\Moisture\\FLD_Minus")
            flood = Raster(self.output_location + "\\newsurface\\FLDPLAIN")
            moist = Raster("MOIST_CLS")

            if self.is_hydrology_false:
                # adding in this for version 5 of AN where we are trying to re-create wet areas
                arcpy.PolygonToRaster_conversion(self.hydro_features, "fid", "wetareas", "", "", self.cellsize)
                wet_areas = Raster("wetareas")
                method1 = Con(flood == 1, Con(IsNull(wet_areas) == 1, 1, 0), 0)  # Flood plain not wet area
                method2 = Con(flood == 0, Con(IsNull(wet_areas) == 1, 1, 0), 0)  # Not flood plain or wet area
                method3 = Con(flood == 1, Con(IsNull(wet_areas) == 0, 1, 0), 0)  # Flood plain and wet area
                method5 = Con(flood == 0, Con(IsNull(wet_areas) == 0, 1, 0), 0)  # Only a wet area
                # method4 =  Con( method5 == 1, 1, Con(method3 == 1, 1, Con(method1 == 1,(Con(substrate_consider == 1,Con(moist > 5, 5, Con(moist > 3, moist - 1, moist)), Con(moist > 5, 5,Con(moist > 2, moist - 2, moist)))),moist)))
                method4 = Con(method5 == 1, Con(moist >= 5, 5, moist),
                              Con(method3 == 1, Con(moist > 1, moist - 1, moist),
                                  Con(method1 == 1, Con(moist > 5, 5,
                                                        Con(substrate_consider == 1,
                                                            Con(moist == 5, 4, moist),
                                                            Con(moist > 3, moist - 1,

                                                        moist))),
                                      moist)))
                # if you are a wet area and we are drier than a sub hygric, make it a mesic site, otherwise, keep original moisture call
                # if you are a wet area and in a flood plain and we are not open water, decrease mositure
                # if you are a flood plain but not wet area, and you are drier than a mesic then make it a mesic
                # if you are a flood plain but not wet area and you are not in a tailings area and you are mesic, now you are sub-hygric
                # if you are a flood plain but not wet area and you are in a tailings area and drier than a hydric, decrease moisture by 1

                method4.save("MST_RGM")
            else:
                method4 = Con(flood == 0, (
                    Con(substrate_consider == 1, Con(moist > 5, 5, Con(moist > 2, moist - 1, moist)),
                        Con(moist > 5, 5, Con(moist > 2, moist - 2, moist)))), moist)
                method4.save("MST_RGM")
            mst_rgm = 'mst_rgm'
        else:
            mst_rgm = 'moist_cls'
        if not arcpy.Exists(mst_rgm):
            print 'why do I not exist?'
            sys.exit()

        # arcpy.gp.Reclassify_sa("GB", "Value", "-1 1 1;2 2;3 3;4 4;5 5;6 6;7 7;8 8", output_location + "\\Moisture\\MST_RGM", "DATA")
        # arcpy.CopyRaster_management("MST_RGM", "Moist_RGM", "", "", "", "", "", "8_BIT_UNSIGNED")
        # To adjust slope and aspect values to wet areas.
        arcpy.gp.Reclassify_sa(mst_rgm, "Value",
                               "1 1;2 1;3 NoData;4 NoData;5 NoData;6 NoData;7 NoData; 8 NoData; ",
                               "SL_MR", "DATA")
        arcpy.gp.Expand_sa("SL_MR", "SL_MR2", "1", [1])

        arcpy.RasterToPolygon_conversion("SL_MR2", "SL_MR3")
        arcpy.MultipartToSinglepart_management("SL_MR3.shp", "SL_MR4")
        arcpy.AddField_management("SL_MR4.shp", "AREA", "DOUBLE")
        arcpy.CalculateField_management("SL_MR4.shp", "AREA", "!Shape.Area@Hectares!", "PYTHON_9.3")
        arcpy.FeatureClassToFeatureClass_conversion("SL_MR4.shp", self.output_location + "\\Moisture", "SL_MR5",
                                                    '"AREA" > 0.1')
        arcpy.AddField_management("SL_MR5.shp", "MRSL", "SHORT")
        arcpy.CalculateField_management("SL_MR5.shp", "MRSL", "1", "PYTHON_9.3")

        delete_features = ["Extent", "MAJORFLD2.shp", "MAJORFLD2x", "MAJORFLD4.shp",
                           "MAJORFLD5.shp", "MAJORFLD6.shp", "MAJORFLD7.shp", "FLDPLAIN", "SL_MR", "SL_MR2",
                           "SL_MR3.shp",
                           "SL_MR4.shp", self.output_location + "\\temp"]
        for feature in delete_features:
            try:
                arcpy.Delete_management(feature)
            except:
                continue
        print "Generalizing moisture layer @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')

        arcpy.gp.Expand_sa(mst_rgm, "MR1", "3", [4])
        arcpy.gp.Shrink_sa("MR1", "MR2", "2", [4])
        arcpy.gp.Expand_sa("MR2", "MR3", "3", [3])
        arcpy.gp.Shrink_sa("MR3", "MR4", "2", [3])
        arcpy.gp.Expand_sa("MR4", "MR5", "3", [2])
        arcpy.gp.Shrink_sa("MR5", "MR6", "2", [2])
        arcpy.gp.Expand_sa("MR6", "MR7", "2", [5])
        arcpy.gp.Shrink_sa("MR7", "MR8", "2", [5])
        arcpy.gp.Expand_sa("MR8", "MR9", "2", [6])
        arcpy.gp.Shrink_sa("MR9", "MR10", "2", [6])
        arcpy.gp.Expand_sa("MR10", "MR11", "2", [7])
        arcpy.gp.Shrink_sa("MR11", "MR12", "2", [7])
        arcpy.gp.Expand_sa("MR12", "MR13", "2", [8])
        arcpy.gp.Shrink_sa("MR12", "MR14", "2", [8])

        fix_grid_code_1 = Con(Raster(mst_rgm) == 1, 1, Raster('mr14'))
        arcpy.gp.RegionGroup_sa(fix_grid_code_1, "TP35", "EIGHT", "WITHIN")
        arcpy.gp.ZonalGeometry_sa("TP35", "VALUE", "TP36", "AREA", "1")
        arcpy.gp.SetNull("TP36", fix_grid_code_1, "TP37", 'VALUE < 10')
        arcpy.gp.FocalStatistics_sa(fix_grid_code_1, "MAJ1", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("MAJ1", "MAJ2", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("MAJ2", "MAJ3", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.FocalStatistics_sa("MAJ3", "MAJ4", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
        arcpy.gp.Int_sa("TP37", "TP37int")
        arcpy.gp.Int_sa("MAJ4", "MAJint")
        ras = Con(IsNull(Raster("TP37")) == 1, Raster("MAJ4"), Raster("TP37"))
        ras.save("TP38")
        arcpy.gp.FocalStatistics_sa("TP38", "Maj5", "Rectangle 5 5 CELL", "MEDIAN", "DATA")
        ras2 = Con(IsNull(Raster("TP38")) == 1, Raster("Maj5"), Raster("TP38"))
        ras2.save("MR99")

        arcpy.RasterToPolygon_conversion("MR99", "MR19")
        arcpy.RepairGeometry_management("MR19.shp")
        arcpy.FeatureclassToCoverage_conversion("MR19.shp POLYGON", "MR20")

        print 'Eliminating @ ' + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        if arcpy.Exists(self.output_location + "\\MOISTURE\\temp"):
            arcpy.Delete_management(self.output_location + "\\MOISTURE\\temp")
        os.makedirs(self.output_location + "\\MOISTURE\\temp")
        arcpy.env.workspace = self.output_location + "\\MOISTURE\\temp"
        arcpy.env.outputMFlag = "Disabled"
        shape = self.output_location + "\\MOISTURE\\MR20\\polygon"
        layer1 = 'layer.lyr'
        array = []
        y = int(arcpy.GetCount_management(shape).getOutput(0))
        x = 1
        count = 1
        print 'Eliminating slivers < 0.01ha'
        while y != x:
            y = x
            arcpy.MakeFeatureLayer_management(shape, layer1)
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"AREA" < 100')
            zz = int(arcpy.GetCount_management(layer1).getOutput(0))
            arcpy.Eliminate_management(layer1, "Xa{}".format(count), "AREA")
            shape = "Xa{}.shp".format(count)
            try:
                arcpy.AddField_management(shape, "AREA", "DOUBLE")
            except:
                time.sleep(60)
                arcpy.AddField_management(shape, "AREA", "DOUBLE")
            arcpy.CalculateField_management(shape, "AREA", "!Shape.Area!", "PYTHON_9.3")

            array.append(shape)
            arcpy.Delete_management(layer1)
            x = int(arcpy.GetCount_management(shape).getOutput(0))
            count += 1

        arcpy.Dissolve_management(shape, "MR_El_p01", "GRIDCODE", "", "SINGLE_PART")
        for d in array:
            arcpy.Delete_management(d)

        array = []
        print "Eliminating slivers < 0.1ha @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        shape = "MR_El_p01.shp"
        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
        arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
        code = 0
        feature_count = 1
        while code < 9:
            count = 1
            print 'working on gridcode {}'.format(str(code))

            if code == 0:
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                        '"GRIDCODE" = {} '.format(str(code)))
                if int(arcpy.GetCount_management(layer1).getOutput(0)) == 0:
                    print 'We have solved all code = 0\'s'
                else:
                    arcpy.Eliminate_management(layer1,
                                               self.output_location + "\\MOISTURE\\temp\\Move{}S{}".format(str(code),
                                                                                                           str(count)))
                    arcpy.Dissolve_management(
                        self.output_location + "\\MOISTURE\\temp\\Move{}S{}.shp".format(str(code), str(count)),
                        self.output_location + "\\MOISTURE\\temp\\Move{}Sx{}".format(str(code), str(count)),
                        "GRIDCODE", "", "SINGLE_PART")
                    arcpy.RepairGeometry_management(
                        self.output_location + "\\MOISTURE\\temp\\Move{}Sx{}.shp".format(str(code), str(count)))
                    shape = self.output_location + "\\MOISTURE\\temp\\Move{}Sx{}.shp".format(str(code), str(count))
                    arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                    arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                    arcpy.Delete_management(layer1)
                    array.append(shape)
            else:
                while feature_count > 0:
                    layer2 = 'layer2.lyr'
                    print 'Working on slivers between 0 and 0.1 - iteration {}. There are {} features left.'.format(
                        str(count), str(feature_count))
                    arcpy.Copy_management(shape, "Code{}S{}".format(str(code), str(count)))
                    shape2 = self.output_location + "\\MOISTURE\\temp\\CODE{}S{}.shp".format(str(code), str(count))
                    arcpy.MakeFeatureLayer_management(shape, layer1)
                    arcpy.MakeFeatureLayer_management(shape2, layer2)
                    # This ensures that there is only movement of 1 class by moisture regimes with areas < 0.3ha
                    arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                            '"GRIDCODE" = {} AND "AREA_ha" < 0.1'.format(str(code)))
                    feature_count = int(arcpy.GetCount_management(layer1).getOutput(0))

                    arcpy.SelectLayerByAttribute_management(layer2, "NEW_SELECTION", '"GRIDCODE" > {} '.format(
                        str(code + 1)) + 'OR "GRIDCODE" < {}'.format(str(code - 1)))

                    arcpy.Eliminate_management(layer1,
                                               self.output_location + "\\MOISTURE\\temp\\Move{}S{}".format(str(code),
                                                                                                           str(count)),
                                               "", "", layer2)
                    arcpy.Dissolve_management(
                        self.output_location + "\\MOISTURE\\temp\\Move{}S{}.shp".format(str(code), str(count)),
                        self.output_location + "\\MOISTURE\\temp\\Move{}Sy{}".format(str(code), str(count)),
                        "GRIDCODE", "", "SINGLE_PART")
                    arcpy.RepairGeometry_management(
                        self.output_location + "\\MOISTURE\\temp\\Move{}S{}.shp".format(str(code), str(count)))
                    shape = self.output_location + "\\MOISTURE\\temp\\Move{}Sy{}.shp".format(str(code), str(count))

                    try:
                        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                    except:
                        time.sleep(60)
                        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")

                    arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                    arcpy.Delete_management(layer1)
                    arcpy.Delete_management(layer2)
                    array.append(shape)
                    array.append(shape2)
                    count += 1
                    if count == 5:
                        feature_count = 0
            feature_count = 1
            code += 1
        arcpy.Copy_management(shape, "final_elim")
        arcpy.AddField_management("final_elim.shp", "MRClass", "STRING")

        arcpy.MakeFeatureLayer_management("final_elim.shp", layer1)
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 1')
        arcpy.CalculateField_management(layer1, "MRClass", '"hydric"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 2')
        arcpy.CalculateField_management(layer1, "MRClass", '"sub hydric"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 3')
        arcpy.CalculateField_management(layer1, "MRClass", '"hygric"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 4')
        arcpy.CalculateField_management(layer1, "MRClass", '"sub hygric"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 5')
        arcpy.CalculateField_management(layer1, "MRClass", '"mesic"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 6')
        arcpy.CalculateField_management(layer1, "MRClass", '"sub mesic"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 7')
        arcpy.CalculateField_management(layer1, "MRClass", '"sub xeric"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 8')
        arcpy.CalculateField_management(layer1, "MRClass", '"xeric"', "PYTHON_9.3")
        arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")
        arcpy.AddField_management(layer1, "MR_GRID", "SHORT")
        arcpy.CalculateField_management(layer1, "MR_GRID", '!GRIDCODE!', 'PYTHON_9.3')
        arcpy.Dissolve_management(layer1, self.output_location + "\\MOISTURE\\MR", ["MRCLASS", "MR_GRID"], "",
                                  "SINGLE_PART")
        arcpy.Delete_management(self.output_location + "\\MOISTURE\\temp")
        # for d in array:
        #     try:
        #         arcpy.Delete_management(d)
        #     except:
        #         continue

        delete_features = ["MR3", "MR4", "MR5", "MR6", "MR7", "MR8", "MR9", "MR10", "MR11", "MR12", "MR13",
                           "MR14", "MR15", "MR16", "MR17", "MR18", "MR19.shp", "MR20", layer1, "MRA.shp",
                           "TP35", "TP36", "TP37", "MAJ1", "MAJ2", "MAJ3", "MAJ4", "TP37int", "MAJint", "TP38",
                           "MR99",
                           "MR_El_p01.shp"]
        for d in delete_features:
            try:
                arcpy.Delete_management(d)
            except:
                continue

        print 'Done Creating Moisture Regime'

    def topographic(self):
        layer1 = 'layer1.lyr'
        layer2 = 'layer2.lyr'
        dem = self.output_location + "\\newsurface\\dem_clip"
        if self.CreateConditionSet > 0:
            pass
        if self.BoundaryMatch > 0:
            pass
        else:
            if not arcpy.Exists(self.ExistingEcosites):
                print 'You have not pointed to any existing ecosite layer. Turn off boundary match or point to layer.'
                sys.exit()
            print "Processing boundary ecosites @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            if arcpy.Exists(self.output_location + "\\BNDRYMTCH"):
                arcpy.Delete_management(self.output_location + "\\BNDRYMTCH")
            os.makedirs(self.output_location + "\\BNDRYMTCH")
            arcpy.env.workspace = self.output_location + "\\BNDRYMTCH"
            dsc = arcpy.Describe(dem)
            arcpy.env.extent = dsc.Extent

            try:
                arcpy.Dissolve_management(self.ExistingEcosites, "ECOPHASE", ["ECOPHASE", "SITENUM", "PHASENUM"], "",
                                          "SINGLE_PART")
            except:
                print 'Your boundary match layer must have the populated fields "ECOPHASE", "SITENUM", and "PHASENUM" of type' \
                      ' string\short integer\short integer with string in lower case (e.g. a1 or h3).'
                sys.exit()
            arcpy.AddField_management("ECOPHASE.SHP", "CND", "STRING")
            arcpy.CalculateField_management("ECOPHASE.SHP", "CND", "!ECOPHASE!", 'PYTHON_9.3')

        if self.ProcessSLPChar > 0:
            pass
        else:

            print "Creating Slope Factor @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            print datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            if arcpy.Exists(self.output_location + "\\Slope"):
                arcpy.Delete_management(self.output_location + "\\Slope")
            os.makedirs(self.output_location + "\\Slope")
            arcpy.env.workspace = self.output_location + "\\Slope"
            dsc = arcpy.Describe(dem)
            arcpy.env.extent = dsc.Extent

            arcpy.gp.Expand_sa(self.output_location + "\\NewSurface\\" + "SLPCLS", "SL1", "3", [1])
            arcpy.gp.Shrink_sa("SL1", "SL2", "3", [1])
            arcpy.gp.Expand_sa("SL2", "SL3", "3", [2])
            arcpy.gp.Shrink_sa("SL3", "SL4", "3", [2])
            arcpy.gp.Expand_sa("SL4", "SL5", "2", [3])
            arcpy.gp.Shrink_sa("SL5", "SL6", "2", [3])
            arcpy.gp.Expand_sa("SL6", "SL7", "2", [4])
            arcpy.gp.Shrink_sa("SL7", "SL8", "2", [4])
            arcpy.gp.Expand_sa("SL8", "SL9", "1", [5])
            arcpy.gp.Shrink_sa("SL9", "SL10", "1", [5])
            arcpy.gp.Expand_sa("SL10", "SL11", "1", [6])
            arcpy.gp.Shrink_sa("SL11", "SL12", "1", [6])
            arcpy.gp.Expand_sa("SL12", "SL13", "1", [7])
            arcpy.gp.Shrink_sa("SL13", "SL14", "1", [7])
            arcpy.gp.Expand_sa("SL14", "SL15", "1", [8])
            arcpy.gp.Shrink_sa("SL15", "SL16", "1", [8])

            arcpy.RasterToPolygon_conversion("SL16", "SL17")
            arcpy.RepairGeometry_management("SL17.shp")

            arcpy.FeatureclassToCoverage_conversion("SL17.shp POLYGON", "SL18", "", "DOUBLE")
            # print 'Eliminate "Moisture_cov" poly and arc in PostGre SQL and save "eliminated" as "MR.SHP" inside of the folder {}'.format(str(arcpy.env.workspace))

            shape = "SL18\\polygon"

            array = []
            y = int(arcpy.GetCount_management(shape).getOutput(0))
            x = 1
            count = 1
            print "Eliminating slivers < 0.1ha @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            while y != x:
                y = x
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"AREA" < 1000')
                zz = int(arcpy.GetCount_management(layer1).getOutput(0))
                print str(zz)
                arcpy.Eliminate_management(layer1, "Xa{}".format(count), "AREA")
                shape = "Xa{}.shp".format(count)
                try:
                    arcpy.AddField_management(shape, "AREA", "DOUBLE")
                except:
                    time.sleep(10)
                    arcpy.AddField_management(shape, "AREA", "DOUBLE")
                arcpy.CalculateField_management(shape, "AREA", "!Shape.Area!", "PYTHON_9.3")

                array.append(shape)
                arcpy.Delete_management(layer1)
                x = int(arcpy.GetCount_management(shape).getOutput(0))
                count += 1

            arcpy.FeatureClassToFeatureClass_conversion(shape, self.output_location + "\Slope", "SLA")
            for d in array:
                arcpy.Delete_management(d)
            arcpy.AddField_management("SLA.shp", "SLClass", "TEXT", "", "", "25", "", "NULLABLE",
                                      "NON_REQUIRED", "")

            arcpy.Union_analysis(["SLA.shp", self.output_location + "\\moisture\\SL_MR5.shp"], "SL_MR6", "", "",
                                 "NO_GAPS")
            arcpy.MakeFeatureLayer_management("SL_MR6.shp", layer1)

            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 1')
            arcpy.CalculateField_management(layer1, "SLClass", '"level"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 2')
            arcpy.CalculateField_management(layer1, "SLClass", '"2 - 5"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 3')
            arcpy.CalculateField_management(layer1, "SLClass", '"6 - 9"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 4')
            arcpy.CalculateField_management(layer1, "SLClass", '"10 - 15"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 5')
            arcpy.CalculateField_management(layer1, "SLClass", '"16 - 30"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 6')
            arcpy.CalculateField_management(layer1, "SLClass", '"31 - 45"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 7')
            arcpy.CalculateField_management(layer1, "SLClass", '"46 - 70"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 8')
            arcpy.CalculateField_management(layer1, "SLClass", '"70+"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"MRSL" = 1')
            arcpy.CalculateField_management(layer1, "SLCLASS", '"level"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")
            arcpy.AddField_management(layer1, "SL_GRID", "SHORT")
            arcpy.CalculateField_management(layer1, "SL_GRID", "!GRIDCODE!", 'PYTHON_9.3')
            arcpy.Dissolve_management(layer1, "SL", ["SLCLASS", "SL_GRID"], "", "SINGLE_PART")

            delete_features = ["SL1", "SL2", "SL3", "SL4", "SL5", "SL6", "SL7", "SL8", "SL9", "SL10", "SL11", "SL12",
                               "SL13", "SL14", "SL15", "SL16", "SLA.shp", "SL17.shp", "sl18", "in_memory\\", layer1]
            self.delete(delete_features)

        if self.ProcessASPChar > 0:
            pass
        else:
            print "Creating Aspect Factor @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')

            if arcpy.Exists(self.output_location + "\\Aspect"):
                arcpy.Delete_management(self.output_location + "\\Aspect")
            os.makedirs(self.output_location + "\\Aspect")
            arcpy.env.workspace = self.output_location + "\\Aspect"
            dsc = arcpy.Describe(self.output_location + "\\Newsurface\\ASPTCLS")
            arcpy.env.extent = dsc.Extent

            arcpy.gp.Expand_sa(self.output_location + "\\Newsurface\\ASPTCLS", "AS1", "3", [1])
            arcpy.gp.Shrink_sa("AS1", "AS2", "3", [1])
            arcpy.gp.Expand_sa("AS2", "AS3", "2", [2])
            arcpy.gp.Shrink_sa("AS3", "AS4", "2", [2])
            arcpy.gp.Expand_sa("AS4", "AS5", "2", [3])
            arcpy.gp.Shrink_sa("AS5", "AS6", "2", [3])
            arcpy.gp.Expand_sa("AS6", "AS7", "2", [4])
            arcpy.gp.Shrink_sa("AS7", "AS8", "2", [4])
            arcpy.gp.Expand_sa("AS8", "AS9", "2", [5])
            arcpy.gp.Shrink_sa("AS9", "AS10", "2", [5])

            arcpy.gp.Expand_sa("AS10", "AS11", "3", [1])
            arcpy.gp.Shrink_sa("AS11", "AS12", "3", [1])
            arcpy.gp.Expand_sa("AS12", "AS13", "2", [2])
            arcpy.gp.Shrink_sa("AS13", "AS14", "2", [2])
            arcpy.gp.Expand_sa("AS14", "AS15", "2", [3])
            arcpy.gp.Shrink_sa("AS15", "AS16", "2", [3])
            arcpy.gp.Expand_sa("AS16", "AS17", "2", [4])
            arcpy.gp.Shrink_sa("AS17", "AS18", "2", [4])
            arcpy.gp.Expand_sa("AS18", "AS19", "2", [5])
            arcpy.gp.Shrink_sa("AS19", "AS20", "2", [5])

            # arcpy.gp.FocalStatistics_sa("AS10", "AS11", "Rectangle 3 3 CELL", "MAJORITY", "NODATA")

            arcpy.RasterToPolygon_conversion("AS20", "AS21")
            arcpy.FeatureclassToCoverage_conversion("AS21.shp POLYGON", "AS22")
            shape = "AS22\polygon"

            array = []
            y = int(arcpy.GetCount_management(shape).getOutput(0))
            x = 1
            count = 1
            print "eliminating slivers @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            while y != x:
                y = x
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"AREA" < 1000')
                arcpy.Eliminate_management(layer1, "Xa{}".format(count), "AREA")
                shape = "Xa{}.shp".format(count)
                try:
                    arcpy.AddField_management(shape, "AREA", "DOUBLE")
                except:
                    time.sleep(10)
                    arcpy.AddField_management(shape, "AREA", "DOUBLE")
                arcpy.CalculateField_management(shape, "AREA", "!Shape.Area!", "PYTHON_9.3")

                array.append(shape)
                arcpy.Delete_management(layer1)
                x = int(arcpy.GetCount_management(shape).getOutput(0))
                count += 1

            arcpy.AddField_management(shape, "asClass", "TEXT")
            arcpy.MakeFeatureLayer_management(shape, layer1)
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 0 OR "GRIDCODE" = 1')
            arcpy.CalculateField_management(layer1, "ASClass", '"level"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 2')
            arcpy.CalculateField_management(layer1, "ASClass", '"northerly"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 3')
            arcpy.CalculateField_management(layer1, "ASClass", '"easterly"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 4')
            arcpy.CalculateField_management(layer1, "ASClass", '"southerly"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 5')
            arcpy.CalculateField_management(layer1, "ASClass", '"westerly"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")
            arcpy.AddField_management(layer1, "AS_GRID", "SHORT")
            arcpy.CalculateField_management(layer1, "AS_GRID", "!GRIDCODE!", 'PYTHON_9.3')
            arcpy.Dissolve_management(shape, "AS", ["ASCLASS", "AS_GRID"], "", "SINGLE_PART")
            for d in array:
                arcpy.Delete_management(d)
            delete_features = ["AS1", "AS2", "AS3", "AS4", "AS5", "AS6", "AS7", "AS8", "AS9", "AS10", "AS11", "AS12",
                               "AS13",
                               "AS14", "AS15", "AS16", "AS17", "AS18", "AS19", "AS20", "AS21.shp", "AS22", "AS23.shp",
                               "in_memory\\", layer1]
            self.delete(delete_features)

        if self.ProcessTopo > 0:
            pass
        else:
            print "Creating Topography Factor @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            if arcpy.Exists(self.output_location + "\\TopoPosition"):
                arcpy.Delete_management(self.output_location + "\\TopoPosition")
            os.makedirs(self.output_location + "\\TopoPosition")
            arcpy.env.workspace = self.output_location + "\\TopoPosition"
            dsc = arcpy.Describe(dem)
            arcpy.env.extent = dsc.Extent

            arcpy.Resample_management(dem, "dem_9", "9")
            arcpy.gp.FocalStatistics_sa(dem, "dem_m", "Circle 15 CELL", "MEAN", "DATA")
            arcpy.gp.FocalStatistics_sa("dem_9", "dem_m9", "Circle 81 CELL", "MEAN", "DATA")
            tpi_dem = Raster(dem) - Raster("dem_m")
            tpi_dem9 = Raster("dem_9") - Raster("dem_m9")
            tpi_dem.save("TPI")
            tpi_dem9.save("TPI9")

            arcpy.gp.Slope_sa(dem, "SLOPE", "DEGREE", "1")
            arcpy.gp.Reclassify_sa("SLOPE", "Value", "0 6 1;6 5000 NoData", "slopeL", "DATA")
            arcpy.gp.Reclassify_sa("SLOPE", "Value", "0 6 NoData;6 5000 1", "slopeH", "DATA")
            arcpy.gp.Expand_sa("slopeL", "slopeLE", "5", [1])
            arcpy.gp.Expand_sa("slopeH", "slopeHE", "5", [1])
            arcpy.gp.Shrink_sa("slopeLE", "slopeLES", "10", [1])
            arcpy.gp.Shrink_sa("slopeHE", "slopeHES", "10", [1])
            dem_add = Int(
                ((Raster(dem) / Raster(dem)) - 1) + (Con(IsNull(Raster("slopeHES")) == 1, 0, Raster("slopeHES"))))
            dem_add.save("dem_add")
            arcpy.gp.RegionGroup_sa(dem_add, "demadd_reg", "EIGHT", "WITHIN")
            arcpy.gp.ZonalGeometry_sa("demadd_reg", "VALUE", "dem_zonal", "AREA")
            con_zonal = Con(Raster("dem_add") == 0, Con(Raster("dem_zonal") < 50000, 1, Raster("dem_add")),
                            Raster("dem_add"))
            con_zonal.save("con_zonal")
            arcpy.gp.Shrink_sa(con_zonal, "con_zs", "20", [1])
            arcpy.gp.Expand_sa("con_zs", "con_zs2", "20", [1])

            TPReclass = "-5000 -0.05 1; -0.05 0.05 2;0.05 5000 3"
            TPReclass9 = "-5000 -06 1; -6 7 2;7 5000 3"
            SLPReclass = "0 2 100;2 6 200;6 10 300;10 16 400;16 31 500;31 90 600"
            arcpy.gp.Reclassify_sa("TPI", "Value", TPReclass, "TPI_CLS", "DATA")
            arcpy.gp.Reclassify_sa("TPI9", "Value", TPReclass9, "TPI_CLS9", "DATA")
            arcpy.gp.Reclassify_sa("SLOPE", "Value", SLPReclass, "SLOPE_TPI", "DATA")
            arcpy.env.cellsize = 1
            arcpy.Resample_management("TPI_CLS9", "TPI_CLS1", "1")

            add = Raster("TPI_CLS") + Raster("SLOPE_TPI")
            add2 = Raster("TPI_CLS1") + Raster("SLOPE_TPI")
            add.save("tpi_2_cls")
            add2.save("tpi_2_cls9")
            arcpy.gp.Reclassify_sa("tpi_2_cls", "Value",
                                   "101 1;102 2;103 2;201 3;202 4; 203 7;301 4;302 5;303 6;401 4;402 5;403 6;501 5;502 6;503 6;601 5;602 5;603 6",
                                   "tpis_final", "DATA")
            arcpy.gp.Reclassify_sa("tpi_2_cls9", "Value",
                                   "101 203 NoData;301 4;302 5;303 6;401 4;402 5;403 6;501 5;502 5;503 6;601 4;602 5;603 6",
                                   "tpi9_final", "DATA")
            arcpy.gp.Expand_sa("tpi9_final", "con_zs24", "20", [4])
            arcpy.gp.Expand_sa("con_zs24", "con_zs25", "20", [5])
            arcpy.gp.Expand_sa("con_zs25", "con_zs26", "20", [6])
            arcpy.gp.Expand_sa("con_zs26", "con_zs27", "20", [7])
            tpi_con = Con(Raster("con_zs2") == 0, Raster("tpis_final"),
                          (Con(IsNull(Raster("tpi9_final")) == 1, Raster("con_zs27"), Raster("tpi9_final"))))
            tpi_con.save("final_tpi")
            array = ["dem_9", "dem_m", "dem_m9", "TPI", "TPI9", "TPI_CLS", "TPI_CLS9", "SLOPE_TPI", "tpi_2_cls",
                     "tpi_2_cls9",
                     "tpis_final", "tpi9_final", "dem_add", "dem_zonal", "demadd_reg", "con_zs", "con_zs2",
                     "con_zonal", "slope", "slope", "slopehe", "slopehes", "slopel", "slopele", "slopeles", "tpi_cls1",
                     "con_zs24", "con_zs25", "con_zs26", "con_zs27"]
            for feature in array:
                try:
                    arcpy.Delete_management(feature)
                except:
                    continue

            arcpy.gp.Expand_sa("final_tpi", "TP7", "1", [1])
            arcpy.gp.Shrink_sa("TP7", "TP8", "1", [1])
            arcpy.gp.Expand_sa("TP8", "TP9", "1", [2])
            arcpy.gp.Shrink_sa("TP9", "TP10", "1", [2])
            arcpy.gp.Expand_sa("TP10", "TP11", "1", [3])
            arcpy.gp.Shrink_sa("TP11", "TP12", "1", [3])
            arcpy.gp.Expand_sa("TP12", "TP13", "1", [4])
            arcpy.gp.Shrink_sa("TP13", "TP14", "1", [4])
            arcpy.gp.Expand_sa("TP14", "TP15", "1", [5])
            arcpy.gp.Shrink_sa("TP15", "TP16", "1", [5])
            arcpy.gp.Expand_sa("TP16", "TP17", "1", [6])
            arcpy.gp.Shrink_sa("TP17", "TP18", "1", [6])
            arcpy.gp.Expand_sa("TP18", "TP19", "1", [7])
            arcpy.gp.Shrink_sa("TP19", "TP20", "1", [7])
            arcpy.gp.Expand_sa("TP20", "TP21", "1", [1])
            arcpy.gp.Shrink_sa("TP21", "TP22", "1", [1])
            arcpy.gp.Expand_sa("TP22", "TP23", "1", [2])
            arcpy.gp.Shrink_sa("TP23", "TP24", "1", [2])
            arcpy.gp.Expand_sa("TP24", "TP25", "1", [3])
            arcpy.gp.Shrink_sa("TP25", "TP26", "1", [3])
            arcpy.gp.Expand_sa("TP26", "TP27", "1", [4])
            arcpy.gp.Shrink_sa("TP27", "TP28", "1", [4])
            arcpy.gp.Expand_sa("TP28", "TP29", "1", [5])
            arcpy.gp.Shrink_sa("TP29", "TP30", "1", [5])
            arcpy.gp.Expand_sa("TP30", "TP31", "1", [6])
            arcpy.gp.Shrink_sa("TP31", "TP32", "1", [6])
            arcpy.gp.Expand_sa("TP32", "TP33", "1", [7])
            arcpy.gp.Shrink_sa("TP33", "TP34", "1", [7])

            arcpy.gp.RegionGroup_sa("TP34", "TP35", "EIGHT", "WITHIN")
            arcpy.gp.ZonalGeometry_sa("TP35", "VALUE", "TP36", "AREA", "1")
            arcpy.gp.SetNull("TP36", "TP34", "TP37", 'VALUE < 10')
            arcpy.gp.FocalStatistics_sa("TP34", "MAJ1", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
            arcpy.gp.FocalStatistics_sa("MAJ1", "MAJ2", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
            arcpy.gp.FocalStatistics_sa("MAJ2", "MAJ3", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
            arcpy.gp.FocalStatistics_sa("MAJ3", "MAJ4", "Rectangle 5 5 CELL", "MAJORITY", "DATA")
            arcpy.gp.Int_sa("TP37", "TP37int")
            arcpy.gp.Int_sa("MAJ4", "MAJint")
            ras = Con(IsNull(Raster("TP37")) == 1, Raster("MAJ4"), Raster("TP37"))
            ras.save("TP38")
            arcpy.gp.FocalStatistics_sa("TP38", "Maj5", "Rectangle 5 5 CELL", "MEDIAN", "DATA")
            ras2 = Con(IsNull(Raster("TP38")) == 1, Raster("Maj5"), Raster("TP38"))
            ras2.save("Tp40")

            arcpy.RasterToPolygon_conversion("TP40", "TP41")
            arcpy.FeatureclassToCoverage_conversion("TP41.shp POLYGON", "TP42")

            shape = self.output_location + "\\TopoPosition\\TP42\\polygon"
            os.makedirs(self.output_location + "\\TopoPosition\\temp")
            arcpy.env.workspace = self.output_location + "\TopoPosition\\temp"
            array = []
            y = int(arcpy.GetCount_management(shape).getOutput(0))
            x = 1
            count = 1
            print "Eliminating slivers < 0.01ha @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            while y != x:
                y = x
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"AREA" < 100')
                zz = int(arcpy.GetCount_management(layer1).getOutput(0))
                arcpy.Eliminate_management(layer1, "Xa{}".format(count), "AREA")
                shape = "Xa{}.shp".format(count)
                try:
                    arcpy.AddField_management(shape, "AREA", "DOUBLE")
                except:
                    time.sleep(10)
                    arcpy.AddField_management(shape, "AREA", "DOUBLE")
                arcpy.CalculateField_management(shape, "AREA", "!Shape.Area!", "PYTHON_9.3")

                array.append(shape)
                arcpy.Delete_management(layer1)
                x = int(arcpy.GetCount_management(shape).getOutput(0))
                count += 1

            print 'Finished eliminating slivers < 0.01ha @ ' + datetime.datetime.fromtimestamp(time.time()).strftime(
                '%H:%M:%S')
            arcpy.Dissolve_management(shape, "TP_El_p01", "GRIDCODE", "", "SINGLE_PART")
            for d in array:
                arcpy.Delete_management(d)

            array = []
            print "Eliminating slivers < 0.1ha @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            shape = "TP_El_p01.shp"
            arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
            arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
            code = 0
            feature_count = 1
            layer2 = 'layer2.lyr'
            while code < 8:
                count = 1
                print 'working on gridcode {}'.format(str(code))

                if code == 0:
                    arcpy.MakeFeatureLayer_management(shape, layer1)
                    arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                            '"GRIDCODE" = {} '.format(str(code)))
                    if int(arcpy.GetCount_management(layer1).getOutput(0)) == 0:
                        print 'We have solved all code = 0\'s'
                    else:
                        arcpy.Eliminate_management(layer1,
                                                   self.output_location + "\\TopoPosition\\temp\\Move{}S{}".format(
                                                       str(code),
                                                       str(count)))
                        arcpy.Dissolve_management(
                            self.output_location + "\\TopoPosition\\temp\\Move{}S{}.shp".format(str(code), str(count)),
                            self.output_location + "\\TopoPosition\\temp\\Move{}S2{}".format(str(code), str(count)),
                            "GRIDCODE", "", "SINGLE_PART")
                        arcpy.RepairGeometry_management(
                            self.output_location + "\\TopoPosition\\temp\\Move{}S2{}.shp".format(str(code), str(count)))
                        shape = self.output_location + "\\TopoPosition\\temp\\Move{}S2{}.shp".format(str(code),
                                                                                                     str(count))
                        arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                        arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                        arcpy.Delete_management(layer1)
                        array.append(shape)
                else:
                    while feature_count > 0:

                        print 'Working on slivers between 0 and 0.1 - iteration {}. There are {} features left.'.format(
                            str(count), str(feature_count))
                        arcpy.Copy_management(shape, "Code{}S{}".format(str(code), str(count)))
                        shape2 = self.output_location + "\\TopoPosition\\temp\\CODE{}S{}.shp".format(str(code),
                                                                                                     str(count))
                        arcpy.MakeFeatureLayer_management(shape, layer1)
                        arcpy.MakeFeatureLayer_management(shape2, layer2)
                        # This ensures that there is only movement of 1 class by moisture regimes with areas < 0.3ha
                        arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                                '"GRIDCODE" = {} AND "AREA_ha" < 0.25'.format(str(code)))
                        feature_count = int(arcpy.GetCount_management(layer1).getOutput(0))

                        arcpy.SelectLayerByAttribute_management(layer2, "NEW_SELECTION", '"GRIDCODE" > {} '.format(
                            str(code + 1)) + 'OR "GRIDCODE" < {}'.format(str(code - 1)))

                        arcpy.Eliminate_management(layer1,
                                                   self.output_location + "\\TopoPosition\\temp\\Move{}S{}".format(
                                                       str(code),
                                                       str(count)), "",
                                                   "", layer2)
                        arcpy.Dissolve_management(
                            self.output_location + "\\TopoPosition\\temp\\Move{}S{}.shp".format(str(code), str(count)),
                            self.output_location + "\\TopoPosition\\temp\\Move{}S2{}".format(str(code), str(count)),
                            "GRIDCODE", "", "SINGLE_PART")
                        arcpy.RepairGeometry_management(
                            self.output_location + "\\TopoPosition\\temp\\Move{}S2{}.shp".format(str(code), str(count)))
                        shape = self.output_location + "\\TopoPosition\\temp\\Move{}S2{}.shp".format(str(code),
                                                                                                     str(count))

                        try:
                            arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                        except:
                            time.sleep(10)
                            arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")

                        arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                        arcpy.Delete_management(layer1)
                        arcpy.Delete_management(layer2)
                        array.append(shape)
                        array.append(shape2)
                        count += 1
                        if count == 5:
                            feature_count = 0
                feature_count = 1
                code += 1

            arcpy.env.workspace = self.output_location + "\TopoPosition\\"

            arcpy.AddField_management(shape, "TPClass", "TEXT")
            arcpy.MakeFeatureLayer_management(shape, layer1)
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 1')
            arcpy.CalculateField_management(layer1, "TPClass", '"depression"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 2')
            arcpy.CalculateField_management(layer1, "TPClass", '"level"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 3')
            arcpy.CalculateField_management(layer1, "TPClass", '"toe"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 4')
            arcpy.CalculateField_management(layer1, "TPClass", '"lower slope"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 5')
            arcpy.CalculateField_management(layer1, "TPClass", '"mid slope"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 6')
            arcpy.CalculateField_management(layer1, "TPClass", '"upper slope"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"GRIDCODE" = 7')
            arcpy.CalculateField_management(layer1, "TPClass", '"crest"', "PYTHON_9.3")
            arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")

            arcpy.AddField_management(layer1, "TP_GRID", "SHORT")
            arcpy.CalculateField_management(layer1, "TP_GRID", "!GRIDCODE!", 'PYTHON_9.3')
            arcpy.Dissolve_management(shape, self.output_location + "\TopoPosition\TP", ["TPCLASS", "TP_GRID"], "",
                                      "SINGLE_PART")
            arcpy.Delete_management(self.output_location + "\TopoPosition\\temp")
            delete_features = ["TP1", "TP2", "TP3", "TP6", "TP7", "TP8", "TP9", "TP10", "TP11", "TP12", "TP13",
                               "TPab.shp"
                               "TP14", "TP15",
                               "TP16", "TP17", "TP18", "TP19", "TP20", "TP21", "TP22", "TP23", "TP24", "TP25",
                               "TP26", "TP27", "TP28", "TP29", "TP30", "TP31", "TP32", "TP33", "TP34", "TP35", "TP36",
                               "TP37",
                               "TP38", "TP39", "TP40", "MAJ1", "MAJ2", "MAJ3", "MAJ4", "MAJINT", "TP37INT", "TP41.shp",
                               "TP42.shp", "TP43.shp",
                               "TP44.shp", "in_memory\\", "TPAA.shp", layer1, "Maj5", "tp42", "tp14"]
            self.delete(delete_features)

        if self.CreateSoilNut > 0:
            pass
        else:
            print "Creating Soil Nutrient Datasets @" + datetime.datetime.fromtimestamp(time.time()).strftime(
                '%H:%M:%S')
            if arcpy.Exists(self.output_location + "\SOIL"):
                arcpy.Delete_management(self.output_location + "\SOIL")
            os.makedirs(self.output_location + "\SOIL")
            arcpy.env.workspace = self.output_location + "\SOIL"
            dsc = arcpy.Describe(dem)
            arcpy.env.extent = dsc.Extent

            arcpy.Dissolve_management(self.output_location + "/as.shp", "SOILCLS")
            arcpy.AddField_management("SOILCLS.shp", "SoilCLS", "SHORT")
            arcpy.CalculateField_management("SOILCLS.shp", "SoilCLS", '3', "PYTHON_9.3")

    def condition_set(self):
        layer1 = 'layer1.lyr'
        if self.CreateConditionSet:

            # datasets = [self.output_location + "\\moisture\\MR.shp",
            #             self.output_location + "\\TopoPosition\\TP.shp",
            #             self.output_location + "\\Aspect\\as.shp",
            #             self.output_location + "\\Slope\\SL.shp", self.output_location + "\\Soil\\SoilCLS.shp",
            #             self.substrate]
            datasets = [self.output_location + "\\MR.shp",
                        self.output_location + "\\TP.shp",
                        self.output_location + "\\as.shp",
                        self.output_location + "\\SL.shp", self.output_location + "\\SoilCLS.shp",
                        self.substrate]
            for r in datasets:
                if not arcpy.Exists(r):
                    print 'You have not run the ' + r + ' dataset yet!'
                    sys.exit()
            print "Creating Conditional Datasets @" + datetime.datetime.fromtimestamp(time.time()).strftime(
                '%H:%M:%S')
            if arcpy.Exists(self.output_location + "\\CND"):
                arcpy.Delete_management(self.output_location + "\\CND")
            os.makedirs(self.output_location + "\\CND")
            os.makedirs(self.output_location + "\\CND\\TEMP")
            arcpy.env.workspace = self.output_location + "\\CND"
            desc = arcpy.Describe(self.output_location + "\\Moisture\\MR.shp")
            arcpy.env.extent = desc.Extent

            arcpy.CreateFishnet_management(self.output_location + "\\CND\\default",
                                           str(desc.extent.XMin + 10) + " " + str(desc.extent.YMin + 10),
                                           str(desc.extent.XMin + 20) + " " + str(desc.extent.YMin + 10), "0", "0",
                                           "5",
                                           "5",
                                           str(desc.extent.XMin + 20) + " " + str(desc.extent.YMin + 20),
                                           "NO_labelS", "#",
                                           "POLYGON")
            arcpy.AddField_management(self.output_location + "\\CND\\default.shp", "CND", "STRING")
            ecophase_array = ['a1', 'b1', 'b2', 'b3', 'b4', 'c1', 'd1', 'd2', 'd3', 'e1', 'e2', 'e3', 'f1', 'f2',
                              'f3',
                              'g1',
                              'h1', 'i1', 'i2', 'j1', 'j2', 'k1', 'k2', 'k3', 'l1']
            array_count = 0
            arcpy.MakeFeatureLayer_management(self.output_location + "\\CND\\default.shp", layer1)

            for row in arcpy.SearchCursor(self.output_location + "\\CND\\default.shp"):
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                        '"FID" = {}'.format(str(array_count)))
                arcpy.CalculateField_management(layer1, "CND", '"{}"'.format(str(ecophase_array[array_count])),
                                                'PYTHON_9.3')
                arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")
                array_count += 1
            arcpy.Delete_management(layer1)

            new_array = []
            value = 1
            FieldMappings = arcpy.FieldMappings()
            for feature in datasets:
                arcpy.MakeFeatureLayer_management(feature, layer1)
                fieldList = []
                for field in arcpy.ListFields(layer1):
                    if field.name in ['Shape', 'FID', 'Area_ha', 'Area']:
                        continue
                    fieldList.append(field.name)
                arcpy.env.extent = "MAXOF"
                arcpy.Dissolve_management(feature, "feature{}".format(str(value)), fieldList, "", "SINGLE_PART")
                arcpy.ClearEnvironment("extent")
                new_array.append("feature{}.shp".format(str(value)))
                arcpy.Delete_management(layer1)
                value += 1

            for x in new_array:
                if arcpy.Exists("INT.shp"):
                    arcpy.Delete_management("INT.shp")
                arcpy.Intersect_analysis(x, "INT.shp")
                if int(arcpy.GetCount_management("INT.shp").getOutput(0)) == 0:
                    pass
                else:
                    print x + ' has overlapping features. Fix these and re-run. View the overlapping features in: INT.shp'
                    sys.exit()
                arcpy.Delete_management("INT.shp")

            arcpy.Union_analysis(new_array, "UN1", "", "", "NO_GAPS")
            arcpy.Clip_analysis('un1.shp', self.footprint, 'un2')

            for d in new_array:
                arcpy.Delete_management(d)
            arcpy.MakeFeatureLayer_management("UN2.SHP", layer1)

            # FIX Slope classes outside that of the natural system
            # arcpy.AddField_management(layer1, "SOILCLS2", "SHORT")
            # I think that we are going to have to put in an adjuster for other fields based on slope classes > 10-15.
            # For now the following is used:
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'70+\' AND "SOILCLS" >= 2')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 1")
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'70+\' AND "SOILCLS" >= 3')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 2")
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'70+\' AND "SOILCLS" >= 4')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'70+\'')
            arcpy.CalculateField_management(layer1, "SLCLASS", '"10 - 15"', 'PYTHON_9.3')

            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'46 - 70\' AND "SOILCLS" >= 2')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 1")
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'46 - 70\' AND "SOILCLS" >= 3')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 2")
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'46 - 70\' AND "SOILCLS" >= 4')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 3")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'46 - 70\'')
            arcpy.CalculateField_management(layer1, "SLCLASS", '"10 - 15"', 'PYTHON_9.3')

            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'31 - 45\' AND "SOILCLS" >= 3')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 1")
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'31 - 45\' AND "SOILCLS" >= 4')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 2")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'31 - 45\'')
            arcpy.CalculateField_management(layer1, "SLCLASS", '"10 - 15"', 'PYTHON_9.3')

            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'16 - 30\' AND "SOILCLS" >= 4')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS] - 1")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SLCLASS" = \'16 - 30\'')
            arcpy.CalculateField_management(layer1, "SLCLASS", '"10 - 15"', 'PYTHON_9.3')
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SOILCLS2" = 0')
            # arcpy.CalculateField_management(layer1, "SOILCLS2", "[SOILCLS]")

            arcpy.AddField_management(layer1, "SOILTXT", "TEXT")
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SOILCLS" = 1')
            arcpy.CalculateField_management(layer1, "SOILTXT", '"very poor"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SOILCLS" = 2')
            arcpy.CalculateField_management(layer1, "SOILTXT", '"poor"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SOILCLS" = 3')
            arcpy.CalculateField_management(layer1, "SOILTXT", '"medium"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SOILCLS" = 4')
            arcpy.CalculateField_management(layer1, "SOILTXT", '"rich"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"SOILCLS" = 5')
            arcpy.CalculateField_management(layer1, "SOILTXT", '"very rich"', 'PYTHON_9.3')
            arcpy.SelectLayerByAttribute_management(layer1, "CLEAR_SELECTION")
            arcpy.Delete_management(layer1)

            array = []
            arcpy.AddField_management("UN2.shp", "CNDClass", "TEXT")
            arcpy.CalculateField_management("UN2.SHP", "CNDCLASS",
                                            '!MRCLASS! + "," + !TPCLASS! + "," + !SOILTXT! + "," + !SLCLASS! + "," + !ASCLASS!',
                                            'PYTHON_9.3')

            arcpy.env.extent = "MAXOF"
            arcpy.MultipartToSinglepart_management("UN2.shp", "UN3")
            arcpy.ClearEnvironment("extent")
            array.append("UN1.shp")
            array.append("UN2.shp")
            array.append("UN3.shp")
            # arcpy.AddField_management("UN2.shp", "AREA_ha", "DOUBLE")
            # arcpy.CalculateField_management("UN2.shp", "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
            # arcpy.MakeFeatureLayer_management("UN2.shp", layer1)
            # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"MRCLASS" = \'\' OR "TPCLASS" = \'\' OR "SLCLASS" = \'\' OR "ASCLASS" = \'\' OR "SOILTXT" = \'\' OR "AREA_HA" < 0.01')
            # arcpy.DeleteFeatures_management(layer1)
            # arcpy.Union_analysis("UN2.shp", "UN3", "", "", "NO_GAPS")
            # array.append("UN3.shp")
            shape = "UN3.shp"
            y = int(arcpy.GetCount_management(shape).getOutput(0))
            x = 1
            count = 1
            print "eliminating slivers @" + datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            while y != x:
                y = x
                arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                        '"MRCLASS" = \'\' OR "TPCLASS" = \'\' OR "SLCLASS" = \'\' OR "ASCLASS" = \'\' OR "SOILTXT" = \'\' OR "AREA_HA" < 0.00001')
                arcpy.Eliminate_management(layer1, "Xa{}".format(count), "AREA")
                shape = "Xa{}.shp".format(count)
                array.append(shape)
                arcpy.Delete_management(layer1)
                x = int(arcpy.GetCount_management(shape).getOutput(0))
                count += 1

            y = int(arcpy.GetCount_management(shape).getOutput(0))
            x = 1
            count = 1

            while y != x:
                y = x
                arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                        '"MRCLASS" = \'\' OR "TPCLASS" = \'\' OR "SLCLASS" = \'\' OR "ASCLASS" = \'\' OR "SOILTXT" = \'\' OR "AREA_HA" < 0.0001')
                arcpy.Eliminate_management(layer1, "Xa2{}".format(count), "AREA")
                shape = "Xa2{}.shp".format(count)
                array.append(shape)
                arcpy.Delete_management(layer1)
                x = int(arcpy.GetCount_management(shape).getOutput(0))
                count += 1

            y = int(arcpy.GetCount_management(shape).getOutput(0))
            x = 1
            count = 1

            while y != x:
                y = x
                arcpy.AddField_management(shape, "AREA_ha", "DOUBLE")
                arcpy.CalculateField_management(shape, "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
                arcpy.MakeFeatureLayer_management(shape, layer1)
                arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION",
                                                        '"MRCLASS" = \'\' OR "TPCLASS" = \'\' OR "SLCLASS" = \'\' OR "ASCLASS" = \'\' OR "SOILTXT" = \'\' OR "AREA_HA" < 0.05')
                arcpy.Eliminate_management(layer1, "Xa3{}".format(count), "AREA")
                shape = "Xa3{}.shp".format(count)
                array.append(shape)
                arcpy.Delete_management(layer1)
                x = int(arcpy.GetCount_management(shape).getOutput(0))
                count += 1
            arcpy.FeatureClassToFeatureClass_conversion(shape, self.output_location + "\CND", "CND1")
            for d in array:
                arcpy.Delete_management(d)

        print 'Done slivers; working on final layer'

        arcpy.env.scratchworkspace = self.scratch_workspace
        dem = self.output_location + "\\newsurface\\dem_clip"
        dsc = arcpy.Describe(dem)
        arcpy.env.extent = dsc.Extent
        arcpy.env.workspace = self.output_location + "\\CND"

        array = []
        field_list = ["MRCLASS", "TPCLASS", "SOILTXT", "SLCLASS", "ASCLASS", "CNDCLASS", self.substrate_name_field]
        arcpy.Dissolve_management("CND1.SHP", "CND5", field_list, "", "SINGLE_PART")

        shape = self.output_location + '/cnd/CND5.shp'
        if arcpy.Exists(self.output_location + "\BNDRYMTCH\ecophase.shp"):
            if int(arcpy.GetCount_management(self.output_location + "\BNDRYMTCH\ecophase.shp").getOutput(0)) == 0:
                print 'No pre-existing ecophases to account for.'
            else:
                print 'Boundary matching taking place'
                arcpy.Erase_analysis(shape, self.output_location + "\BNDRYMTCH\ecophase.shp", "CND2")
                arcpy.Merge_management(self.output_location + "\BNDRYMTCH\ecophase.shp; CND2.shp", "CND3")

                shape = self.output_location + '/cnd/CND3.shp'
                sys.exit('need to check that we are bringing this in correctly')

        layer1 = 'layer.lyr'
        y = int(arcpy.GetCount_management(shape).getOutput(0))
        x = 1
        count = 1
        print 'eliminating errors'
        while y != x:
            y = x
            arcpy.MakeFeatureLayer_management(shape, layer1)
            # There is opportunity here to make this smarter. Move up and down in label, not just a simple eliminate
            arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"CND" = ' + "'ERROR'")
            arcpy.Eliminate_management(layer1, "X{}".format(count))
            shape = "X{}.shp".format(count)
            array.append(shape)
            arcpy.Delete_management(layer1)
            x = int(arcpy.GetCount_management(shape).getOutput(0))
            count += 1
        arcpy.FeatureClassToFeatureClass_conversion(shape, self.output_location + "\CND", "condition_set")

        print 'Calculating ecophase'
        insrs = arcpy.Describe('CND5.shp').spatialReference.factoryCode
        command = 'shp2pgsql -I -S -d -s %s %s reclamation_assignment_model.ram_data_from_python_for_ecophase_classification| %s' % (
        insrs, self.output_location + "/CND/condition_set.shp", self.psql)
        self.cmdline.execute_command_line(command)

        #
        # field_list = ['CND', 'Ecosite', 'a1', 'b1', 'b2', 'b3', 'b4', 'c1', 'd1', 'd2', 'd3', 'e1', 'e2', 'e3', 'f1', 'f2', 'f3', 'g1', 'h1', 'i1', 'i2', 'j1', 'j2', 'k1', 'k2', 'k3', 'l1']
        # HelperFunctions().fast_join_field(self.label, 'label', self.output_location + "/CND/cnd5.shp", 'CNDCLASS',  joinFields=field_list, joinAllFields=False)
        # #
        # # arcpy.MakeFeatureLayer_management("CND5.SHP", layer1)
        # # arcpy.JoinField_management(layer1, "CNDCLASS", self.label, "label")
        # HelperFunctions().fast_join_field(self.substrate_proportions, "subs_type", self.output_location + "/CND/cnd5.shp", "substrate_")
        # # arcpy.Delete_management(layer1)
        #
        # arcpy.Merge_management("CND5.shp; " + self.output_location + "\\CND\\default.shp", "CND4")
        #
        # print 'Calculating ecophase'
        # arcpy.AddField_management(shape, 'a1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'b1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'b2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'b3_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'b4_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'c1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'd1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'd2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'd3_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'e1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'e2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'e3_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'f1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'f2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'f3_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'g1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'h1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'i1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'i2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'j1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'j2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'k1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'k2_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'k3_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'l1_1', 'DOUBLE')
        # arcpy.AddField_management(shape, 'ecophase', 'STRING')
        #
        # arcpy.CalculateField_management(shape, 'a1_1', '[sub_a] * [a1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'b1_1', '[sub_b] * [b1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'b2_1', '[sub_b] * [b2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'b3_1', '[sub_b] * [b3]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'b4_1', '[sub_b] * [b4]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'c1_1', '[sub_c] * [c1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'd1_1', '[sub_d] * [d1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'd2_1', '[sub_d] * [d2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'd3_1', '[sub_d] * [d3]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'e1_1', '[sub_e] * [e1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'e2_1', '[sub_e] * [e2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'e3_1', '[sub_e] * [e3]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'f1_1', '[sub_f] * [f1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'f2_1', '[sub_f] * [f2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'f3_1', '[sub_f] * [f3]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'g1_1', '[sub_g] * [g1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'h1_1', '[sub_h] * [h1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'i1_1', '[sub_i] * [i1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'i2_1', '[sub_i] * [i2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'j1_1', '[sub_j] * [j1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'j2_1', '[sub_j] * [j2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'k1_1', '[sub_k] * [k1]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'k2_1', '[sub_k] * [k2]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'k3_1', '[sub_k] * [k3]', 'PYTHON_9.3')
        # arcpy.CalculateField_management(shape, 'l1_1', '[sub_l] * [l1]', 'PYTHON_9.3')
        #
        # expression = 'MyCalc(!a1_1!,!b1_1!,!b2_1!,!b3_1!,!b4_1!,!c1_1!,!d1_1!,!d2_1!,!d3_1!,!e1_1!,!e2_1!,!e3_1!,!f1_1!,!f2_1!,!f3_1!,!g1_1!,!h1_1!,!i1_1!,!i2_1!,!j1_1!,!j2_1!,!k1_1!,!k2_1!,!k3_1!,!l1_1!,!CND!)'
        # codeblock = """def MyCalc(a1_1,b1_1,b2_1,b3_1,b4_1,c1_1,d1_1,d2_1,d3_1,e1_1,e2_1,e3_1,f1_1,f2_1,f3_1,g1_1,h1_1,i1_1,i2_1,j1_1,j2_1,k1_1,k2_1,k3_1,l1_1, CND):
        # array = [a1_1,b1_1,b2_1,b3_1,b4_1,c1_1,d1_1,d2_1,d3_1,e1_1,e2_1,e3_1,f1_1,f2_1,f3_1,g1_1,h1_1,i1_1,i2_1,j1_1,j2_1,k1_1,k2_1,k3_1,l1_1]
        # array1 = ['a1','b1','b2','b3','b4','c1','d1','d2','d3','e1','e2','e3','f1','f2','f3','g1','h1','i1','i2','j1','j2','k1','k2','k3','l1']
        # index_me = 0
        # for r in array:
        #     if CND == 'CONFLICT':
        #         if r == max(array):
        #             ecophase = array1[index_me]
        #             break
        #         index_me += 1
        #     else:
        #         ecophase = CND
        # return ecophase"""
        # arcpy.CalculateField_management(shape, 'ecophase', expression, "PYTHON_9.3", codeblock)
        # arcpy.CalculateField_management(shape, 'ecosite', 'left([ecophase],1)', 'PYTHON_9.3')
        # print 'Creating final Layer'
        # arcpy.Copy_management(shape, "CND_pre")
        # arcpy.SimplifyPolygon_cartography(shape, "CND", "BEND_SIMPLIFY", "10 Meters", "100 SquareMeters",
        #                                   "RESOLVE_ERRORS",
        #                                   "KEEP_COLLAPSED_POINTS")
        # arcpy.AddField_management("CND.shp", "AREA_ha", "DOUBLE")
        # arcpy.CalculateField_management("CND.shp", "AREA_ha", "!Shape.Area@Hectares!", "PYTHON_9.3")
        # # arcpy.MakeFeatureLayer_management("CND.shp", layer1)
        # # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"CND" = \'\'')
        # # arcpy.CalculateField_management(layer1, "CND", "[condition]")
        # # arcpy.SelectLayerByAttribute_management(layer1, "NEW_SELECTION", '"CND" = \'ERROR\'')
        # array.append("CND1.shp")
        # array.append("CND4.shp")
        # array.append("CND5.shp")
        # array.append("default.shp")
        #
        # for d in array:
        #     arcpy.Delete_management(d)

        # y = int(arcpy.GetCount_management(shape).getOutput(0))
        # x = 1
        # count = 1
        # print 'eliminating errors'
        # while y != x:
        #     y = x
        #     arcpy.MakeFeatureLayer_management(shape, Layer2)
        #     # There is opportunity here to make this smarter. Move up and down in label, not just a simple eliminate
        #     arcpy.SelectLayerByAttribute_management(Layer2, "NEW_SELECTION", '"CND" = ' + "'ERROR'")
        #     arcpy.Eliminate_management(Layer2, "X{}".format(count))
        #     shape = "X{}.shp".format(count)
        #     array.append(shape)
        #     arcpy.Delete_management(Layer2)
        #     x = int(arcpy.GetCount_management(shape).getOutput(0))
        #     count += 1
        # arcpy.FeatureClassToFeatureClass_conversion(shape, output_location + "\CND", "CND4")

        # for feature in ["CND1.shp", "CND2.shp", "CND3.shp", "CND4.shp"]:
        #     try:
        #         array.append(feature)
        #     except:
        #         pass
        # for d in array:
        #     arcpy.Delete_management(d)

        # shape = self.output_location + "\CND\\CND4.shp"
        # print 'starting the madness'

    def run(self):

        # self.create()
        # self.start()
        # self.streams()
        # self.moist()
        # self.topographic()
        self.condition_set()
        # self.from_here1()
        # self.everything_else()
