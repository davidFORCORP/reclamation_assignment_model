from models.base_execution.surface_background import DemCreation


class CreateSurface:

    def __init__(self):
        self.background = DemCreation()

    def run(self):

        self.background.extent_run()
        self.background.raw_dem_preparation()

        dem = self.background.culvert_burn()
        dem = self.background.burn_in_hydrological_features(dem)

        self.background.create_base_rasters(dem)

        print '...surface complete'
        return
