from models.base_execution.substrate_preparation import SubstratePreparation
from models.base_execution.hydrology_background import HydrologyCreation

class CreateMoistureRegime():
    def __init__(self):
        pass

    def run(self):

        test = SubstratePreparation().test_if_to_run()
        if test:
            return

        sai = SubstratePreparation().define_sai()
        flow_accumulation, flow_direction = HydrologyCreation().base_data()
        flow_initiation_cutoff = 40000
        # flow_accumulation, flow_initiation_cutoff = HydrologyCreation().adjust_flow_accumulation(flow_accumulation, sai)
        HydrologyCreation().create_and_assess_streams(flow_accumulation, flow_direction, flow_initiation_cutoff)
        HydrologyCreation().predict_increased_moisture()
        HydrologyCreation().create_topographic_moisture_index()
        HydrologyCreation().tmi_to_tmr()

        print '...moisture regime complete'
        return
