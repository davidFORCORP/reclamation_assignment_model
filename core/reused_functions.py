import arcpy, sys
import time, os, shutil, warnings
import subprocess
from datetime import datetime
import re


class WorkspaceWorker():

    def __init__(self, workspace):
        self.workspace = workspace

    def delete(self):

        try:
            shutil.rmtree(self.workspace)
        except:
            for feature in arcpy.ListFeatureClasses(self.workspace):
                arcpy.Delete_management(feature)
            for raster in arcpy.ListRasters(self.workspace):
                arcpy.Delete_management(raster)
            try:
                time.sleep(20)
                shutil.rmtree(self.workspace)
            except:
                warnings.warn('...... cannot delete temp workspace')

    def create(self):

        temp_space = self.workspace + '/temp'
        if not os.path.isdir(self.workspace):
            os.makedirs(self.workspace)
        if os.path.isdir(temp_space):
            try:
                shutil.rmtree(temp_space)
            except:
                try:
                    time.sleep(20)
                    shutil.rmtree(temp_space)
                except:
                    WorkspaceWorker(temp_space).delete()
                    # warnings.warn('...... cannot delete temp workspace')
                    return
        os.makedirs(temp_space)

        return temp_space


class CleanAndRepair():

    def __init__(self, temp_space):
        self.temp_space = temp_space
        arcpy.env.overwriteOutput = True

    def run(self, shape, area_in_meters_to_remove=1000):
        arcpy.env.scratchWorkspace = self.temp_space
        arcpy.env.workspace = self.temp_space

        print "......started dataset cleaning @ " + datetime.fromtimestamp(time.time()).strftime('%H:%M')
        arcpy.RepairGeometry_management(shape)
        arcpy.MultipartToSinglepart_management(shape, "cleaning")
        shape = 'cleaning.shp'
        arcpy.RepairGeometry_management(shape)

        layer = 'layer.lyr'

        y = int(arcpy.GetCount_management(shape).getOutput(0))
        x = 1
        count = 1
        while y != x:

            try:
                arcpy.AddField_management(shape, "AREA", "DOUBLE")
            except:
                time.sleep(10)
                arcpy.AddField_management(shape, "AREA", "DOUBLE")

            arcpy.CalculateField_management(shape, "AREA", "!Shape.Area!", "PYTHON_9.3")

            y = x
            arcpy.MakeFeatureLayer_management(shape, layer)
            arcpy.SelectLayerByAttribute_management(layer, "NEW_SELECTION", '"AREA" < ' + str(area_in_meters_to_remove))
            arcpy.Eliminate_management(layer, "Xa{}".format(count), "AREA")
            shape = "Xa{}.shp".format(count)

            arcpy.Delete_management(layer)
            x = int(arcpy.GetCount_management(shape).getOutput(0))
            count += 1

        arcpy.MakeFeatureLayer_management(shape, layer)
        arcpy.SelectLayerByAttribute_management(layer, "NEW_SELECTION", '"AREA" = 0')
        arcpy.DeleteFeatures_management(layer)
        arcpy.Delete_management(layer)

        print "......finished eliminating slivers @ " + datetime.fromtimestamp(time.time()).strftime('%H:%M')

        return os.path.join(self.temp_space, shape)

    def buffer_quick(self, shape, area_in_meters_to_buffer=1, out_in='OUT_FIRST', number_of_iterations=1):
        """

        :param shape:
        :param area_in_meters_to_buffer:
        :param out_in: OUT_FIRST or IN_FIRST
        :param number_of_iterations: buffer in and out how many times
        :return:
        """

        arcpy.env.scratchWorkspace = self.temp_space
        arcpy.env.workspace = self.temp_space

        print "......started dataset cleaning @ " + datetime.fromtimestamp(time.time()).strftime('%H:%M')
        positive = area_in_meters_to_buffer
        negative = area_in_meters_to_buffer * -1
        arcpy.RepairGeometry_management(shape)

        for iteration in range(number_of_iterations):
            if out_in == 'IN_FIRST':
                arcpy.Buffer_analysis(shape, 'temp', negative)
                shape = 'temp.shp'
                count = int(arcpy.GetCount_management(shape).getOutput(0))
                if count == 0:
                    return None

                arcpy.MultipartToSinglepart_management(shape, "cleaning")
                shape = 'cleaning.shp'
                arcpy.RepairGeometry_management(shape)
                arcpy.Buffer_analysis(shape, 'temp', positive)
                shape = 'temp.shp'
                arcpy.MultipartToSinglepart_management(shape, "cleaning")
                shape = 'cleaning.shp'
                arcpy.RepairGeometry_management(shape)
            else:
                arcpy.Buffer_analysis(shape, 'temp', positive)
                shape = 'temp.shp'
                arcpy.MultipartToSinglepart_management(shape, "cleaning")
                shape = 'cleaning.shp'
                arcpy.RepairGeometry_management(shape)
                arcpy.Buffer_analysis(shape, 'temp', negative)
                shape = 'temp.shp'
                arcpy.MultipartToSinglepart_management(shape, "cleaning")
                shape = 'cleaning.shp'
                arcpy.RepairGeometry_management(shape)
            count = int(arcpy.GetCount_management('cleaning.shp').getOutput(0))
            if count == 0:
                return None

        buffer_fixed = os.path.join(self.temp_space, 'buffer_fix.shp')
        arcpy.Copy_management(shape, buffer_fixed)

        return buffer_fixed


class CleanRaster():
    def __init__(self, temp_space):
        self.temp_space = temp_space
        self.raster_clean_space = os.path.join(temp_space, 'raster_clean')
        if not os.path.isdir(self.raster_clean_space):
            os.makedirs(self.raster_clean_space)

    def run(self, raster, number_of_cells=1, out_in='OUT_FIRST', reverse=False, specific_values=None,
            specific_order=False):
        """

        :param raster:
        :param number_of_cells:
        :param out_in: OUT_FIRST = expand then shrink; IN_FIRST = shrink then expand; OUT_ONLY = just perform expand; IN_ONLY = just perfrom shrink
        :param reverse:
        :param specific_values: list object of values to use in cleaning
        :return:
        """

        arcpy.CheckOutExtension("Spatial")
        arcpy.env.overwriteOutput = True
        arcpy.env.scratchWorkspace = self.raster_clean_space
        arcpy.env.workspace = self.raster_clean_space

        if not arcpy.Exists(raster):
            raster = os.path.join(self.temp_space, raster)
        arcpy.CalculateStatistics_management(raster)

        raster_values = [row[0] for row in arcpy.da.SearchCursor(raster, ["VALUE"])]
        raster_values = list(dict.fromkeys(raster_values))

        if reverse:
            raster_values.sort(reverse=True)
        else:
            if not specific_order:
                raster_values.sort()
            else:
                for specific_value in specific_values:
                    if specific_value not in raster_values:
                        specific_values.remove(specific_value)
                raster_values = specific_values

        for raster_value in raster_values:
            if specific_values:
                if raster_value not in specific_values:
                    continue

            if out_in == 'IN_FIRST':
                in_ = self.shrink(raster, raster_value, number_of_cells)
                out_ = self.expand(in_, raster_value, number_of_cells)
                raster = out_

            elif out_in == 'OUT_FIRST':
                out_ = self.expand(raster, raster_value, number_of_cells)
                in_ = self.shrink(out_, raster_value, number_of_cells)
                raster = in_

            elif out_in == 'OUT_ONLY':
                out_ = self.expand(raster, raster_value, number_of_cells)
                raster = out_

            elif out_in == 'IN_ONLY':
                in_ = self.shrink(raster, raster_value, number_of_cells)
                raster = in_

        return os.path.join(self.raster_clean_space, raster)

    def shrink(self, raster, raster_value, number_of_cells):
        arcpy.env.scratchWorkspace = self.raster_clean_space
        arcpy.env.workspace = self.raster_clean_space

        result = 'result_s' + str(raster_value) + '.tif'
        arcpy.gp.Shrink_sa(raster, result, number_of_cells, [raster_value])
        arcpy.CalculateStatistics_management(result)

        return result

    def expand(self, raster, raster_value, number_of_cells):
        arcpy.env.scratchWorkspace = self.raster_clean_space
        arcpy.env.workspace = self.raster_clean_space

        result = 'result_e' + str(raster_value) + '.tif'
        arcpy.gp.Expand_sa(raster, result, number_of_cells, [raster_value])
        arcpy.CalculateStatistics_management(raster)

        return result


class HelperFunctions():
    def __init__(self):
        pass

    def get_key(self, dictionary, val):
        keys = [k for k, v in dictionary.items() if v == val]
        if keys:
            return keys[0]
        return None

    def execute_command_line(self, cmd):
        process = subprocess.Popen(cmd, shell=True)
        while process.poll() is None:
            time.sleep(0.5)

    def add_single_quotes_to_string_array(self, string_array):

        f = "some test {Value1,Value2} more text {Value3} more text {Value4, Value1, Value5, Value9, Value11, Value21}"
        rec = re.sub(r"{([^{}]*)}", lambda x: "{{'{}'}}".format("', '".join(re.split(r'\s*,\s*', x.group(1)))), f)
        print(rec)

    def fast_join_field(self, table_with_fields_to_add, table_with_fields_to_add_key, table_to_add_fields_to, table_to_add_fields_to_key, joinFields=None, joinAllFields=True):

        if not joinAllFields:
            fList = [f for f in arcpy.ListFields(table_with_fields_to_add) if f.name in joinFields]
        else:
            fList = [f for f in arcpy.ListFields(table_with_fields_to_add) if str(f.name).upper() not in ['OID', 'FID', 'FID_', 'OID_', 'SHAPE']]

        intable_fields = [str(f.name).upper() for f in arcpy.ListFields(table_to_add_fields_to)]

        for i in range(len(fList)):
            name = fList[i].name
            type = fList[i].type
            if name.upper() in intable_fields:
                fList.pop(fList[i])
                continue
            if type == 'Integer':
                arcpy.AddField_management(table_to_add_fields_to, name, field_type='LONG')
            elif type == 'String':
                arcpy.AddField_management(table_to_add_fields_to, name, field_type='TEXT', field_length=fList[i].length)
            elif type == 'Double':
                arcpy.AddField_management(table_to_add_fields_to, name, field_type='DOUBLE')
            elif type == 'Date':
                arcpy.AddField_management(table_to_add_fields_to, name, field_type='DATE')
            else:
                arcpy.AddError('\nUnknown field type: {0} for field: {1}'.format(type, name))

        fList = [x.name for x in fList]
        join_fields = [table_with_fields_to_add_key] + fList
        accepting_fields = [table_to_add_fields_to_key] + fList

        key_list = [x[0].upper() for x in arcpy.da.SearchCursor(table_to_add_fields_to, [table_to_add_fields_to_key], sql_clause=(None, 'ORDER BY ' + table_to_add_fields_to_key))]

        with arcpy.da.SearchCursor(table_with_fields_to_add, [str(x) for x in join_fields], sql_clause=(None, 'ORDER BY ' + table_with_fields_to_add_key)) as cursor:
            for append_row in cursor:
                if append_row[0].upper() not in key_list:
                    continue
                with arcpy.da.UpdateCursor(table_to_add_fields_to, [str(x) for x in accepting_fields], sql_clause=(None, 'ORDER BY ' + table_to_add_fields_to_key)) as receiving_table:
                    for row in receiving_table:
                        if append_row[0].upper() != row[0].upper():
                            continue
                        for i in range(len(fList)):
                            row[i+1] = append_row[i+1]
                            receiving_table.updateRow(row)
