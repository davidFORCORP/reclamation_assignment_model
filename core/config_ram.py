try:
    from configparser import ConfigParser # python 3
except ImportError:
    from ConfigParser import SafeConfigParser as ConfigParser # python 2


class Config(object):
    """
    Reads from the settings.ini file and provides the configuration
    to the rest of the application
    """

    def __init__(self):
        self.config = ConfigParser()
        self.config.read('settings_mmv18.ini')

    def get(self, section, option):
        return self.config.get(section, option)
